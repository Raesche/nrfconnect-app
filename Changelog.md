## 0.0.1 - 2022-09-01

### Initial release
- Working application, for evaluation and testing purpose

## 0.0.1 - 2022-xx-xx

### Added
- Added features

## Changed
- Changes

### Fixed
- Fix #1
- Fix #2
