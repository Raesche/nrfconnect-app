# BLT2450 nRF Connect App

BLT2450 nRF Connect App is an application that can be launched by [nRF Connect for Desktop](https://github.com/NordicSemiconductor/pc-nrfconnect-launcher). It enables to control any BLT2450 tester that are connected to the system and requires a server application
to be running.

## Installation

Installation showed to be tricky because the pc-nrfconnect-shared module requires an older version of react than the MUI framework. Just
running npm install may lead to problems and the following sequence proofed to work as a workaround:

-   clone repo or revert local package.json and package-lock.json changes
-   remove node_modules if present (using rmdir /Q/S node_modules on windows)
-   remove @arendi/testing from package.json dependencies (if present)
-   execute "npm link @arendi/testing" if a local copy is available ("npm link" must be executed in the @arendi/testing dev directory before)

## Development

Call "npm run dev" to start development build with watch feature
Call "npm rund build" to create distribution bundle
Call "npm pack" to create a zipped copy of the project

## License

See the [LICENSE](LICENSE) file for details.
