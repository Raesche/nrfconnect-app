/* eslint-disable simple-import-sort/imports */
import React from 'react'
import { DtmPattern } from '@arendi/testing'
import Selection from './Selection'


const items = DtmPattern.List().map(def => ({text:def[0], value:def[1]}))

const PatternSelect = React.memo(({onChange=null, selected=DtmPattern.Prbs9, multiple=false, disabled=false, style={}}) => {
  return (
      <Selection 
          label="Pattern"
          items={items}
          selected={selected}
          onChange={onChange}
          multiple={multiple}
          disabled={disabled}
          style={style}
      />
  )
})

PatternSelect.propTypes = {
    onChange: null,
    selected: [],
    multiple: false,
    disabled: false,
    style: {}
}

export default PatternSelect