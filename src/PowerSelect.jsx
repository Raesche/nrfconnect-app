/* eslint-disable simple-import-sort/imports */
import React from 'react'
import { Blt24TxPower } from '@arendi/testing'
import Selection from './Selection'


const items = Blt24TxPower.List().map(def => ({text:`${def[1]} dBm`, value:def[1]}))


const PowerSelect = React.memo(({onChange=null, selected=Blt24TxPower.Pos0Dbm, multiple=false, disabled=false, style={}}) => {
  return (
      <Selection 
          label="Power"
          items={items}
          selected={selected}
          onChange={onChange}
          multiple={multiple}
          disabled={disabled}
          style={style}
      />
  )
})

PowerSelect.propTypes = {
    onChange: null,
    selected: [],
    multiple: false,
    disabled: false,
    style: {}
}

export default PowerSelect