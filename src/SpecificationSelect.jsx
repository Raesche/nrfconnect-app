/* eslint-disable simple-import-sort/imports */
import React from 'react'
import { Specification } from '@arendi/testing'
import Selection from './Selection'


const items = Specification.List().map(def => ({text:`${def[0]}`, value:def[1]}))

const SpecificationSelect = React.memo(({onChange=null, selected=Specification.V5_0, multiple=false, disabled=false, style={}}) => {
  return (
      <Selection 
          label="Specification"
          items={items}
          selected={selected}
          onChange={onChange}
          multiple={multiple}
          disabled={disabled}
          style={style}
      />
  )
})

SpecificationSelect.propTypes = {
    onChange: null,
    selected: [],
    multiple: false,
    disabled: false,
    style: {}
}

export default SpecificationSelect