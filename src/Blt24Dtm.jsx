/* eslint-disable react/no-unused-prop-types */
/* eslint-disable simple-import-sort/imports */
/* eslint-disable no-shadow */
import React, { useCallback, useState, useEffect, useRef } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { SetActiveTesterAttenuation } from './reducers/TesterReducer'
import { DtmMode } from '@arendi/testing'
import DtmConfig from './DtmConfig'
import { SetDtmConfig, SetTesterMeasurementData, SetDutMeasurementData, ExitDtmApplication } from './reducers/DtmReducer'


import { blue, grey, green } from '@mui/material/colors'
import Stack from '@mui/material/Stack'
import Grid from '@mui/material/Grid'
import Card from '@mui/material/Card'
import CardHeader from '@mui/material/CardHeader'
import CardContent from '@mui/material/CardContent'
import CardActions from '@mui/material/CardActions'
import FormControl from '@mui/material/FormControl'
import Tooltip from '@mui/material/Tooltip'
import Divider from '@mui/material/Divider'

import ArrowBackIcon from '@mui/icons-material/KeyboardDoubleArrowLeft'
import ArrowForwardIcon from '@mui/icons-material/KeyboardDoubleArrowRight'
import ChevronLeftIcon from '@mui/icons-material/ChevronLeft'
import ChevronRightIcon from '@mui/icons-material/ChevronRight'
import IconButton from '@mui/material/IconButton'
import MoreVertIcon from '@mui/icons-material/MoreVert'
import StartTxIcon from '@mui/icons-material/PlayArrow'
import StartRxIcon from '@mui/icons-material/Mic'
import StopIcon from '@mui/icons-material/Stop'
import SnapshotIcon from '@mui/icons-material/CameraAlt'

import Chart from './Chart'
import Knob from './Knob'

const CHART_TIME_INTERVAL_MS = 1000 * 60 * 2

// Keep data from up to 3 minutes
const ReduceTo3Minutes = serie => {
    const currentDate = new Date()
    const currentTime = currentDate.getTime()
    const closest = serie.findIndex(sample => {
        const sampleDate = new Date(sample[0])
        const sampleTime = sampleDate.getTime()
        return (currentTime - sampleTime) <= 1000 * 60 * 3
    })

    if (closest >= 1) {
        serie.splice(0, closest - 1)
        return serie
    }

    if (closest === 0)
        return serie

    return []
}


// See
// https://vasconez.dev/posts/1
// https://vasconez.dev/posts/2
// https://dev.to/maneetgoyal/using-apache-echarts-with-react-and-typescript-353k
// https://echarts.apache.org/en/option.html#title


function Blt24Dtm() {

    const dispatch = useDispatch()
    const { activeDut: dut, dtmMode: dutDtmMode, dtm: dutDtm } = useSelector(state => state.app.dut)
    const { activeTester: tester, dtmMode: testerDtmMode, dtm: testerDtm, attenuationDb } = useSelector(state => state.app.tester)
    const { dtmConfig, testerMeasurementData: testerSerie, dutMeasurementData: dutSerie } = useSelector(state => state.app.dtm)
    const [timer, SetTimer] = useState(0)
    const [series, SetSeries] = useState([])
    const disableSettingsControls = ((testerDtmMode !== DtmMode.Idle)) || (dut ? (dutDtmMode !== DtmMode.Idle) : false)
    const chartRef = useRef(null)
    const testerSerieRef = useRef(testerSerie)
    const dutSerieRef = useRef(dutSerie)
    const limitSerieRef = useRef({})


    // Save settings and measurements to persistent store on unmount
    useEffect(() => {
        return () => {
            dispatch(SetTesterMeasurementData(testerSerieRef.current))
            dispatch(SetDutMeasurementData(dutSerieRef.current))
            dispatch(ExitDtmApplication())
        }
    }, []) // eslint-disable-line react-hooks/exhaustive-deps


    const SetAttenuation = useCallback(value => dispatch(SetActiveTesterAttenuation(Math.min(Math.max(value, 0), 120))), []) // eslint-disable-line react-hooks/exhaustive-deps


    const DtmConfigHandler = useCallback(settings => dispatch(SetDtmConfig({
        channel: settings.channels,
        bytes: settings.bytes,
        pattern: settings.pattern,
        phy: settings.phy,
        interval: settings.interval
    })), []) // eslint-disable-line react-hooks/exhaustive-deps


    // Sample PER data and update chart series
    useEffect(() => {
        const now = new Date()
        const past = new Date(now - CHART_TIME_INTERVAL_MS)
        const nowISOString = now.toISOString()
        testerSerieRef.current = ReduceTo3Minutes([...testerSerieRef.current, [nowISOString, (testerDtmMode === DtmMode.Rx) ? testerDtm.per : null]])
        dutSerieRef.current = ReduceTo3Minutes([...dutSerieRef.current, [nowISOString, (dutDtmMode === DtmMode.Rx) ? dutDtm.per : null]])
        limitSerieRef.current = {
            name: "Limit",
            itemStyle: { color: '#ff0000' },
            lineStyle: { type: 'solid', width: 0.5 },
            data: [[past.toISOString(), 30.8], [nowISOString, 30.8]]
        }

        SetSeries([limitSerieRef.current, { name: "Tester Rx", data: testerSerieRef.current }, { name: "DUT Rx", data: dutSerieRef.current }])
    }, [timer]) // eslint-disable-line react-hooks/exhaustive-deps


    // Adjust chart update interval
    useEffect(() => {
        const interval = setInterval(() => (SetTimer(value => value + 1)), (dtmConfig.interval ?? 1000) / 2)
        return () => {
            clearInterval(interval)
        }
    }, [dtmConfig.interval])


    // Start tester DTM Tx mode
    const TesterStartTx = async () => {
        await tester?.SetDtmMode()
        await tester?.DtmTxStart(dtmConfig.channel, dtmConfig.bytes, dtmConfig.pattern, dtmConfig.phy, attenuationDb, 0)
    }

    // Start tester DTM Rx mode
    const TesterStartRx = async () => {
        await tester?.SetDtmMode()
        await tester?.DtmRxStart(dtmConfig.channel, dtmConfig.bytes, dtmConfig.pattern, dtmConfig.phy, attenuationDb, dtmConfig.interval)
    }

    // Stop tester DTM mode
    const TesterStop = async () => {
        await tester?.DtmStop()
    }

    // Start DUT DTM Tx mode
    const DutStartTx = async () => {
        await dut?.DtmTxStart(dtmConfig.channel, dtmConfig.bytes, dtmConfig.pattern, dtmConfig.phy, null, 0)
    }

    // Start DUT DTM Rx mode
    const DutStartRx = async () => {
        await dut?.DtmRxStart(dtmConfig.channel, dtmConfig.bytes, dtmConfig.pattern, dtmConfig.phy, null, dtmConfig.interval)
    }

    // Stop DUT DTM mode
    const DutStop = async () => {
        await dut?.DtmStop()
    }

    // Export SVG image
    const ExportImage = () => {
        chartRef.current?.ExportImage("Dtm.svg")
    }

    return (
        <Grid container spacing={2} sx={{ minWidth: 400, maxWidth: 1000, mt: 0 }}>

            {/* BLT24 tester controls */}
            <Grid item xs>
                <Card sx={{ minWidth: 400, minHeight: 400 }}>
                    <CardHeader
                        action={
                            <IconButton aria-label="settings">
                                <MoreVertIcon />
                            </IconButton>
                            }
                        title="Tester"
                        subheader="Set BLT2450 DTM mode and attenuation"
                        />

                    <CardContent sx={{ minHeight: 300, display: "flex", flexDirection: "column" }}>
                        <Grid container spacing={2}>

                            {/* PER and attenuation knobs */}
                            <Grid item xs={6}>
                                <Knob
                                    value={testerDtm.per}
                                    min={0}
                                    max={100}
                                    start={-45}
                                    stop={45}
                                    segmentColor={blue[500]}
                                    valueColor={blue[500]}
                                    labelColor={grey[500]}
                                    label="% PER"
                                    step={0.1}
                                    decimals={1}
                                    width="200"
                                    height="200"
                                    disabled={testerDtmMode !== DtmMode.Rx}
                                    />
                            </Grid>
                            <Grid item xs={6}>
                                <Knob
                                    value={attenuationDb}
                                    min={0}
                                    max={120}
                                    start={-45}
                                    stop={45}
                                    segmentColor={green[500]}
                                    valueColor={green[500]}
                                    labelColor={grey[500]}
                                    label="dB"
                                    step={0.25}
                                    onChanged={SetAttenuation}
                                    width="200"
                                    height="200"
                                    />
                            </Grid>

                            {/* DTM controls */}
                            <Grid item xs={6} sx={{ marginTop: "auto" }}>
                                <Stack direction="row" justifyContent="space-evenly" spacing={1}>
                                    <IconButton disabled={testerDtmMode === DtmMode.Tx} onClick={TesterStartTx}><StartTxIcon sx={(testerDtmMode === DtmMode.Tx) ? { color: blue[500] } : null} /></IconButton>
                                    <IconButton disabled={testerDtmMode === DtmMode.Rx} onClick={TesterStartRx}><StartRxIcon sx={(testerDtmMode === DtmMode.Rx) ? { color: blue[500] } : null} /></IconButton>
                                    <IconButton disabled={testerDtmMode === DtmMode.Idle} onClick={TesterStop}><StopIcon sx={(testerDtmMode === DtmMode.Idle) ? { color: blue[500] } : null} /></IconButton>
                                </Stack>
                            </Grid>

                            {/* Attenuation controls */}
                            <Grid item xs={6} sx={{ marginTop: "auto" }}>
                                <Stack direction="row" justifyContent="space-evenly" spacing={1}>
                                    <IconButton onClick={() => SetAttenuation(attenuationDb - 1)}><ArrowBackIcon /></IconButton>
                                    <IconButton onClick={() => SetAttenuation(attenuationDb - 0.25)}><ChevronLeftIcon /></IconButton>
                                    <IconButton onClick={() => SetAttenuation(attenuationDb + 0.25)}><ChevronRightIcon /></IconButton>
                                    <IconButton onClick={() => SetAttenuation(attenuationDb + 1)}><ArrowForwardIcon /></IconButton>
                                </Stack>
                            </Grid>
                        </Grid>
                    </CardContent>
                </Card>
            </Grid>

            {/* DUT controls */}
            <Grid item xs="auto">
                <Card sx={{ minWidth: 300, maxWidth: 300, minHeight: 400 }}>
                    <CardHeader
                        action={
                            <IconButton aria-label="settings">
                                <MoreVertIcon />
                            </IconButton>
                            }
                        title="DUT"
                        subheader={dut ? `Set ${dut.identifier} DTM mode` : "No DUT selected"}
                        />

                    <CardContent sx={{ minHeight: 300, display: "flex", flexDirection: "column" }}>
                        <Grid container spacing={2}>

                            {/* PER and attenuation knobs */}
                            <Grid item xs={12}>
                                <Knob
                                    value={dutDtm.per}
                                    min={0}
                                    max={100}
                                    start={-45}
                                    stop={45}
                                    segmentColor={blue[500]}
                                    valueColor={blue[500]}
                                    labelColor={grey[500]}
                                    label="% PER"
                                    step={0.1}
                                    decimals={1}
                                    width="200"
                                    height="200"
                                    disabled={dutDtmMode !== DtmMode.Rx}
                                    />
                            </Grid>

                            {/* DTM controls */}
                            <Grid item xs={12}>
                                <Stack direction="row" justifyContent="space-evenly" spacing={1}>
                                    <IconButton disabled={(dutDtmMode === DtmMode.Tx) || !(dut?.identifier)} onClick={DutStartTx}><StartTxIcon sx={(dutDtmMode === DtmMode.Tx) ? { color: blue[500] } : null} /></IconButton>
                                    <IconButton disabled={(dutDtmMode === DtmMode.Rx) || !(dut?.identifier)} onClick={DutStartRx}><StartRxIcon sx={(dutDtmMode === DtmMode.Rx) ? { color: blue[500] } : null} /></IconButton>
                                    <IconButton disabled={(dutDtmMode === DtmMode.Idle) || !(dut?.identifier)} onClick={DutStop}><StopIcon sx={(dutDtmMode === DtmMode.Idle) ? { color: blue[500] } : null} /></IconButton>
                                </Stack>
                            </Grid>
                        </Grid>
                    </CardContent>
                </Card>
            </Grid>


            {/* DTM chart */}
            <Grid item xs={12}>
                <Card sx={{ minWidth: 400, paddingTop: 0 }}>
                    {/* DTM settings controls, misc. */}
                    <CardContent sx={{ mt: 1 }}>
                        <Stack spacing={0} sx={{ width: "100%", maxwidth: 800 }}>
                            <Stack direction="row" spacing={2}>
                                <Tooltip
                                    title={<h5>{disableSettingsControls ? "DTM settings can only be changed when tester and DUT are in DTM Idle mode" : "Define DTM settings"}</h5>}
                                    enterDelay={1000}
                                    placement="right"
                                    arrow
                                    >
                                    <div>
                                        <DtmConfig
                                            channels={dtmConfig.channel}
                                            bytes={dtmConfig.bytes}
                                            pattern={dtmConfig.pattern}
                                            phy={dtmConfig.phy}
                                            interval={dtmConfig.interval}
                                            onChange={DtmConfigHandler}
                                            disabled={disableSettingsControls}
                                            />
                                    </div>
                                </Tooltip>
                            </Stack>

                        </Stack>
                    </CardContent>

                    <Divider />

                    <CardContent sx={{ mt: 0, padding: 0 }}>
                        <Grid container spacing={0}>
                            <Grid item xs={12}>
                                <Chart
                                    ref={chartRef}
                                    yMin={0}
                                    yMax={100}
                                    height={340}
                                    yAxisUnit="%"
                                    xMin={limitSerieRef.current?.data?.[0][0]}
                                    time
                                    series={series}
                                    />
                            </Grid>
                        </Grid>
                    </CardContent>

                    {/* Misc. controls */}
                    <CardActions>
                        <Stack spacing={2} sx={{ width: "100%", maxwidth: 800 }}>
                            <Stack direction="row" spacing={2}>

                                {/* Save SVG image */}
                                <FormControl sx={{ ml: 4 }} size="small" >
                                    <Tooltip title={<h5>Save snapshot</h5>} enterDelay={1000} placement="top" arrow>
                                        <IconButton onClick={ExportImage}><SnapshotIcon fontSize="medium" /></IconButton>
                                    </Tooltip>
                                </FormControl>
                            </Stack>
                        </Stack>
                    </CardActions>
                </Card>
            </Grid>
        </Grid>

    )
}

Blt24Dtm.propTypes = {

}

export default Blt24Dtm