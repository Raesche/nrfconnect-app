/* eslint-disable simple-import-sort/imports */
import React from 'react'
import { Parity } from '@arendi/testing'
import Selection from './Selection'


const items = Parity.List().map(def => ({text:`${def[0]}`, value:def[1]}))

const ParitySelect = React.memo(({onChange=null, selected=Parity.None, multiple=false, disabled=false, style={}}) => {
  return (
      <Selection 
          label="Parity"
          items={items}
          selected={selected}
          onChange={onChange}
          multiple={multiple}
          disabled={disabled}
          style={style}
      />
  )
})

ParitySelect.propTypes = {
    onChange: null,
    selected: [],
    multiple: false,
    disabled: false,
    style: {}
}

export default ParitySelect