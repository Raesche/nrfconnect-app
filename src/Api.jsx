/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-await-in-loop */
/* eslint-disable no-nested-ternary */
/* eslint-disable simple-import-sort/imports */
import React, { useCallback, useEffect, useRef, useState} from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Init as InitDut } from './reducers/DutReducer'
import { Init as InitTester } from './reducers/TesterReducer'
import { Init as InitApi, SetServerSettings } from './reducers/ApiReducer'
import { Api, WsInterface } from '@arendi/testing'
import ServerTest from './ServerTest'
import Box from '@mui/material/Box'
import List from '@mui/material/List'
import ListItemButton from '@mui/material/ListItemButton'
import ListItemText from '@mui/material/ListItemText'


import CircularProgress from '@mui/material/CircularProgress'
import { red, green } from '@mui/material/colors';
import SuccessIcon from '@mui/icons-material/Done'
import FailIcon from '@mui/icons-material/Clear'


// Make wsInterface and API available for other modules
export const wsInterface = new WsInterface();
export const api = new Api(wsInterface)



function ArendiTesting ({url=''}) {
  const dispatch = useDispatch()
  const { isConnected, isConnecting, serverName, serverPort} = useSelector(state => state.app.api)

  const [ dialogOpen, SetDialogOpen ] = useState(false)

  



  // Server settings changed by pressing "Ok" button
  // Copy settings and close dialog!
  const ServerSettingsHandler = useCallback((name, port) => {
    dispatch(SetServerSettings(name, port))
  }, [])

  const ServerDialogCloseHandler = useCallback(() => {
    SetDialogOpen(false)
  }, [])



  useEffect(() => {


    // RunServer()
    
    // List handler
    // const TesterListHandler = list => dispatch(SetTesterList(list))
    // const DutListHandler = list => dispatch(SetDutList(list))

    // Register list handler and intialize reducers
    // api.TesterList.OnListChanged(TesterListHandler)
    // api.DutList.OnListChanged(DutListHandler)
    InitApi(dispatch, api)
    InitTester(dispatch, api)
    InitDut(dispatch, api)
    
    // Cleanup list handler on unmount
    return () => {
      // api.TesterList.OffListChanged(TesterListHandler)
      // api.DutList.OffListChanged(DutListHandler)
    }
    }, []) // eslint-disable-line react-hooks/exhaustive-deps


  
  // Creates server connection status icon
  const ConnectionStatusIcon = () => {
    if (isConnected)
      return <SuccessIcon fontSize="medium" sx={{color: green[500]}}/>

    if (isConnecting)
      return <CircularProgress size={25}/>

    return <FailIcon fontSize="medium" sx={{color: red[500]}}/>
  }


  return (
      <>
          <Box sx={{bgcolor: 'background.paper', mb:2}}>
              <List>
                  <ListItemButton onClick={() => SetDialogOpen(open => !open)}>
                      <ListItemText primary="Server" secondary={isConnected? "connected" : "disconnected"} />              
                      {ConnectionStatusIcon()}
                  </ListItemButton> 
              </List>
          </Box>

          <ServerTest
              open={dialogOpen}
              onClose={ServerDialogCloseHandler}
              onChange={ServerSettingsHandler}
              name={serverName}
              port={serverPort}
          />
      </>
  )
}

ArendiTesting.propTypes = {
  url: ''
}

export default ArendiTesting
