/* eslint-disable react/no-unused-prop-types */
/* eslint-disable simple-import-sort/imports */
/* eslint-disable no-shadow */
import React, {useState, useEffect, useRef} from 'react'
import { useSelector } from 'react-redux'

import { blue, grey } from '@mui/material/colors'
import Stack from '@mui/material/Stack'
import Grid from '@mui/material/Grid'
import Card from '@mui/material/Card'
import CardHeader from '@mui/material/CardHeader'
import CardContent from'@mui/material/CardContent'
import CardActions from '@mui/material/CardActions'
import FormControl from '@mui/material/FormControl'
import Tooltip from '@mui/material/Tooltip'

import IconButton from '@mui/material/IconButton'
import MoreVertIcon from '@mui/icons-material/MoreVert'
import SnapshotIcon from '@mui/icons-material/CameraAlt'

import Chart from './Chart'
import Knob from './Knob'

function Blt24PowerMeter() {

  // get Tester data from store
  const { powerDbm } = useSelector(state => state.app.tester)  
  const [testerSerie, SetTesterSerie] = useState([])
  const [timer, SetTimer] = useState(0)
  const chartRef = useRef(null)

  // Initialize component
  useEffect(() => {    
    // Make sure chart is regularly updated, even if attenuation has not been changed
    const interval = setInterval(() => (SetTimer(value => value + 1)), 200)
    
    // Cleanup when dismounted
    return () => clearInterval(interval)
  }, [])  // eslint-disable-line react-hooks/exhaustive-deps

    
  useEffect(() => {    
    if (typeof powerDbm === 'number') {
        const serie = [...testerSerie, [new Date().toISOString(), powerDbm]].splice(-300, 300)
        SetTesterSerie(serie)
    }  
  }, [timer]) // eslint-disable-line react-hooks/exhaustive-deps


  // Export SVG image
  const ExportImage = () => {
    chartRef.current?.ExportImage("PowerMeter.svg")
  }

    return (
        <Grid container spacing={2} sx={{ minWidth: 600, maxWidth: 1000, mt: 0 }}>
            {/* Tester controls */}
            <Grid item xs>
                <Card sx={{ minWidth: 300, maxWidth: 300, minHeight: 400, mb: 0 }}>
                    <CardHeader
                        action={
                            <IconButton aria-label="settings">
                                <MoreVertIcon />
                            </IconButton>
                          }
                        title="Power Meter"
                        subheader="RF power measured on RF1"
                      />

                    <CardContent sx={{ minHeight: 300, display: "flex", flexDirection: "column" }}>
                        <Grid container spacing={2}>
                            {/* PER and attenuation knobs */}
                            <Grid item xs={12}>
                                <Knob
                                    value={powerDbm}
                                    min={-75}
                                    max={10}
                                    start={-45}
                                    stop={45}
                                    segmentColor={blue[500]}
                                    valueColor={blue[500]}
                                    labelColor={grey[500]}
                                    label="dBm"
                                    step={0.1}
                                    decimals={1}
                                    width="200"
                                    height="200"
                                />
                            </Grid>
                        </Grid>
                    </CardContent>
                </Card>
            </Grid>

            {/* Dummy, required so Chart component can take up full space. Why??? */}
            <Grid item xs="auto" sx={{ minWidth: 300, maxWidth: 300, minHeight: 400, mb: 0 }}/>      
                
            {/* PER Chart */ }
            <Grid item xs={12}>
                <Card sx={{ minWidth: 400, mb: 0 }}>
                    <CardContent>
                        <Grid container spacing={2}>
                            <Grid item xs={12}>
                                <Chart
                                    ref={chartRef}
                                    yMin={-75}
                                    yMax={10}
                                    height={400}
                                    yAxisUnit="dBm"
                                    time
                                    series={[{name:"Input power [dBm]", data: testerSerie}]}
                                    />
                            </Grid>
                        </Grid>
                    </CardContent>

                    <CardActions>
                        <Stack spacing={2} sx={{width:"100%", maxwidth:800}}>
                            <Stack direction="row" spacing={2}>
                                {/* Save SVG image */}
                                <FormControl sx={{ml:4}} size="small" >
                                    <Tooltip title="Save snapshot" enterDelay={500}>
                                        <IconButton onClick={ExportImage}><SnapshotIcon fontSize="medium"/></IconButton>
                                    </Tooltip>
                                </FormControl>
                            </Stack>
                        </Stack>
                    </CardActions>
                </Card>
            </Grid>
        </Grid>
  )
}

Blt24PowerMeter.propTypes = {

}

export default Blt24PowerMeter