/* eslint-disable no-case-declarations */
/* eslint-disable class-methods-use-this */

import { DtmBytes, DtmChannel, DtmDirection, DtmPattern, DtmPhy } from '@arendi/testing'
import { getPersistentStore } from 'pc-nrfconnect-shared'

const SET_SENSITIVITY_DTM_CONFIG = "SET_SENSITIVITY_DTM_CONFIG"
const SET_SENSITIVITY_ATTENUATION_RANGE = "SET_SENSITIVITY_ATTENUATION_RANGE"
const SET_SENSITIVITY_ATTENUATION_STEPSIZE = "SET_SENSITIVITY_ATTENUATION_STEPSIZE"
const SET_SENSITIVITY_DIRECTION = "SET_SENSITIVITY_DIRECTION"
const SET_SENSITIVITY_MEASUREMENT_DATA = "SET_SENSITIVITY_MEASUREMENT_DATA"

const CONFIG_ROOT = "sensitivityApplication"
const CONFIG_SETTINGS = "settings"

const ATTENUATION_MIN = 0
const ATTENUATION_MAX = 120

const Restrict = (value:number, min:number, max:number) => (value < min)? min : (value > max)? max : value // eslint-disable-line no-nested-ternary  

// Class for handling loading, changing and saving of settings
class Settings {
  
  dtm: any
  rangeDb: any[]
  stepDb: number
  direction: DtmDirection
  constructor() {
    this.Load()
  }

  Load() {
    const { dtm, rangeDb, stepDb, direction } = this.Repair(ReadConfig(CONFIG_SETTINGS))
    this.dtm = dtm
    this.rangeDb = rangeDb
    this.stepDb = stepDb
    this.direction = direction
  }

  Save() {
    WriteConfig(CONFIG_SETTINGS, this.Repair(this))
  }

  Repair({dtm, rangeDb, stepDb, direction}:any = {}) {
    return {
      dtm: this.RepairDtm(dtm),
      rangeDb: ((rangeDb instanceof Array) && rangeDb.length === 2)? rangeDb : [ATTENUATION_MIN, ATTENUATION_MAX],
      stepDb: (typeof stepDb === "number")? stepDb : 1,
      direction: (DtmDirection.Name(direction))? direction : DtmDirection.TesterToDut
    }
  }

  RepairDtm(config:any={}) {
    if (!(config?.channels instanceof Array) || config?.channels?.length === 0)
      config.channels = [DtmChannel.Ch0]
  
    if (typeof config?.pattern !== "number")
      config.pattern = DtmPattern.Prbs9
  
    if (typeof config?.bytes !== "number")
      config.bytes = DtmBytes.B37
  
    if (typeof config?.phy !== "number")
      config.phy = DtmPhy.Phy1Mbps
  
    if (typeof config?.interval !== "number")
      config.interval = 1000
  
    return config
  }
}


// Read / write configuration to persistent store
const store = getPersistentStore()
const ReadConfig = node => store.get(`${CONFIG_ROOT}${node? `.${node}` : ''}`)
const WriteConfig = (node, data) => store.set(`${CONFIG_ROOT}${node? `.${node}` : ''}`, data)


// Read initial settings
const settings = new Settings()


export const SetDtmConfig = dtmConfig => ({
  type: SET_SENSITIVITY_DTM_CONFIG,
  dtmConfig
})

export const SetAttenuationRange = rangeDb => ({
  type: SET_SENSITIVITY_ATTENUATION_RANGE,
  rangeDb
})

export const SetAttenuationStepsize = stepDb => ({
  type: SET_SENSITIVITY_ATTENUATION_STEPSIZE,
  stepDb
})

export const SetDirection = direction => ({
  type: SET_SENSITIVITY_DIRECTION,
  direction
})

export const SetMeasurementData = data => ({
  type: SET_SENSITIVITY_MEASUREMENT_DATA,
  data
})


const initialState = {
  dtmConfig: settings.dtm,
  rangeDb: settings.rangeDb,
  stepDb: settings.stepDb,
  direction: settings.direction,
  measurementData: [[]]
}


export default (state = initialState, {type, ...action}) => {
  switch(type) {

      // Set dtm settings and save to persistent store
      case SET_SENSITIVITY_DTM_CONFIG:
        settings.dtm = action.dtmConfig
        settings.Save()
        return {
          ...state,
          dtmConfig: settings.dtm
        }

      // Set attenuation range and save to persistent store
      case SET_SENSITIVITY_ATTENUATION_RANGE:      
        const startDb = Restrict(action.rangeDb[0], 0, 120)?? 0
        const stopDb = Restrict(action.rangeDb[1], 0, 120)?? 120      
        settings.rangeDb = [startDb, stopDb]
        settings.Save()
        return {
          ...state,
          rangeDb: settings.rangeDb
        }

      // Set stepsize and save to persistent store
      case SET_SENSITIVITY_ATTENUATION_STEPSIZE:
        settings.stepDb = action.stepDb
        settings.Save()
        return {
          ...state,
          stepDb: settings.stepDb
        }

      // Set direction and save to persistent store
      case SET_SENSITIVITY_DIRECTION:
        settings.direction = action.direction
        settings.Save()
        return {
          ...state,
          direction: settings.direction
        }

      // Set measurement data
      case SET_SENSITIVITY_MEASUREMENT_DATA: { 
        return {
          ...state,
          measurementData: action.data?? []
        }
      }

      default:
          return state
  }
}