
import { DtmBytes, DtmChannel, DtmPattern, DtmPhy } from '@arendi/testing'
import { getPersistentStore } from 'pc-nrfconnect-shared'

const SET_DTM_DTM_CONFIG = "SET_DTM_DTM_CONFIG"
const SET_DTM_TESTER_MEASUREMENT_DATA = "SET_DTM_TESTER_MEASUREMENT_DATA"
const SET_DTM_DUT_MEASUREMENT_DATA = "SET_DTM_DUT_MEASUREMENT_DATA"
const EXIT_DTM_APPLICATION = "EXIT_DTM_APPLICATION"

const CONFIG_ROOT = "dtmApplication"
const CONFIG_DTM = "settings.dtm"
const CONFIG_MEASUREMENT = "measurements"


// Fixes errors in dtm configuration
const RepairDtmConfig = (config:any = {}) => {
  if (typeof config?.channel !== "number")
  config.channel = DtmChannel.Ch0

  if (typeof config?.pattern !== "number")
  config.pattern = DtmPattern.Prbs9

  if (typeof config?.bytes !== "number")
  config.bytes = DtmBytes.B37

  if (typeof config?.phy !== "number")
  config.phy = DtmPhy.Phy1Mbps

  if (typeof config?.interval !== "number")
  config.interval = 200

  return config
}

// Fixes errors in measurement data
const RepairMeasurementData = measurement => {
  const {tester=[], dut=[]} = measurement?? {tester:[], dut:[]}
  return {tester, dut}
}

// Read / write configuration to persistent store
const store = getPersistentStore()
const ReadConfig = node => store.get(`${CONFIG_ROOT}${node? `.${node}` : ''}`)
const WriteConfig = (node, data) => store.set(`${CONFIG_ROOT}${node? `.${node}` : ''}`, data)

// Read / write DTM configuration to persistent store, repair if required
const ReadDtmConfig = () => RepairDtmConfig(ReadConfig(CONFIG_DTM))
const WriteDtmConfig = config => WriteConfig(CONFIG_DTM, RepairDtmConfig(config))

// Read / write measurement data
const ReadMeasurementData = () => RepairMeasurementData(ReadConfig(CONFIG_MEASUREMENT))
const WriteMeasurementData = data => WriteConfig(CONFIG_MEASUREMENT, RepairMeasurementData(data))


export const SetDtmConfig = config => ({
  type: SET_DTM_DTM_CONFIG,
  config
})

export const SetTesterMeasurementData = data => ({
  type: SET_DTM_TESTER_MEASUREMENT_DATA,
  data
})

export const SetDutMeasurementData = data => ({
  type: SET_DTM_DUT_MEASUREMENT_DATA,
  data
})

export const ExitDtmApplication = () => ({
  type: EXIT_DTM_APPLICATION
})


const initialState = {
  dtmConfig: ReadDtmConfig(),
  testerMeasurementData: ReadMeasurementData().tester,
  dutMeasurementData: ReadMeasurementData().dut
}


export default (state = initialState, {type, ...action}) => {
  switch(type) {

      // Save default dtm config in persistent store
      case SET_DTM_DTM_CONFIG: {
        WriteDtmConfig(action.config)
        return {
          ...state,
          dtmConfig: action.config
        }
      }

      // set measurement data
      case SET_DTM_TESTER_MEASUREMENT_DATA: {        
        return {
          ...state,
          testerMeasurementData: action.data?? []
        }
      }

      // set measurement data
      case SET_DTM_DUT_MEASUREMENT_DATA: {        
        return {
          ...state,
          dutMeasurementData: action.data?? []
        }
      }

      // Save settings to persistent store
      case EXIT_DTM_APPLICATION: {
        // WriteMeasurementData({tester: state.testerMeasurementData, dut: state.dutMeasurementData})
        return state
      }

      default:
          return state
  }
}