
import { DtmChannel,Tester } from '@arendi/testing'
import { getPersistentStore } from 'pc-nrfconnect-shared'


const SET_GENERATOR_POWER = "SET_GENERATOR_POWER"
const GENERATOR_START = "GENERATOR_START"
const GENERATOR_STOP = "GENERATOR_STOP"

const RepairGeneratorConfig = config => {
  if (typeof config?.channel !== "number")
    config.channel = DtmChannel.Ch0

  if (typeof config?.powerDbm !== "number")
    config.powerDbm = 0

  if (typeof config?.active !== "boolean")
    config.active = false

  return config
}


const store = getPersistentStore()
const defaultGeneratorConfig = RepairGeneratorConfig(store.get("generatorConfig")?? {})


export const SetPower = (tester:Tester, powerDbm:number) => ({
  type: SET_GENERATOR_POWER,
  tester,
  powerDbm
})

export const Start = (tester:Tester, channel:number, powerDbm:number) => ({
  type: GENERATOR_START,
  tester,
  channel,
  powerDbm
})

export const Stop = tester => ({
  type: GENERATOR_STOP,
  tester
})


const initialState = {
  channel: defaultGeneratorConfig.channel,
  powerDbm: defaultGeneratorConfig.powerDbm,
  active: false
}


export default (state = initialState, {type, ...action}) => {
  switch(type) {

      // Set output power and save in persistent store
      case SET_GENERATOR_POWER: {
        if (!(action.tester instanceof Tester))
          throw new Error("Error in SET_GENERATOR_POWER, tester is not a valid Tester instance")

        action.tester.SetGeneratorPower(action.powerDbm)
        return {
          ...state,
          powerDbm: action.powerDbm
        }
      }

      // Start generator
      case GENERATOR_START: {
        if (!(action.tester instanceof Tester))
          throw new Error("Error in GENERATOR_START, tester is not a valid Tester instance")

        const config = RepairGeneratorConfig({
          channel: action.channel,
          powerDbm: action.powerDbm,
          active: true
        })
       
        store.set("generatorConfig", config)
        action.tester.GeneratorStart(config.channel, config.powerDbm)
        return {
          ...state,
          ...config
        }
      }

      // Stop generator
      case GENERATOR_STOP: {
        if (!(action.tester instanceof Tester))
          throw new Error("Error in GENERATOR_STOP, tester is not a valid Tester instance")

        const config = RepairGeneratorConfig({
          ...state,
          active: false
        })

        store.set("generatorConfig", config)
        action.tester.GeneratorStop()
        return {
          ...state,
          ...config
        }
      }


      default:
        return state
  }
}