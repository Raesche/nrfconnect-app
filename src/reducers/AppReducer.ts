

const APP_SET_MEASUREMENT_ACTIVE = "APP_SET_MEASUREMENT_ACTIVE"

export const SetMeasurementActive = active => ({
  type: APP_SET_MEASUREMENT_ACTIVE,
  active
})



const initialState = {
  measurementActive: false
}


export default (state = initialState, {type, ...action}) => {
  switch(type) {

      // Set when measurement is currently active
      case APP_SET_MEASUREMENT_ACTIVE:
        return {
          ...state,
          measurementActive: action.active
        }

      default:
          return state
  }
}