/* eslint-disable no-shadow */
/* eslint-disable no-case-declarations */
/* eslint-disable @typescript-eslint/ban-types */

import { Api, ConnectionStatus, DtmMode, Dut, DutDefaultConfig, DutList } from '@arendi/testing'
import { getPersistentStore, logger } from 'pc-nrfconnect-shared'


// Action definitions
// Names are shared and must be unique throughout the whole app!!!
const DUT_INIT = "DUT_INIT"
const SET_LIST = "SET_LIST"
const SET_ACTIVE_DUT = "SET_ACTIVE_DUT"
const ACTIVE_DUT_DTM_MODE_CHANGED = "ACTIVE_DUT_DTM_MODE_CHANGED"
const ACTIVE_DUT_DTM_CHANGED = "ACTIVE_DUT_DTM_CHANGED"
const SET_FAVORITE_DUT = "SET_FAVORITE_DUT"
const SET_DUT_LIST = "SET_DUT_LIST"
const SET_DUT_CONNECTION_STATUS = "SET_DUT_CONNECTION_STATUS"



let api = undefined as unknown as Api

let dutList!: DutList
let dispatch: Function
const store = getPersistentStore()


const CloseHandler = data => {
  // console.log("Interface closed", data)
  // console.log("Interface closed")
}

const OpenHandler = data => {
  // console.log("Interface opened", data)
  // console.log("Interface opened")
}


const ListHandler = list => {
  dispatch?.({type: SET_DUT_LIST, list})
}

const ConnectionHandler = data => {
  dispatch?.({type:SET_DUT_CONNECTION_STATUS, data})
}

const DtmModeHandler = mode => {
  dispatch?.({type: ACTIVE_DUT_DTM_MODE_CHANGED, mode})
}

const DtmHandler = data => {
  dispatch?.({type: ACTIVE_DUT_DTM_CHANGED, ...data})
}


export const Init = (dispatchRef, apiRef) => {
  if (!dispatchRef || !apiRef)
    throw new Error("Error in DutReducer.Init(): dispatchRef and Api must be defined")

  dispatch = dispatchRef
  api = apiRef
  dispatch({type: DUT_INIT, api}) 
}

/* 

export const SetList = list => ({
  type: SET_LIST,
  list
}) */

export const SetActiveDut = dut => ({
  type: SET_ACTIVE_DUT,
  dut
})

export const ClearActiveDut = () => ({
  type: SET_ACTIVE_DUT,
  dut: null
})

export const SetFavoriteDut = identifier => ({
  type: SET_FAVORITE_DUT,
  identifier
})

export const IsConnected = dut => (dut?.connectionStatus === ConnectionStatus.Connected)

const initialState = {
  activeDut: null as unknown as Dut,
  favoriteDut: store.get("favoriteDut"),  // Identifier string only
  dtmMode: DtmMode.Idle,
  dtm: {
    per: null,
    count: null,
    intervalMs: null
  },

  attached: null,
  removed: null,

  connectionStatus: ConnectionStatus.Disconnected,
  baudrate: DutDefaultConfig.baudrate,
  handshake: DutDefaultConfig.handshake,
  parity: DutDefaultConfig.parity,
  specification: DutDefaultConfig.specification,
  protocol: DutDefaultConfig.protocol
}


export default (state = initialState, {type, ...action}) => {
  switch(type) {

    case DUT_INIT:
      // Already initialized
      // if (state.dutList)
      //  return state

      return {
        ...state,
      //  dutList
      }

    /* case SET_DUT_LIST:
      // Unregister events from "old" dut list
      state.list.forEach(dut => dut.OffConnectionChanged(ConnectionHandler))

      // Register events from "new" dut list
      action.list?.forEach(dut => dut.OnConnectionChanged(ConnectionHandler))
      
      logger.info(`DUT list received:`)
      action.list?.forEach(dut => logger.info(` - ${dut.identifier} (${dut.description})`))
      return {
        ...state,
        list:action.list
      }

      */
      
    case SET_ACTIVE_DUT:
      // No change required
      if (state.activeDut === action.dut)
        return state

      // Unregister events from "old" activeDut
      state.activeDut?.OffDtmModeChanged?.(DtmModeHandler)
      state.activeDut?.OffDtmEvent?.(DtmHandler)
      state.activeDut?.OffConnectionChanged?.(ConnectionHandler)

      // const dut = api.DutList?.Get(action.dut?.identifier)
      // pendingActiveDut = action.dut
      
      if (action.dut) {
        // Register events from "new" activeDut        
        action.dut.OnDtmModeChanged?.(DtmModeHandler)
        action.dut.OnDtmEvent?.(DtmHandler)
        action.dut.OnConnectionChanged?.(ConnectionHandler)
        logger.info(`Selecting DUT ${action.dut.identifier} (${action.dut.description})`)
      }
      else {
        logger.info(`No DUT selected`)
      }

      return {
        ...state,
        activeDut: IsConnected(action.dut)? action.dut : null,
        dtmMode: action.dut?.dtmMode,
        connectionStatus: action.dut?.connectionStatus,
        baudrate: action.dut?.baudrate,
        handshake: action.dut?.handshake,
        parity: action.dut?.parity,
        specification: action.dut?.specification,
        protocol: action.dut?.protocol        
      }

      

    // DUT connection state changed
    /* case SET_DUT_CONNECTION_STATUS:
      
      let activeDut:Dut | undefined

      // Clear activeDut if its connection has closed      
      if (!IsConnected(state.activeDut))
        activeDut = undefined

      // Set activeDut if its connection has opened
      if (IsConnected(pendingActiveDut))
        activeDut = pendingActiveDut      
      
      setTimeout(() => dispatch?.(SetActiveDut(activeDut)), 100)

      return {
        ...state,
        connectionStatus: state.activeDut?.connectionStatus,
        baudrate: action?.baudrate,
        handshake: action?.handshake,
        parity: action?.parity,
        specification: action?.specification,
        protocol: action?.protocol
      }

      */

    case SET_FAVORITE_DUT:
      store.set("favoriteDut", action?.identifier)
      return {
        ...state,
        favoriteDut: action?.identifier
      }

    case ACTIVE_DUT_DTM_MODE_CHANGED:
      logger.info(`DUT set to ${DtmMode.Name(action.mode)} mode`)
      return {
        ...state,
        dtmMode: action.mode
      }

    case ACTIVE_DUT_DTM_CHANGED:
      return {
        ...state,
        dtm: action
      }  

    default:
        return state
  }
}