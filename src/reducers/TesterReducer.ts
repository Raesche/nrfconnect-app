/* eslint-disable @typescript-eslint/ban-types */
/* eslint-disable no-case-declarations */

import { Api, DtmMode, Enum, Tester, TesterList, TesterMode } from '@arendi/testing'
import { getPersistentStore, logger } from 'pc-nrfconnect-shared'

// Action definitions
// Names are shared and must be unique throughout the whole app!!!
const TESTER_INIT = "TESTER_INIT"
const SET_TESTER_LIST = "SET_TESTER_LIST"
const SET_ACTIVE_TESTER = "SET_ACTIVE_TESTER"
const SET_FAVORITE_TESTER = "SET_FAVORITE_TESTER"
const SET_ACTIVE_TESTER_MODE = "SET_ACTIVE_TESTER_MODE"
const ACTIVE_TESTER_MODE_CHANGED = "ACTIVE_TESTER_MODE_CHANGED"
const ACTIVE_TESTER_POWER_CHANGED = "ACTIVE_TESTER_POWER_CHANGED"
const ACTIVE_TESTER_ATTENUATION_CHANGED = "ACTIVE_TESTER_ATTENUATION_CHANGED"
const ACTIVE_TESTER_DTM_MODE_CHANGED = "ACTIVE_TESTER_DTM_MODE_CHANGED"
const ACTIVE_TESTER_DTM_CHANGED = "ACTIVE_TESTER_DTM_CHANGED"
const ACTIVE_TESTER_GENERATOR_CHANGED = "ACTIVE_TESTER_GENERATOR_CHANGED"
const SET_ACTIVE_TESTER_ATTENUATION = "SET_ACTIVE_TESTER_ATTENUATION"


const DEFAULT_TIMEOUT_MS = 3000

class ExtendedTesterMode extends Enum {
  static Idle = TesterMode.Idle
  static Dtm = TesterMode.Dtm
  static Attenuator = TesterMode.Attenuator
  static PowerMeter = TesterMode.PowerMeter
  static Generator = TesterMode.Generator
  static Ble = TesterMode.Ble
  static Sensitivity = 100
}

let api = undefined as unknown as Api


let dispatch: Function
const store = getPersistentStore()

let pendingMode = store.get("pendingMode")?? ExtendedTesterMode.Idle

// Calculates the "extended" Blt24Mode (converts to ExtendedTesterMode.Sensitivity if applicable)
const GetExtendedTesterMode = blt24Mode => (pendingMode === ExtendedTesterMode.Sensitivity && blt24Mode === TesterMode.Dtm)? ExtendedTesterMode.Sensitivity : blt24Mode


const ModeHandler = mode => {
  dispatch?.({type: ACTIVE_TESTER_MODE_CHANGED, mode})
}

const PowerHandler = powerDbm => {
  dispatch?.({type: ACTIVE_TESTER_POWER_CHANGED, powerDbm})
}

const AttenuationHandler = attenuationDb => {
  dispatch?.({type: ACTIVE_TESTER_ATTENUATION_CHANGED, attenuationDb})
}

const GeneratorHandler = data => {
  dispatch?.({type: ACTIVE_TESTER_GENERATOR_CHANGED, active: data.active, powerDbm: data.powerDbm})
}

const DtmModeHandler = mode => {
  dispatch?.({type: ACTIVE_TESTER_DTM_MODE_CHANGED, mode})
}

const DtmHandler = data => {
  dispatch?.({type: ACTIVE_TESTER_DTM_CHANGED, ...data})
}

export { ExtendedTesterMode }


export const Init = (dispatchRef, apiRef) => {
  if (!dispatchRef || !apiRef)
    throw new Error("Error in TesterReducer.Init(): dispatchRef and api must be defined")

  dispatch = dispatchRef
  api = apiRef
  dispatch({type: TESTER_INIT, api}) 
}

/* export const SetTesterList = (list:Tester[]) => ({
  type: SET_TESTER_LIST,
  list
}) */

export const SetActiveTester = tester => ({
  type: SET_ACTIVE_TESTER,
  tester
})

export const SetFavoriteTester = serialNumber => ({
  type: SET_FAVORITE_TESTER,
  serialNumber
})

export const ClearActiveTester = () => ({
  type: SET_ACTIVE_TESTER,
  tester: null
})

export const SetActiveTesterMode = mode => ({
  type: SET_ACTIVE_TESTER_MODE,
  mode
})

export const SetActiveTesterAttenuation = attenuationDb => ({
  type: SET_ACTIVE_TESTER_ATTENUATION,
  attenuationDb
})


const initialState = {
  activeTester: null as unknown as Tester,
  hardwareVersionMajor: 0,
  hardwareVersionMinor: 0,
  favoriteTester: store.get("favoriteTester"),  // serialNumber string only
  mode: TesterMode.Idle,
  powerDbm: null,
  attenuationDb: null,
  generatorActive: false,
  generatorPowerDbm: null,
  dtmMode: DtmMode.Idle,
  dtmPer: null,
  dtmCount: null,
  dtmIntervalMs: null,
  dtm: {
    per: null,
    count: null,
    intervalMs: null
  }
}


export default (state = initialState, {type, ...action}) => {
  switch(type) {


      case TESTER_INIT:
        // Already initialized
        // if (state.testerList)
         // return
        
        // testerList = new TesterList(action.wsInterface)
        // testerList.OnListChanged(ListHandler)
        // action.wsInterface.OnOpen(OpenHandler)
        // action.wsInterface.OnClose(CloseHandler)
        return {
          ...state,
          // TesterList: action.testerList
        }
        
      /* case SET_TESTER_LIST:
      case API_TESTER_LIST_CHANGED:
        logger.info(`Tester list received:`)
        console.log("TESTER LIST RECEIVED", action)
        action.list.forEach((tester:Tester) => logger.info(` - ${tester.serialNumber} (${tester.productName})`))
        return {
          ...state,
          list:action.list,
          testerMissing: !action.list.length
        }
*/
      case SET_ACTIVE_TESTER:          
        // No change required
        if (state.activeTester === action.tester)
          return state          

        // unregister
        state.activeTester?.OffModeChanged?.(ModeHandler)
        state.activeTester?.OffPowerChanged?.(PowerHandler)
        state.activeTester?.OffAttenuationChanged?.(AttenuationHandler)
        state.activeTester?.OffGeneratorChanged?.(GeneratorHandler)
        state.activeTester?.OffDtmModeChanged?.(DtmModeHandler)
        state.activeTester?.OffDtmEvent?.(DtmHandler)

        const tester = api.TesterList?.Get(action.tester?.serialNumber)
       
        if (tester) {
          // register new event handler
          tester.OnModeChanged?.(ModeHandler)
          tester.OnPowerChanged?.(PowerHandler)
          tester.OnAttenuationChanged?.(AttenuationHandler)
          tester.OnGeneratorChanged?.(GeneratorHandler)
          tester.OnDtmModeChanged?.(DtmModeHandler)
          tester.OnDtmEvent?.(DtmHandler)

          // save current tester in persistent store
          store.set("activeTester", tester.serialNumber)
          logger.info(`Selecting tester ${tester.serialNumber}`)

        }
        else {
          logger.info(`No tester selected`)
        }


        return {
          ...state,
          isValid: true,
          mode: GetExtendedTesterMode(tester?.mode),
          dtmMode: tester?.dtmMode,
          activeTester: tester,
          hardwareVersionMajor: tester?.hardwareVersion.major,
          hardwareVersionMinor: tester?.hardwareVersion.minor,
          attenuationDb: tester?.attenuationDb,
          powerDbm: tester?.powerDbm,
          generatorActive: tester?.generatorActive,
          generatorPowerDbm: tester?.generatorPowerDbm
        }

      case SET_FAVORITE_TESTER:
        store.set("favoriteTester", action?.serialNumber)
        return {
          ...state,
          favoriteTester: action?.serialNumber
        }

      case ACTIVE_TESTER_MODE_CHANGED:
        const mode = GetExtendedTesterMode(action.mode)
        logger.info(`Tester set to ${ExtendedTesterMode.Name(mode)} mode`)

        return {
          ...state,
          mode
        }

      case SET_ACTIVE_TESTER_MODE: {
        pendingMode = TesterMode.Idle
        
        switch (action.mode) {
          // Normal Blt24Mode
          case TesterMode.Idle:
          case TesterMode.Dtm:
          case TesterMode.PowerMeter:
          case TesterMode.Attenuator:
          case TesterMode.Generator:
          case TesterMode.Ble:
            pendingMode = action.mode
            state.activeTester?.SetMode(action.mode, DEFAULT_TIMEOUT_MS)
            break

          // Extended Blt24Mode from this application only
          case ExtendedTesterMode.Sensitivity:
            pendingMode = ExtendedTesterMode.Sensitivity
            state.activeTester?.SetMode(TesterMode.Dtm, DEFAULT_TIMEOUT_MS)
            break
        }

        store.set("pendingMode", pendingMode)        

        return {
          ...state
        }
      }

      case ACTIVE_TESTER_POWER_CHANGED:
        return {
          ...state,
          powerDbm: action.powerDbm
        }

      case ACTIVE_TESTER_ATTENUATION_CHANGED:
        return {
          ...state,
          attenuationDb: action.attenuationDb,
        }

      case ACTIVE_TESTER_GENERATOR_CHANGED:
        return {
          ...state,
          generatorActive: action.active,
          generatorPowerDbm: action.powerDbm
        }

      case ACTIVE_TESTER_DTM_MODE_CHANGED:
        return {
          ...state,
          dtmMode: action.mode
        }

      case ACTIVE_TESTER_DTM_CHANGED:
        return {
          ...state,
          dtmPer: action.per,
          dtmCount: action.count,
          dtmIntervalMs: action.intervalMs,
          dtm: action
        }

      case SET_ACTIVE_TESTER_ATTENUATION:
        state.activeTester?.SetAttenuation(action.attenuationDb, DEFAULT_TIMEOUT_MS)
        return state


      default:
          return state
  }
}