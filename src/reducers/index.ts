import { combineReducers } from 'redux';

import api from './ApiReducer'
import app from './AppReducer'
import dtm from './DtmReducer'
import dut from './DutReducer'
import generator from './GeneratorReducer'
import sensitivity from './SensitivityReducer'
import tester from './TesterReducer';

export default combineReducers({
    app,
    api,
    tester,
    dut,
    generator,
    dtm,
    sensitivity
});
