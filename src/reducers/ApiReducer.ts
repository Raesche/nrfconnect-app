/* eslint-disable import/no-cycle */
import { ApiEvent, Dut, DutConfiguration, DutList, Handshake, Parity, Protocol,Specification, Tester, TesterList } from '@arendi/testing'
// import { ConnectionStatus, DutConfiguration, Event as ApiEvent } from '@arendi/testing/dist/src/api/api'
import { getPersistentStore, logger } from 'pc-nrfconnect-shared'

import { api } from '../Api'  // possible cyclic import

// Action definitions
// Names are shared and must be unique throughout the whole app!!!
const API_INIT = "API_INIT"
const API_CONNECTED = "API_CONNECTED"
const API_DISCONNECTED = "API_DISCONNECTED"
const API_CONNECTING = "API_CONNECTING"

const API_TESTER_LIST_CHANGED = "API_TESTER_LIST_CHANGED"
const API_DUT_LIST_CHANGED = "API_DUT_LIST_CHANGED"
const API_DUT_CONNECT = "API_DUT_CONNECT"
const API_DUT_DISCONNECT = "API_DUT_DISCONNECT"
const API_DUT_CONNECTION_CHANGED = "API_DUT_CONNECTION_CHANGED"
const API_DUT_DTM_MODE_CHANGED = "API_DUT_DTM_MODE_CHANGED"
const API_DUT_DTM_RESULT_CHANGED = "API_DUT_DTM_RESULT_CHANGED"
const API_SERVER_SETTINGS_CHANGED = "API_SERVER_SETTINGS_CHANGED"

const DEFAULT_SERVER_NAME = 'localhost'
const DEFAULT_SERVER_PORT = 5000
const POLL_INTERVAL = 5000
const RETRY_INTERVAL = 500
const CONNECT_TIMEOUT = 2000

let dispatch
let pollTimer
const store = getPersistentStore()

const CloseHandler = () => {
  // console.log("Api closed")
  dispatch?.({type: API_DISCONNECTED, api})
  clearInterval(pollTimer)
  pollTimer = setInterval(PollHandler, POLL_INTERVAL)
}

const OpenHandler = () => {
  // console.log("Api opened", api)
  dispatch?.({type: API_CONNECTED, api})
  clearInterval(pollTimer)
}

const PollHandler = () => {
  // console.log("Connecting to Api...")
  dispatch?.({type: API_CONNECTING, api})
}

const DutConnectionHandler = (message:any) => {
  // console.log("DUT_CONNECTION_HANDLER", message)
  dispatch?.({type: API_DUT_CONNECTION_CHANGED, ...message})
}

const DutDtmModeHandler = (message:any) => {
  // console.log("DUT_DTM_MODE_HANDLER", message)
  dispatch?.({type: API_DUT_DTM_MODE_CHANGED, ...message})
}

const DutDtmResultHandler = (message:any) => {
  // console.log("DUT_DTM_RESULT_HANDLER", message)
  dispatch?.({type: API_DUT_DTM_RESULT_CHANGED, ...message})
}

const TesterListHandler = (list:Tester[]) => {
  // console.log("Tester list received", list)
  dispatch?.({type: API_TESTER_LIST_CHANGED, list})
}

const DutListHandler = (list:Dut[]) => {
  // console.log("DUT list received", list)
  dispatch({type: API_DUT_LIST_CHANGED, list})
}


export const Init = dispatchRef => {
  if (!dispatchRef)
    throw new Error("Error in ApiReducer.Init(): dispatchRef must be defined")

  dispatch = dispatchRef
  dispatch({type: API_INIT})
}

export const DutConnect = (
  identifier: string,
  config: DutConfiguration,
  timeoutMs?: number) => ({
  type: API_DUT_CONNECT,
  identifier,
  config,
  timeoutMs
})

export const DutDisconnect = (
  identifier: string,
  timeoutMs?: number) => ({
  type: API_DUT_DISCONNECT,
  identifier,
  timeoutMs
})

export const SetServerSettings = (
  name: string,
  port: number) => ({
  type: API_SERVER_SETTINGS_CHANGED,
  name,
  port
})


const initialState = {
  isConnected: false,
  isConnecting: false,
  TesterList: undefined as unknown as TesterList,
  testerList: [] as Tester[],
  DutList: undefined as unknown as DutList,
  dutList: [] as Dut[],
  serverName: store.get("serverName")?? DEFAULT_SERVER_NAME,
  serverPort: store.get("serverPort")?? DEFAULT_SERVER_PORT
}


export default (state = initialState, {type, ...action}) => {
  switch(type) {

    case API_INIT: {
      api.OnOpen(OpenHandler)
      api.OnClose(CloseHandler)
      api.On(ApiEvent.DutConnectionEvent, DutConnectionHandler)
      api.On(ApiEvent.DutDtmModeEvent, DutDtmModeHandler)
      api.On(ApiEvent.DutDtmEvent, DutDtmResultHandler)
      api.TesterList.OnListChanged(TesterListHandler)
      api.DutList.OnListChanged(DutListHandler)
      const url = `ws://${state.serverName}:${state.serverPort}/blt24`
      api.iface.Open({url, protocols:undefined, retryMs:RETRY_INTERVAL}, CONNECT_TIMEOUT)
      return {
        ...state,
        TesterList: api.TesterList,
        testerList: api.TesterList.list,
        DutList: api.DutList,
        dutList: api.DutList.list
      }
    }

    case API_CONNECTED:
      logger.info('Connected to WebSocket server')
      api.GetTesterList(2000)
      api.GetDutList(2000)
      return {
        ...state,
        isConnected: true,
        isConnecting: false
      }

    case API_DISCONNECTED:
      logger.info('Disconnected from WebSocket server')
      return {
        ...state,
        isConnected: false,
        isConnecting: false
      }

    case API_CONNECTING: {
      logger.info('Connecting to server...')
      const url = `ws://${state.serverName}:${state.serverPort}/blt24`
      api.iface.Open({url, protocols:undefined, retryMs:RETRY_INTERVAL}, CONNECT_TIMEOUT)
      return {
        ...state,
        isConnecting: true
      }
    }

    case API_SERVER_SETTINGS_CHANGED: {
      const serverName = action.name?? DEFAULT_SERVER_NAME
      const serverPort = action.port?? DEFAULT_SERVER_PORT
      store.set("serverName", serverName)
      store.set("serverPort", serverPort)
      return {
        ...state,
        serverName,
        serverPort
      }
    }

    case API_TESTER_LIST_CHANGED:
      logger.info(`TesterList received`)
      action.list.forEach((tester:Tester) => logger.info(` - ${tester.serialNumber} (${tester.productName})`))
      return {
        ...state,
        testerList: action.list
      }

    case API_DUT_LIST_CHANGED:
      logger.info(`DutList received`)
      action.list.forEach((dut:Dut) => logger.info(` - ${dut.identifier} (${dut.description})`))
      return {
        ...state,
        dutList: action.list
      }

    case API_DUT_CONNECT:
      // TODO: CHeck if not already open
      logger.info(`Connecting ${action.identifier} with ${Protocol.Name(action.config.protocol)}, ${action.config.baudrate}, handshake ${Handshake.Name(action.config.handshake)}, parity ${Parity.Name(action.config.parity)}, specification ${Specification.Name(action.config.specification)}`)
      api.DutConnect(action.identifier, action.config, action.timeoutMs)
      return state

    case API_DUT_DISCONNECT:
      // TODO: CHeck if not already closed
      logger.info(`Disconnecting ${action.identifier}`)
      api.DutDisconnect(action.identifier, action.timeoutMs)
      return state

    case API_DUT_CONNECTION_CHANGED:
      return {
        ...state,
        dutList: [...api.DutList.list]
      }

    case API_DUT_DTM_MODE_CHANGED:
      return {
        ...state,
        dutList: [...api.DutList.list]
      }

    case API_DUT_DTM_RESULT_CHANGED:
      return {
        ...state,
        dutList: [...api.DutList.list]
      }

    default:
        return state
  }
}