/* eslint-disable prefer-template */
/* eslint-disable simple-import-sort/imports */
/* eslint-disable no-shadow */
import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { SetActiveTesterMode, ExtendedTesterMode } from './reducers/TesterReducer'
import { setCurrentPane } from 'pc-nrfconnect-shared'

import Box from '@mui/material/Box'
import List from '@mui/material/List'
import ListItemButton from '@mui/material/ListItemButton'
import ListItemIcon from '@mui/material/ListItemIcon'
import ListItemText from '@mui/material/ListItemText'
import DtmIcon from '@mui/icons-material/VerifiedOutlined'
import PowerMeterIcon from '@mui/icons-material/SpeedOutlined'
import SensitivityIcon from '@mui/icons-material/BarChart'
import SignalGeneratorIcon from '@mui/icons-material/Podcasts'
import AttenuatorIcon from '@mui/icons-material/Tune'
import EmcIcon from '@mui/icons-material/FlashOnOutlined'

function Blt24SetMode() {

    const dispatch = useDispatch()
    
    const SetMode = mode => {        
        dispatch(SetActiveTesterMode(mode))
        dispatch(setCurrentPane(0))
    }

  // get active tester from store
  const {activeTester:tester, mode} = useSelector(state => state.app.tester)

  return (
      <div>       
          <Box sx={{ width: '100%', maxWidth: 360, bgcolor: 'background.paper' }} >
              <List>
                  <ListItemButton disabled={tester?.serialNumber === undefined} selected={mode === ExtendedTesterMode.Dtm} onClick={() => SetMode(ExtendedTesterMode.Dtm)}>
                      <ListItemIcon><DtmIcon /></ListItemIcon>
                      <ListItemText primary="DTM"/>
                  </ListItemButton>
                  <ListItemButton disabled={tester?.serialNumber === undefined} selected={mode === ExtendedTesterMode.Attenuator} onClick={() => SetMode(ExtendedTesterMode.Attenuator)}>
                      <ListItemIcon><AttenuatorIcon /></ListItemIcon>
                      <ListItemText primary="Attenuator"/>
                  </ListItemButton>
                  <ListItemButton disabled={tester?.serialNumber === undefined} selected={mode === ExtendedTesterMode.PowerMeter} onClick={() => SetMode(ExtendedTesterMode.PowerMeter)}>
                      <ListItemIcon><PowerMeterIcon /></ListItemIcon>
                      <ListItemText primary="Power Meter"/>
                  </ListItemButton>
                  <ListItemButton disabled={tester?.serialNumber === undefined} selected={mode === ExtendedTesterMode.Generator} onClick={() => SetMode(ExtendedTesterMode.Generator)}>
                      <ListItemIcon><SignalGeneratorIcon /></ListItemIcon>
                      <ListItemText primary="Signal Generator"/>
                  </ListItemButton>
                  <ListItemButton disabled={tester?.serialNumber === undefined} selected={mode === ExtendedTesterMode.Sensitivity} onClick={() => SetMode(ExtendedTesterMode.Sensitivity)}>
                      <ListItemIcon><SensitivityIcon /></ListItemIcon>
                      <ListItemText primary="Sensitivity"/>
                  </ListItemButton>
                  <ListItemButton disabled={tester?.serialNumber === undefined} selected={mode === ExtendedTesterMode.Ble} onClick={() => SetMode(ExtendedTesterMode.Ble)}>
                      <ListItemIcon><EmcIcon /></ListItemIcon>
                      <ListItemText primary="EMC Testing"/>
                  </ListItemButton>
              </List>
          </Box>

      </div>
  );
}

Blt24SetMode.propTypes = {
};

export default Blt24SetMode