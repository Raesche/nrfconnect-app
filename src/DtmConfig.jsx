/* eslint-disable react/no-unused-prop-types */
/* eslint-disable simple-import-sort/imports */
/* eslint-disable no-shadow */
import React, { useState, useEffect } from 'react'
import { DtmBytes, DtmChannel, DtmPattern, DtmPhy, DtmDefaultConfig } from '@arendi/testing'
import ChannelSelect from './ChannelSelect'
import PhySelect from './PhySelect'
import PatternSelect from './PatternSelect'
import LengthSelect from './LengthSelect'
import IntervalSelect from './IntervalSelect'
import Stack from '@mui/material/Stack'

const defaultChannel = DtmDefaultConfig.channel
const defaultPattern = DtmDefaultConfig.pattern
const defaultLength = DtmDefaultConfig.bytes
const defaultPhy = DtmDefaultConfig.phy
const defaultInterval = 1000

const styles = {marginRight: "5px", marginBottom: "10px"}

const DtmConfig = React.memo(({channels=defaultChannel, bytes=defaultLength, pattern=defaultPattern, phy=defaultPhy, interval=defaultInterval, multipleChannels=false, onChange=null, disabled=false, children}) => {

    const [selectedChannels, SetSelectedChannels] = useState(channels)
    const [selectedLength, SetSelectedLength] = useState(bytes)
    const [selectedPattern, SetSelectedPattern] = useState(pattern)
    const [selectedPhy, SetSelectedPhy] = useState(phy)
    const [selectedInterval, SetSelectedInterval] = useState(interval)
    useEffect(() => {
        onChange?.({
            channels: selectedChannels,
            bytes: selectedLength,
            pattern: selectedPattern,
            phy: selectedPhy,
            interval: selectedInterval
        })
    // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [selectedChannels, selectedLength, selectedPattern, selectedPhy, selectedInterval])

    return (        
        <Stack direction="row" spacing={0} sx={{ flexWrap:"wrap"}}>      {/* flexWrap see: https://smartdevpreneur.com/the-ultimate-guide-to-the-new-mui-stack-component/ */}
            <ChannelSelect
                selected={selectedChannels}
                multiple={multipleChannels}
                onChange={SetSelectedChannels}
                disabled={disabled}
                style={styles}
            />

            <PatternSelect
                selected={selectedPattern}
                onChange={SetSelectedPattern}
                disabled={disabled}
                style={styles}
            />

            <LengthSelect
                selected={selectedLength}
                onChange={SetSelectedLength}
                disabled={disabled}
                style={styles}
            />

            <PhySelect
                selected={selectedPhy}
                onChange={SetSelectedPhy}
                disabled={disabled}
                style={styles}
            />

            <IntervalSelect
                selected={selectedInterval}
                onChange={SetSelectedInterval}
                disabled={disabled}
                style={styles}
            />

            {children}
        </Stack>
  )
})


DtmConfig.propTypes = {
  channels: [DtmChannel.Ch0],
  bytes: DtmBytes.B37,
  pattern: DtmPattern.Prbs9,
  phy: DtmPhy.Phy1Mbps,
  interval: 1000,
  multipleChannels: false,
  onChange: null,
  children: null,
  disabled: false
}

export default DtmConfig