/* eslint-disable max-classes-per-file */
/* eslint-disable simple-import-sort/imports */
import React, {useEffect, useState, useRef} from 'react'

import MenuItem from '@mui/material/MenuItem'
import Select from '@mui/material/Select'
import ListItemText from '@mui/material/ListItemText'
import InputLabel from '@mui/material/InputLabel'
import FormControl from '@mui/material/FormControl'

import ListItemIcon from '@mui/material/ListItemIcon'
import NotCheckedIcon from '@mui/icons-material/CheckBoxOutlineBlankOutlined'
import CheckedIcon from '@mui/icons-material/CheckBoxOutlined'

const ITEM_HEIGHT = 48
const ITEM_PADDING_TOP = 8
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      minWidth: 150
    }
  }
}

// Converts an array of selected items to a readable string
// selectedItems: Array of selected items that should be added to the string. This array must contain the "value" properties of the selected items
// items: Array of all selectable items. This array must contain objects with a "text" and "value" property
// maxChars: The max number of characters the returning string can have. If maxChars are exceeded, the string will be trimmed and marked with …
const ItemsToNames = (selectedItems=[], items=[], maxChars=50) => {
    const str = selectedItems.map(currentSelectedItem => {
        const item = items.find(currentItem => currentItem.value === currentSelectedItem)
        return item? item.text : ""
    }).join(', ')
        
    return (str.length <= maxChars)? str : `${str.slice(0, maxChars)}…`
}

// Make sure, provided data is always converted to an array
const MakeArray = data => (data instanceof Array)? data : [data]

// Sort array of items numerically, convert to string for comparison
const Sort = items => items.sort((a, b) => `${a}`.localeCompare(b, undefined, { numeric:true }))

// Component definition for creating reusable selections
//  label: String to be displayed above the selection component
//  onChange: Callback function, called when the user selection changes.
//            The callback must accept one parameter that holds the selection: A single value if multiple=false, an array of values otherwise
//  items: An array of selectable items with text and value properties: [{text:"Item1", value:1}, {text:"Item2", value2},..]
//  selected: The pre-selected value if multiple=false or an array of pre-selected values if multiple=true
//  multiple: Set to true to allow selection of multiple items
//  disabled: Set to true to disable component
//  style: Provide custom styles
//  maxChars: The max number of characters the component should display (defaults to 50).
//            If too many items are selected the list will be truncated and marked with …
//  sort: Set to true to allow sorting of selected items, otherwise they will be displayed in the order of selection          
const Selection = React.memo(({label="", onChange=null, items=[], selected=null, multiple=false, disabled=false, style={}, maxChars=50, sort=false}) => {
    
    // Error handling
    if (!(items instanceof Array))
        throw new Error("Error in <Selection>: 'items' must be an array containing elements with 'text' and 'value' properties")

    if (multiple && !(selected instanceof Array))
        throw new Error("Error in <Selection>: 'selected' must be an array with values when 'multiple' is set to true")
    
    if (!multiple && selected instanceof Array)
        throw new Error("Error in <Selection>: 'selected' must not be an array when 'multiple' is set to false")
    
    // Hack: Somehow the input element of the MUI select component is not properly hided when the 
    // component is disabled. So we need to find the input element and set the visibility to "none"
    const inputRef = useRef()
    useEffect(() => {
        if (inputRef.current) {
            const inputElement = [...inputRef.current.children].find(node => node.nodeName === "INPUT")
            if (inputElement)
                inputElement.style.display = "none"
        }
    }, [inputRef])

    
    
    // selection is an array containing all currently selected items
    const [selection, SetSelection] = useState(MakeArray(selected))
    
    // Update selection array whenever 'selected' attribute changes
    // Make sure, selection is always an array, even if it contains no items
    useEffect(() => {
        SetSelection((selected === undefined)? [null] : MakeArray(selected))        
    }, [selected])

    // Create UI list containing all items
    const itemList = items.map(({text, value}) => {
        return (
            <MenuItem value={value}>
                <ListItemIcon>{(selection.find(el => el === value) !== undefined)? <CheckedIcon/> : <NotCheckedIcon />}</ListItemIcon>
                <ListItemText primary={text} />
            </MenuItem>
        )
    })

    // Called whenever the user selection has changed
    // If the 'onChange' attribute is defined, the callback function is executed with the following arguments:
    //  - the value of the selected item, if attribute 'multiple' is set to false (default)
    //  - an array containing the values of all selected items if attribute 'multiple' is set to true
    const ChangeHandler = event => {
        SetSelection(MakeArray(event.target.value))
        onChange?.(event.target.value)
    }

    return (
    
        <FormControl disabled={disabled}>
            <InputLabel>{label}</InputLabel>
            <Select
                ref={inputRef}
                value={selection}
                label={label}
                size="small"
                multiple={multiple}
                MenuProps={MenuProps}
                renderValue={selectedItems => ItemsToNames(sort? Sort(selectedItems): selectedItems, items, maxChars)}
                onChange={ChangeHandler}
                sx={style}
            >
                {itemList}
            </Select>
        </FormControl>

    )
})

Selection.propTypes = {
    label: "",
    onChange: null,
    items: [],
    selected: [],
    multiple: false,
    disabled: false,
    style: {},
    maxChars: 50,
    sort: false
}

export default Selection