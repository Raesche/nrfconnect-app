/* eslint-disable no-nested-ternary */
/* eslint-disable no-plusplus */
/* eslint-disable no-await-in-loop */
/* eslint-disable simple-import-sort/imports */
/* eslint-disable no-shadow */
import React, { useEffect, useRef, useState } from 'react'
import { useSelector } from 'react-redux'
import Alert from '@mui/material/Alert'
import AlertTitle from '@mui/material/AlertTitle'
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import List from '@mui/material/List'
import Divider from '@mui/material/Divider'
import ListItem from '@mui/material/ListItem'
import IconButton from '@mui/material/IconButton'
import ListItemText from '@mui/material/ListItemText'
import ListItemAvatar from '@mui/material/ListItemAvatar'
import Avatar from '@mui/material/Avatar'
import CircularProgress from '@mui/material/CircularProgress'
import { blue, red, green } from '@mui/material/colors'

import SerialNumberIcon from '@mui/icons-material/Fingerprint'
import ProductNameIcon from '@mui/icons-material/Sell'
import FirmwareVersionIcon from '@mui/icons-material/Memory'
import HardwareVersionIcon from '@mui/icons-material/Build'
import ProgramIcon from '@mui/icons-material/Download'
import SuccessIcon from '@mui/icons-material/Done'
import FailIcon from '@mui/icons-material/Clear'


import {
    Api,
    Interface,
    WsInterface,
    Tester,
    TesterMode,
    TesterTxPower,
    DtmMode,
    DtmChannel,
    Sensitivity
  } from '@arendi/testing';

const TIMEOUT_MS = 5000;


  const Between = (value = 0, min = 0, max = 0, unit = '') => {
    const color = value < min ? KYEL : value > max ? KRED : KGRN;
    return UseColor(color, `${value.toFixed(1)} ${unit}`);
  };
  
  class TesterProd extends Tester {
    constructor(
      api,
      serialNumber = '',
      args = {
        productName: '',
        hardwareId: '',
        hardwareVersion: { major: 0, minor: 0 },
        firmwareVersion: { major: 0, minor: 0 },
        mode: TesterMode.Idle,
        dtmMode: DtmMode.Idle,
        dtmConfig: {}, // TODO set default value
        attenuationDb: 0
      }
    ) {
      super(api, serialNumber, args);
    }
  
    SetServiceAttenuation(sw1 = false, sw2 = false, sw3 = false, level = 0) {
      this.api.iface.Write({
        version: { major: 0, minor: 0 },
        type: 'TesterServiceAttenuationSetRequest',
        data: {
          serialNumber: this.serialNumber,
          switch1: sw1 ?? false,
          switch2: sw2 ?? false,
          switch3: sw3 ?? false,
          level: level ?? 0
        }
      });
    }
  }
  
  // Read specified number of power samples and return average
  const ReadAveragePower = async (tester, numSamples = 1) => {
    let avgPowerDbm = 0;
    for (let sample = 0; sample < numSamples; sample++) {
      const { powerDbm } = await tester.ReadPowerMeter(TIMEOUT_MS);
      avgPowerDbm += powerDbm;
    }
    return avgPowerDbm / numSamples;
  };

function ProductionTest() {

  // get active tester from store
  const { list, activeTester, favoriteTester } = useSelector(state => state.app.tester)

  
  const { isConnected, testerList, TesterList } = useSelector(state => state.app.api)
  const [ connectionLost, SetConnectionLost ] = useState(false)
  const [ testerMissing, SetTesterMissing ] = useState(false)

  const [activeDut, SetActiveDut] = useState(false);
  const handleChange = event => {
    SetActiveDut(TesterList.Get(event.target.value));
  };

  const dutList = testerList.filter(dut => {
    return dut !== activeTester
  })

  


  const stateRef = useRef()
  stateRef.current = {isConnected, testerMissing:!testerList.length}

  const [activeStep, setActiveStep] = useState(3);
  const handleNext = () => {
    setActiveStep(prevActiveStep => prevActiveStep + 1);
  };

  const handleBack = () => {
    setActiveStep(prevActiveStep => prevActiveStep - 1);
  };

  const handleReset = () => {
    setActiveStep(0);
  };

  console.log("DUTLIST", dutList)
  // Automatically select first entry in DUT list
  useEffect(() => {
    console.log("SELECTDUTLIST", dutList?.[0]?.serialNumber)
    SetActiveDut(dutList?.[0])
  }, [dutList]) // eslint-disable-line react-hooks/exhaustive-deps
  
  // Handling error signalisations
  useEffect(() => {
    // Set connectionLost if Api is disconnected for more than 2 seconds
    // Reset connectionLost immediately, when Api is connected
    isConnected
        ? SetConnectionLost(false)
        : setTimeout(() => SetConnectionLost(!stateRef.current.isConnected), 2000)

    // Set testerMissing if no Tester is connected for more than 2 seconds
    // Reset testerMissing immediately, when a Tester is connected
    testerList.length
        ? SetTesterMissing(false)
        : setTimeout(() => SetTesterMissing(stateRef.current.isConnected && stateRef.current.testerMissing), 2000)
  }, [isConnected, testerList])


  /* Select BLT2450 DUT component */
  const dutListComponent = dutList?.map(dut => {
    return <MenuItem
        value={dut.serialNumber}>
        {dut.serialNumber}
    </MenuItem>
  })

return (
    <div style={{position:"relative"}}>

        { /* Server not running component */}
        {connectionLost &&
        <Alert variant="filled" severity="warning">
            <AlertTitle>Server not running</AlertTitle>
            In order to run this application, the server must be started
        </Alert>
        }


        { /* DUT selection and info */ }
        <List
            sx={{
                width: '100%',
                maxWidth: 360,
                bgcolor: 'background.paper',
            }}
        >
            { /* DUT selection */ }
            <ListItem>
                <FormControl fullWidth>
                    <InputLabel id="demo-simple-select-label">DUT</InputLabel>
                    <Select
                        labelId="demo-simple-select-label"
                        id="demo-simple-select"
                        value={activeDut? activeDut.serialNumber : "1234"}    // must use dummy value to prevent error
                        label="DUT"
                        onChange={handleChange}
              >
                        {dutListComponent}
                    </Select>
                </FormControl>
            </ListItem>

            <Divider variant="inset" component="li" />
            
            { /* Product name */ }
            <ListItem>
                <ListItemAvatar>
                    <Avatar sx={{ bgcolor: blue[500] }}>
                        <ProductNameIcon />
                    </Avatar>
                </ListItemAvatar>
                <ListItemText primary="Product name" secondary={activeDut?.productName} />
            </ListItem>

            <Divider variant="inset" component="li" />

            { /* Serial Number */ }
            <ListItem>
                <ListItemAvatar>
                    <Avatar sx={{ bgcolor: blue[500] }}>
                        <SerialNumberIcon />
                    </Avatar>
                </ListItemAvatar>
                <ListItemText primary="Serial number" secondary={activeDut?.serialNumber} sx={{ userSelect:"text" }}/>
            </ListItem>

            <Divider variant="inset" component="li" />

            { /* Hardware Version */ }
            <ListItem>
                <ListItemAvatar>
                    <Avatar sx={{ bgcolor: blue[500] }}>
                        <HardwareVersionIcon />
                    </Avatar>
                </ListItemAvatar>
                <ListItemText primary="Hardware version" secondary={activeDut? `${activeDut.hardwareVersion.major}.${activeDut.hardwareVersion.minor}` : '-.--'} sx={{ userSelect:"text" }}/>
            </ListItem>

            <Divider variant="inset" component="li" />

            { /* Firmware Version */ }
            <ListItem
                secondaryAction={
                    <IconButton edge="end" aria-label="update" disabled>
                        <ProgramIcon />
                    </IconButton>
                      }>
                <ListItemAvatar>
                    <Avatar sx={{ bgcolor: blue[500] }}>
                        <FirmwareVersionIcon />
                    </Avatar>
                </ListItemAvatar>
                <ListItemText primary="Firmware version" secondary={activeDut? `${activeDut.firmwareVersion.major}.${activeDut.firmwareVersion.minor}` : '-.--'} sx={{ userSelect:"text" }}/>
            </ListItem>
        </List>

        <br/>

        { /* DUT measurement results */ }
        <List
            sx={{
                width: '100%',
                maxWidth: 360,
                bgcolor: 'background.paper',
            }}
        >

            { /* Output power */ }
            <ListItem secondaryAction={<SuccessIcon fontSize="medium" sx={{color: green[500]}}/>}> 
                <ListItemText primary="Output power" secondary="-8.8 dBm"/>
            </ListItem>

            <Divider component="li" />

            { /* Attenuator 1 */ }
            <ListItem secondaryAction={<FailIcon fontSize="medium" sx={{color: red[500]}}/>}>
                <ListItemText primary="Attenuator 1" secondary="1.2 dB"/>
            </ListItem>

            <Divider component="li" />

            { /* Attenuator 2 */ }
            <ListItem secondaryAction={<SuccessIcon fontSize="medium" sx={{color: green[500]}}/>}>  
                <ListItemText primary="Attenuator 2" secondary="30.0 dB"/>
            </ListItem>

            <Divider component="li" />

            { /* Attenuator 3 */ }
            <ListItem secondaryAction={<CircularProgress size={25}/>}>                          
                <ListItemText primary="Attenuator 3" secondary="--.- dB"/>
            </ListItem>
        </List>
    
    </div>
  )
}

ProductionTest.propTypes = {
}

export default ProductionTest