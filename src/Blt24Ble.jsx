/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable react/no-unused-prop-types */
/* eslint-disable simple-import-sort/imports */
/* eslint-disable no-shadow */
import React, { useCallback, useEffect, useRef, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { SetActiveTesterAttenuation} from './reducers/TesterReducer';

import { blue, grey } from '@mui/material/colors';
import Card from '@mui/material/Card';
import CardHeader from '@mui/material/CardHeader';
import CardContent from '@mui/material/CardContent';
import CardActions from '@mui/material/CardActions'
import FormControl from '@mui/material/FormControl'
import Tooltip from '@mui/material/Tooltip'
import Stack from '@mui/material/Stack'
import Grid from '@mui/material/Grid'
import Button from '@mui/material/Button'
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import ListSubheader from '@mui/material/ListSubheader';
import Switch from '@mui/material/Switch';
import ListItemButton from '@mui/material/ListItemButton';

import ArrowBackIcon from '@mui/icons-material/KeyboardDoubleArrowLeft'
import ArrowForwardIcon from '@mui/icons-material/KeyboardDoubleArrowRight'
import ChevronLeftIcon from '@mui/icons-material/ChevronLeft'
import ChevronRightIcon from '@mui/icons-material/ChevronRight'
import IconButton from '@mui/material/IconButton'
import MoreVertIcon from '@mui/icons-material/MoreVert'
import SnapshotIcon from '@mui/icons-material/CameraAlt'
// import RssiPoor from '@mui/icons-material/Wifi1Bar';
// import RssiOk from '@mui/icons-material/Wifi2Bar';
import RssiGood from '@mui/icons-material/Wifi';


import Chart from './Chart'
import Knob from './Knob'


function Blt24Ble() {
  const dispatch = useDispatch()

  // get Tester data from store
  const { attenuationDb } = useSelector(state => state.app.tester)
  const chartRef = useRef(null)
  const [attenuationSerie, SetAttenuationSerie] = useState([])
  const [timer, SetTimer] = useState(0)


  // Initialize component
  useEffect(() => {    
    // Make sure chart is regularly updated, even if attenuation has not been changed
    const interval = setInterval(() => (SetTimer(value => value + 1)), 200)
    
    // Cleanup when dismounted
    return () => clearInterval(interval)
  }, [])  // eslint-disable-line react-hooks/exhaustive-deps

  // Update chart when attenuation has changed or on timer event
  useEffect(() => {
    SetAttenuationSerie(serie => [...serie, [new Date().toISOString(), attenuationDb]].splice(-300, 300))

  }, [timer]) // eslint-disable-line react-hooks/exhaustive-deps


  // Export SVG image
  const ExportImage = () => {
    chartRef.current?.ExportImage("Attenuator.svg")
  }
  
  const SetAttenuation = useCallback(value => {    
    value = Math.min(Math.max(value, 0), 120)
    dispatch(SetActiveTesterAttenuation(value))
  }, []) // eslint-disable-line react-hooks/exhaustive-deps



  return (
      <Grid container spacing={2} sx={{ minWidth: 400, maxWidth: 1000, mt: 0 }}>
          {/* BLT24 tester controls */}
          <Grid item xs="auto">

              {/* Attenuator knob and buttons */}
              <Card sx={{ minWidth: 300, maxWidth: 300, minHeight: 400 }}>
                  <CardHeader
                      action={
                          <IconButton aria-label="settings">
                              <MoreVertIcon />
                          </IconButton>
                        }
                      title="Attenuator"
                      subheader="Set BLT2450 attenuation"
                    />

                  <CardContent sx={{ minHeight: 300, display: "flex", flexDirection: "column" }}>
                      <Grid container spacing={2}>

                          {/* PER and attenuation knobs */}
                          <Grid item xs={12}>
                              <Knob
                                  value={attenuationDb}
                                  min={0}
                                  max={120}
                                  start={-45}
                                  stop={45}
                                  segmentColor={blue[500]}
                                  valueColor={blue[500]}
                                  labelColor={grey[500]}
                                  label="dB"
                                  step={0.25}
                                  onChanged={SetAttenuation}
                                  width="200"
                                  height="200"
                                  />
                          </Grid>
                            
                          <Grid item xs={12} sx={{ marginTop:"auto" }}>
                              <Stack direction="row" justifyContent="space-evenly" spacing={1}>
                                  <IconButton onClick={() => SetAttenuation(attenuationDb-1)}><ArrowBackIcon /></IconButton>
                                  <IconButton onClick={() => SetAttenuation(attenuationDb-0.25)}><ChevronLeftIcon /></IconButton>
                                  <IconButton onClick={() => SetAttenuation(attenuationDb+0.25)}><ChevronRightIcon /></IconButton>
                                  <IconButton onClick={() => SetAttenuation(attenuationDb+1)}><ArrowForwardIcon /></IconButton>
                              </Stack>
                          </Grid>
                      </Grid>

                  </CardContent>
              </Card>
          </Grid>

          {/* BLE controls */}
          <Grid item xs>
              {/* Peripheral list */}
              <Card sx={{ minWidth: 300, minHeight: 400}}>
                  <CardHeader
                      action={
                          <IconButton aria-label="settings">
                              <MoreVertIcon />
                          </IconButton>
                        }
                      title="Peripherals"
                      subheader="Select peripheral for connection"
                    />

                  <CardContent sx={{ minHeight: 300, display: "flex", flexDirection: "column" }}>
                      <Grid container spacing={2}>

                          {/* Peripheral list */}
                          <Grid item xs={12}>

                              <List>
                                  <ListItemButton selected={false} onClick={() => null}>
                                      <ListItemIcon><RssiGood /></ListItemIcon>
                                      <ListItemText primary="unknown" secondary="68:64:D4:64:E3:B5"/>
                                      <ListItemText primary="RSSI" secondary="-78"/>
                                  </ListItemButton>

                                  <ListItemButton selected onClick={() => null}>
                                      <ListItemIcon><RssiGood /></ListItemIcon>
                                      <ListItemText primary="unknown" secondary="42:AF:6F:A4:E5:CC"/>
                                      <ListItemText primary="RSSI" secondary="-83"/>
                                  </ListItemButton>

                                  <ListItemButton selected={false} onClick={() => null}>
                                      <ListItemIcon><RssiGood /></ListItemIcon>
                                      <ListItemText primary="SmartWatch" secondary="4F:33:27:EA:06:12"/>
                                      <ListItemText primary="RSSI" secondary="-45"/>
                                  </ListItemButton>

                              </List>
                          </Grid>
                      </Grid>

                  </CardContent>
              </Card>
          </Grid>


          {/* RSSI chart */}
          <Grid item xs={12}>
              <Card sx={{ minWidth: 400, mb: 0 }}>
                  <CardContent>
                      <Grid container spacing={2}>
                          <Grid item xs={12}>
                              <Chart
                                  ref={chartRef}
                                  time
                                  yMin={-100}
                                  yMax={10}
                                  height={400}
                                  yAxisUnit="dBm"
                                  series={[{name:"RSSI [dBm]", data: attenuationSerie}]}
                              />
                          </Grid>
                      </Grid>

                  </CardContent>



                  <CardActions>
                      <Stack spacing={2} sx={{width:"100%", maxwidth:800}}>

                          <Stack direction="row" spacing={2}>                         

                   
                              {/* Save SVG image */}
                              <FormControl sx={{ml:4}} size="small" >
                                  <Tooltip title="Save snapshot" enterDelay={500}>
                                      <IconButton onClick={ExportImage}><SnapshotIcon fontSize="medium"/></IconButton>
                                  </Tooltip>
                              </FormControl>

                          </Stack>
                      </Stack>
                  </CardActions>
              </Card>
          </Grid>
      </Grid>
  )
}

Blt24Ble.propTypes = {

}

export default Blt24Ble