const fs = require('fs')
// const PDFDocument = require('pdfkit') // import does not work in this case...
const PDFDocument = require('pdfkit-table')
const SVGtoPDF = require('svg-to-pdfkit')

PDFDocument.prototype.addSVG = function(svg, x, y, options) {
  return SVGtoPDF(this, svg, x, y, options), this;
}
const { DtmPattern, DtmPhy, DtmDefaultConfig, DtmChannel, Handshake, Parity, Specification, Protocol } = require('@arendi/testing')

// Truncate long strings to maxLength-1 characters and append …
const ShortString = (str, maxLength) => (str?.length <= maxLength)? str : `${str?.substring(0, maxLength-1)}\u2026`

// electron / web only:
import Blt24Image from '../resources/BLT2450.png'

// node.js only
// const Blt24Image = fs.readFileSync('BLT2450.png')
// const SampleDiagram = fs.readFileSync('diagram.svg', 'utf-8')


const description = `This measurement evaluates the sensitivity of the device under test at a packet error rate (PER) of 30.8%, corresponding to 1% bit error rate (BER). DUTs with high sensitivity have a higher link budget which increases the communication range and immunity to nearby interferers. Measuring the sensitivity of the low, mid and high band gives an idea how well the DUT is matched over the whole frequency band.`



class Report {

  constructor() {

  }

  async Create({
      onSuccess=null,
      directory="",
      name="report",
      date=new Date(),
      dtmConfig=DtmDefaultConfig,
      measurements=[],
      svg=SampleDiagram,
      productName="--",
      serialNumber="--",
      hardwareVersion=undefined,
      firmwareVersion=undefined,
      dut=null}) {
        
    const colors = {
      red: '#e63946',
      blue1: '#f1faee',
      blue2: '#a8dadc',
      blue3: '#0077b6',
      blue4: '#1d3557',
      grey1: '#efefef'
    }
    
    const fileName = `${directory}\\${name}.pdf`
    const doc = new PDFDocument({size: 'A4'})
    const ml = 50                       // margin left
    const mr = 50                       // margin right
    const pw = doc.page.width           // page width
    const ph = doc.page.height          // page height
    const cw = pw - ml - mr             // content width
    const stream = fs.createWriteStream(fileName)


    // Sort measurements by sensitivity, get best and worst measurements
    const sortedMeasurements = measurements.sort((a, b) => a.sensitivityDbm - b.sensitivityDbm)
    const bestMeasurement = sortedMeasurements[0]
    const worstMeasurement = sortedMeasurements[sortedMeasurements.length-1]

    // Create list containing all DTM channels and merge with available measurement
    const merged = DtmChannel.List().map(channel => {
      const matched = measurements.find(x => x.channel === channel[1])      
      return {
        channel: channel[1],
        sensitivityDbm: matched?.sensitivityDbm?? null,
        per: matched?.per?? null,
      }
    })


    
    // Create header
    doc.rect(0, 0, pw, 230)
      .fill(colors.blue3)
    doc.fontSize(45)
      .fill('#ffffff')
      .text('Sensitivity Report', ml, 130)      
    doc.fontSize(20)
      .fill('#ffffff')
      .text('DTM sensitivity measurement')

    // Add summary and statistics
    doc.fontSize(25)
      .fill('#000000')
      .text('Summary', ml, 350)
    doc.rect(ml, 400, cw, 270)
      .fill(colors.grey1)

      
    // DTM settings summary
    const summaryTable1 = {
      subtitle: "DTM Settings",
      headers: [
        { label: 'parameter', align:"left" },
        { label: 'value', align:"right" },        
      ],
      rows: [
        ["Packet type", DtmPattern.Name(dtmConfig?.pattern)?? "--"],
        ["Packet length", dtmConfig?.bytes? `${dtmConfig.bytes} bytes` : "--"],
        ["Phy", DtmPhy.Name(dtmConfig?.phy)?? "--"],
        ["Interval", dtmConfig?.interval? `${dtmConfig.interval} ms` : "--"],
      ] 
    }
    await doc.table(summaryTable1, {
      hideHeader: true,
      x: ml + 20,
      y: 420,
      columnsSize: [100, 100],
      prepareRow: (row, columnIdx, rowIdx, rowRect, cellRect) => doc.font("Helvetica").fontSize(8),
    })

    // Measurements summary
    const summaryTable2 = {
      subtitle: "Measurements",
      headers: [
        { label: 'parameter', align:"left" },
        { label: 'value1', align:"right" },
        { label: 'value2', align:"right" },     
      ],
      rows: [
        ["Number of measurements", '', measurements.length],
        worstMeasurement
          ? [`Min sensitivity (CH${worstMeasurement.channel})`, `${worstMeasurement.sensitivityDbm?.toFixed(1)?? '--'} dBm`, `${worstMeasurement.per?.toFixed(1)?? '--'} % PER`]
          : [`Min sensitivity (CH --)`, `-- dBm`, `-- % PER`],
        bestMeasurement
          ? [`Max sensitivity (CH${bestMeasurement.channel})`, `${bestMeasurement.sensitivityDbm?.toFixed(1)?? '--'} dBm`, `${bestMeasurement.per?.toFixed(1)?? '--'} % PER`]
          : [`Max sensitivity (CH --)`, `-- dBm`, `-- % PER`]
      ]
    }
    await doc.table(summaryTable2, {
      hideHeader: true,
      x: ml + 20,
      y: 540,
      columnsSize: [100, 50, 50]
    })

    doc.fontSize(9)
      .fill('#000')
      .text(`Created ${date.toISOString().replaceAll("-", ".").replaceAll("T", " ").replaceAll("Z", "")}`, ml + 20, 650)
    


    // Add table page
    doc.addPage()

    // Add Title
    doc.fontSize(25)
      .fill('#000000')
      .text('Measurement results', ml, 80)
      
    // Table showing sensitivity values for channels CH0 .. CH19
    // Use rows instead of datas because it leads to strange line break problems in table with long sensitivity numbers
    // that require different column widths (bug?)
    const resultTable1 = {
      headers: [
        { label: 'Channel', align:"left" },
        { label: 'Frequency', align:"left" },
        { label: 'Sensitivity', align:"center" },     
        { label: 'PER', align:"center" },
      ],
      rows: merged.slice(0, 20).map(measurement => [
        `CH${measurement.channel}`,
        `${2402 + 2*measurement.channel} MHz`,
        `${measurement?.sensitivityDbm?.toFixed(1) || '--'} dBm`,
        `${measurement?.per?.toFixed(1) || '--'} %`
      ])
    }
    await doc.table(resultTable1, {
        x: ml,
        y: 150,
        columnsSize: [50, 50, 70, 50]
    })

    // Table showing sensitivity values for channels CH20 .. CH39
    const resultTable2 = {
      headers: [
        { label: 'Channel', align:"left" },
        { label: 'Frequency', align:"left" },
        { label: 'Sensitivity', align:"center" },     
        { label: 'PER', align:"center" },
      ],
      rows: merged.slice(20, 40).map(measurement => [
        `CH${measurement.channel}`,
        `${2402 + 2*measurement.channel} MHz`,
        `${measurement?.sensitivityDbm?.toFixed(1) || '--'} dBm`,
        `${measurement?.per?.toFixed(1) || '--'} %`
      ])
    }
    await doc.table(resultTable2, {
        x: ml + cw/2 + 20,
        y: 150,
        columnsSize: [50, 50, 70, 50]
    })


    // Add diagram
    doc.addSVG(svg, ml, 520, {width:cw, preserveAspectRatio:"xMinYMin meet"})


    // Add measurement setup page
    doc.addPage()

    // Add Title
    doc.fontSize(25)
      .fill('#000000')
      .text('Measurement setup', ml, 80)
      

    // DUT info table
    const dutInfoTable = {
      subtitle: "DUT info",
      headers: [
        { label: 'parameter', align:"left" },
        { label: 'value', align:"right" },        
      ],
      rows: [
        ["Identifier", dut?.identifier?? "--"],
        ["Description", ShortString(dut?.description?? "--", 50)],
        ["Baudrate", dut?.baudrate?? "--"],
        ["Handshake", Handshake.Name(dut?.handshake)?? "--"],
        ["Parity", Parity.Name(dut?.parity)]?? "--",
        ["Protocol", Protocol.Name(dut?.protocol)?? "--"],
        ["Specification", Specification.Name(dut?.specification)?? "--"]
      ] 
    }
    await doc.table(dutInfoTable, {
      hideHeader: true,
      x: ml + 20,
      y: 150,
      columnsSize: [80, 140],
      prepareRow: (row, columnIdx, rowIdx, rowRect, cellRect) => doc.font("Helvetica").fontSize(8),
    })

    // doc.image(Blt24Image, pw - mr - 200, 130,  { width: 200, preserveAspectRatio:"xMinYMin meet" })


    // Tester info table and image
    const testerInfoTable = {
      subtitle: "Tester info",
      headers: [
        { label: 'parameter', align:"left" },
        { label: 'value', align:"right" },        
      ],
      rows: [
        ["Model", productName],
        ["Serial number", serialNumber],
        ["Hardware version", hardwareVersion? `${hardwareVersion.major}.${hardwareVersion.minor}` : "--"],
        ["Firmware version", firmwareVersion? `${firmwareVersion.major}.${firmwareVersion.minor}` : "--"],
      ] 
    }
    await doc.table(testerInfoTable, {
      hideHeader: true,      
      x: ml + 20,
      y: 350,
      columnsSize: [80, 140],
      prepareRow: (row, columnIdx, rowIdx, rowRect, cellRect) => doc.font("Helvetica").fontSize(8),
    })

    doc.image(Blt24Image, pw - mr - 200, 320,  { width: 200, preserveAspectRatio:"xMinYMin meet" })
    
    

    return new Promise(resolve => {

      // Write PDF to disk and execute callbacks if defined
      stream.on('finish', function() {
        onSuccess?.(fileName)
        resolve(fileName)
      })    
      doc.pipe(stream)
      doc.end()
    })
  }
}

// const report = new Report({})
module.exports = Report
