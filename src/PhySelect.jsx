/* eslint-disable simple-import-sort/imports */
import React from 'react'
import { DtmPhy } from '@arendi/testing'
import Selection from './Selection'


const items = DtmPhy.List().map(def => ({text:def[0], value:def[1]}))

const PhySelect = React.memo(({onChange=null, selected=DtmPhy.Phy1Mbps, multiple=false, disabled=false, style={}}) => {
  return (
      <Selection 
          label="Phy"
          items={items}
          selected={selected}
          onChange={onChange}
          multiple={multiple}
          disabled={disabled}
          style={style}
      />
  )
})

PhySelect.propTypes = {
    onChange: null,
    selected: [],
    multiple: false,
    disabled: false,
    style: {}
}

export default PhySelect