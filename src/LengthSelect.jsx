/* eslint-disable simple-import-sort/imports */
import React from 'react'
import { DtmBytes } from '@arendi/testing'
import Selection from './Selection'


const items = DtmBytes.List().map(def => ({text:`${def[1]} bytes`, value:def[1]}))

const LengthSelect = React.memo(({onChange=null, selected=DtmBytes.B37, multiple=false, disabled=false, style={}}) => {
  return (
      <Selection 
          label="Length"
          items={items}
          selected={selected}
          onChange={onChange}
          multiple={multiple}
          disabled={disabled}
          style={style}
      />
  )
})

LengthSelect.propTypes = {
    onChange: null,
    selected: [],
    multiple: false,
    disabled: false,
    style: {}
}

export default LengthSelect