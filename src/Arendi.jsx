/* eslint-disable react/no-unused-prop-types */
/* eslint-disable simple-import-sort/imports */
/* eslint-disable no-shadow */
import React, {} from 'react'
import Grid from '@mui/material/Grid'
import Card from '@mui/material/Card'
import CardMedia from '@mui/material/CardMedia'
import CardHeader from '@mui/material/CardHeader'
import CardContent from'@mui/material/CardContent'
import Typography from '@mui/material/Typography'
import List from '@mui/material/List'
import ListItem from '@mui/material/ListItem'
import ListItemIcon from '@mui/material/ListItemIcon'
import ListItemText from '@mui/material/ListItemText'
import Link from '@mui/material/Link'
import Divider from '@mui/material/Divider'
import Stack from '@mui/material/Stack'

import { ArendiGreen } from './Colors'

import WebsiteIcon from '@mui/icons-material/Language'
import PhoneIcon from '@mui/icons-material/Call'
import EmailIcon from '@mui/icons-material/Email'
import AdressIcon from '@mui/icons-material/LocationOn'
import CheckIcon from '@mui/icons-material/Check'

import ArendiLogo from '../resources/arendi_products_logo.svg'
import hardware from '../resources/hardware.png'
import Blt24Image from '../resources/BLT2450.png'

function Arendi() {

    return (
        <div>

            <Grid container spacing={2} sx={{ minWidth: 600, maxWidth: 1000, mt: 0 }}>
                
                {/* Arendi info page */ }
                <Grid item xs={12}>
                    
                    <Card sx={{ minWidth: 200, mb: 0 }} spacing={0}>
                        

                        
                        
                        <CardMedia
                            component="img"
                            height="350"
                            image={hardware}
                            alt="Arendi Products"
                        />

                        <CardHeader
                            title="Arendi Products GmbH"
                            action={<img src={ArendiLogo} alt="" height="60"/>}
                            sx={{paddingTop: 1, mt:0, paddingBottom:1}}
                        />

                        {/* Adress and contact info */}
                        <CardContent sx={{paddingTop:2}}>
                            <Stack spacing={0} direction="row" sx={{flexWrap:"wrap"}}>
                                <Link href="https://goo.gl/maps/5Th7x4M8tEhrjxhBA" target="maps" sx={{color:ArendiGreen, marginLeft:2}}  underline='none'>
                                    <AdressIcon sx={{marginRight:2}}/>
                                    Eichtalstrasse 55, 8634 Hombrechtikon, SWITZERLAND
                                </Link>

                                <Link href="https://www.arendi.ch" target="website" sx={{color:ArendiGreen, marginLeft:2}}  underline='none'>
                                    <WebsiteIcon sx={{marginRight:2}}/>
                                    https://www.arendi.ch
                                </Link>

                                { /* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
                                <Link href="#" sx={{color:ArendiGreen, marginLeft:2}}  underline='none'>
                                    <PhoneIcon sx={{marginRight:2}}/>
                                    +41 (0)55 254 30 30
                                </Link>

                                { /* eslint-disable-next-line jsx-a11y/anchor-is-valid */}
                                <Link href="mailto:info@arendi.ch" target="email" sx={{color:ArendiGreen, marginLeft:2}}  underline='none'>
                                    <EmailIcon sx={{marginRight:2}}/>
                                    info@arendi.ch
                                </Link>
                            </Stack>
                        </CardContent>             
                        
                        {/* BLT2450 product info */}
                        <CardHeader
                            title="BLT2450 Bluetooth tester"
                            subheader={<>Visit our product website to <Link href="https://www.arendi.ch/blt2450" target="Blt2450" sx={{color:ArendiGreen}}>
                                download manual and available tools
                            </Link></>}
                            sx={{paddingTop:7, mt:2}}
                        />

                        <CardContent sx={{paddingTop:2}}>
                            <Grid container>
                                <Grid item sm={12} md={6}>
                                    <List>
                                        <ListItem>
                                            <ListItemIcon>
                                                <CheckIcon  sx={{color:ArendiGreen}} />
                                            </ListItemIcon>
                                            <ListItemText primary="Development" secondary="Manually controlled, using graphical user interface"/>                                            
                                        </ListItem>

                                        <ListItem>
                                            <ListItemIcon>
                                                <CheckIcon  sx={{color:ArendiGreen}} />
                                            </ListItemIcon>
                                            <ListItemText primary="Production" secondary="Automated measurements, using scripts"/>                                            
                                        </ListItem>

                                        <ListItem>
                                            <ListItemIcon>
                                                <CheckIcon  sx={{color:ArendiGreen}} />
                                            </ListItemIcon>
                                            <ListItemText primary="Continuous integration" secondary="Automated testing, using scripts"/>                                            
                                        </ListItem>

                                    </List>
                                </Grid>

                                <Grid item sm={12} md={6}>
                                    <CardMedia component="img" image={Blt24Image} alt="BLT2450" sx={{objectFit: "contain"}} />
                                </Grid>
                            </Grid>
                        </CardContent>
                        
                    </Card>
                </Grid>                 
                
            </Grid>

        </div>
  )
}

Arendi.propTypes = {

}

export default Arendi