/* eslint-disable simple-import-sort/imports */
/* eslint-disable no-shadow */
import React, { useEffect, useRef, useState } from 'react'
import { useSelector } from 'react-redux'
import { ExtendedTesterMode } from './reducers/TesterReducer'
import Missing from './Blt24Missing'
import PowerMeter from './Blt24PowerMeter'
import Attenuator from './Blt24Attenuator'
import Generator from './Blt24Generator'
import Dtm from './Blt24Dtm'
import Sensitivity from './Blt24Sensitivity'
import Ble from './Blt24Ble'
import Alert from '@mui/material/Alert'
import AlertTitle from '@mui/material/AlertTitle'
import Slide from '@mui/material/Slide'


/* Applies transition effects to the component(s) */
const Transition = React.memo(({enabled=false, children}) => {
    return (
        <Slide direction={enabled? "down" : "up"} in={enabled} timeout={{appear:300, enter:300, exit:200}} unmountOnExit style={{position:"absolute", top:0, left:0, zIndex: enabled? 1 : 0}}>
            <div>
                {children}
            </div>
        </Slide>
    )
})

Transition.propTypes = {
    enabled: true,
    children: undefined
}

/* Displays the applicable component(s) according to the current situation */
const ActiveComponent = React.memo(({testerMode, testerMissing, connectionLost}) => {
    return (
        <div style={{position:"relative"}}>

            { /* Server not running component */}
            {connectionLost &&
            <Alert variant="filled" severity="warning">
                <AlertTitle>Server not running</AlertTitle>
                In order to run this application, the server must be started
            </Alert>
          }

            { /* Component when no BLT24 available */ }
            <Transition enabled={testerMissing}>
                <Missing />
            </Transition>          
          
            { /* Component for Sensitivity Measurement */}
            <Transition enabled={(testerMode === ExtendedTesterMode.Sensitivity)}>
                <Sensitivity />
            </Transition>
 
            { /* Component for PowerMeter mode */ }
            <Transition enabled={(testerMode === ExtendedTesterMode.PowerMeter)}>
                <PowerMeter />
            </Transition>

            { /* Component for DTM mode */ }
            <Transition enabled={(testerMode === ExtendedTesterMode.Dtm)}>
                <Dtm />
            </Transition>

            { /* Component for Attenuator mode */ }
            <Transition enabled={(testerMode === ExtendedTesterMode.Attenuator)}>
                <Attenuator />
            </Transition>

            { /* Component for Generator mode */ }
            <Transition enabled={(testerMode === ExtendedTesterMode.Generator)}>
                <Generator/>
            </Transition>

            { /* Component for BLE mode */ }
            <Transition enabled={(testerMode === ExtendedTesterMode.Ble)}>
                <Ble/>
            </Transition>
        </div>
    )
})

ActiveComponent.propTypes = {
    testerMode: undefined,
    testerMissing: false,
    connectionLost: false
}


function Blt24Interface() {

  // get active tester from store
  const { mode } = useSelector(state => state.app.tester)
  const { isConnected, testerList } = useSelector(state => state.app.api)
  const [ connectionLost, SetConnectionLost ] = useState(false)
  const [ testerMissing, SetTesterMissing ] = useState(false)
  const stateRef = useRef()
  stateRef.current = {isConnected, testerMissing:!testerList.length}
  
  // Handling error signalisations
  useEffect(() => {
    // Set connectionLost if Api is disconnected for more than 2 seconds
    // Reset connectionLost immediately, when Api is connected
    isConnected
        ? SetConnectionLost(false)
        : setTimeout(() => SetConnectionLost(!stateRef.current.isConnected), 2000)

    // Set testerMissing if no Tester is connected for more than 2 seconds
    // Reset testerMissing immediately, when a Tester is connected
    testerList.length
        ? SetTesterMissing(false)
        : setTimeout(() => SetTesterMissing(stateRef.current.isConnected && stateRef.current.testerMissing), 2000)
  }, [isConnected, testerList])

  return (
      <ActiveComponent testerMode={mode} connectionLost={connectionLost} testerMissing={testerMissing}/>
  )
}

Blt24Interface.propTypes = {
}

export default Blt24Interface