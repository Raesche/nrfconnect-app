/* eslint-disable simple-import-sort/imports */
import React from 'react'
import { DtmChannel } from '@arendi/testing'
import Selection from './Selection'


const items = DtmChannel.List().map(def => ({text: def[0].toUpperCase(), value: def[1]}))

const ChannelSelect = React.memo(({onChange=null, selected=DtmChannel.Ch0, multiple=false, disabled=false, style={}}) => {
  return (
      <Selection 
          label="Channel"
          items={items}
          selected={selected}
          onChange={onChange}
          multiple={multiple}
          disabled={disabled}
          style={style}
          maxChars={30}
          sort
      />
  )
})

ChannelSelect.propTypes = {
    onChange: null,
    selected: [],
    multiple: false,
    disabled: false,
    style: {}
}

export default ChannelSelect