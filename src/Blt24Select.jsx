/* eslint-disable prefer-template */
/* eslint-disable simple-import-sort/imports */
/* eslint-disable no-shadow */
import React, {useEffect, useState} from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { SetActiveTester, SetFavoriteTester } from './reducers/TesterReducer'
import Blt24SetModeComponent from './Blt24SetMode'

import { blue } from '@mui/material/colors'
import Box from '@mui/material/Box'
import List from '@mui/material/List'
import Divider from '@mui/material/Divider'
import Stack from '@mui/material/Stack'
import ListItem from '@mui/material/ListItem'
import IconButton from '@mui/material/IconButton'
import ListItemButton from '@mui/material/ListItemButton'
import ListItemIcon from '@mui/material/ListItemIcon'
import ListItemText from '@mui/material/ListItemText'
import ListItemAvatar from '@mui/material/ListItemAvatar'
import Avatar from '@mui/material/Avatar'
import ExpandLess from '@mui/icons-material/ExpandLess'
import ExpandMore from '@mui/icons-material/ExpandMore'
import Collapse from '@mui/material/Collapse'
import DialogTitle from '@mui/material/DialogTitle'
import Dialog from '@mui/material/Dialog'
import DialogContent from '@mui/material/DialogContent'
import Button from '@mui/material/Button'
import DialogActions from '@mui/material/DialogActions'
import ClickAwayListener from '@mui/base/ClickAwayListener'
import Tooltip from '@mui/material/Tooltip'

import SerialNumberIcon from '@mui/icons-material/Fingerprint'
import ProductNameIcon from '@mui/icons-material/Sell'
import FirmwareVersionIcon from '@mui/icons-material/Memory'
import HardwareVersionIcon from '@mui/icons-material/Build'
import ProgramIcon from '@mui/icons-material/Download'
import SettingsIcon from '@mui/icons-material/MiscellaneousServices'
import EjectIcon from '@mui/icons-material/Eject'
import FavoriteIcon from '@mui/icons-material/StarBorderOutlined'
import FavoriteIconActive from '@mui/icons-material/Star'


function Blt24SelectComponent() {


  // Header containing info to the BLT24 selection status
  function Header(list, activeTester) {
    
    // Show error message if no BLT24 is detected
    if (list.length === 0) {
      return  <ListItemButton disabled>
          <ListItemText primary="BLT2450 missing" secondary="Attach BLT2450 tester"/>                          
      </ListItemButton>
    }
  
    // Show info of selected BLT24 if selected
    // Show select menu if multiple BLT24 are present or if no BLT24 is currently selected
    if (list.length > 1 || activeTester?.serialNumber === undefined) {
      return  <>
          <ListItemButton disabled={measurementActive} onClick={() => SetSelectOpen(selectOpen => !selectOpen)}>
              <ListItemText primary={(activeTester?.serialNumber !== undefined)? activeTester?.serialNumber : "No tester selected"} secondary={activeTester?.productName? `${activeTester.productName} tester` : "unknown device"} />
              {selectOpen ? <ExpandLess /> : <ExpandMore />}
          </ListItemButton>
  
          <Collapse in={selectOpen} timeout="auto" unmountOnExit>
              <ClickAwayListener onClickAway={() => SetSelectOpen(false)}>
                  <div>
                      {testerListComponent}
                  </div>
              </ClickAwayListener>
          </Collapse>
      </>
    }

    // Show info of selected BLT24
    return <ListItemButton disabled={measurementActive}>
        <ListItemText primary={activeTester?.serialNumber} secondary={activeTester?.productName? `${activeTester.productName} tester` : "unknown device"}/>
    </ListItemButton>
  }


  const dispatch = useDispatch()
  const [selectOpen, SetSelectOpen] = useState(false)
  const [dialogOpen, SetDialogOpen] = useState(false)

  // Set active tester in store
  const SetTester = tester => {
    dispatch(SetActiveTester(tester))
  }

  const { measurementActive } = useSelector(state => state.app.app)
  const { list, activeTester, favoriteTester } = useSelector(state => state.app.tester)
  const { TesterList, testerList } = useSelector(state => state.app.api)
  

  const isFavorite = (activeTester?.serialNumber === favoriteTester)

  // Mark active Blt24 as favorite
  const SetFavorite = isFavorite => {
    dispatch(SetFavoriteTester(isFavorite? activeTester?.serialNumber : "", true))
  }
  
  

  // Handle tester attach, remove, connect, disconnect events
  useEffect(() => {

    // Do nothing if a tester is already selected and connected
    const active = TesterList?.Get(activeTester?.serialNumber)
    if (active) {
      return
    }

    // Automatically select favorite tester if present
    const favorite = TesterList?.Get(favoriteTester)
    if (favorite) {
      SetTester(favorite)
      return
    }

    // Select first tester if present
    SetTester(testerList.length? testerList[0] : null)
  }, [testerList]) // eslint-disable-line react-hooks/exhaustive-deps



  const TesterSelectHandler = tester => {
    SetTester(tester)
    SetSelectOpen(false)
  }

  // Creates a list containing all selectable Blt24 tester
  // The list is numerically sorted by the tester serialNumber and the favorite tester always comes first
  const testerListComponent = testerList.sort((a,b) => {
    if (favoriteTester === a.serialNumber)
      return -1
    if (favoriteTester === b.serialNumber)
      return 1
    return a.serialNumber.localeCompare(b.serialNumber, undefined, { numeric:true })
  }).map(tester => {
    return <ListItemButton
        dense
        selected={tester===activeTester}
        sx={{pl: 2}}
        onClick={() => TesterSelectHandler(tester)}
        >
        <ListItemIcon>{(favoriteTester === tester.serialNumber)? <FavoriteIconActive /> : <FavoriteIcon color="disabled" />}</ListItemIcon>
        <ListItemText primary={tester.serialNumber} secondary={tester.productName? `${tester.productName} tester` : "unknown device"} />
    </ListItemButton>
  })

  return (
      <div>       
          <Box sx={{bgcolor: 'background.paper', mb:2}}>
              <List>

                  { /* Select BLT24, show info */ }
                  {Header(testerList, activeTester)}

                  <Divider/>
                  <Blt24SetModeComponent />
                  <Divider/>
                  <ListItem disabled={(activeTester?.serialNumber === undefined) || measurementActive}>
                      <Stack direction="row" justifyContent="space-between" spacing={3}>
                          <Tooltip title={ isFavorite? "Clear favorite" : "Mark as favorite"} enterDelay={500}>
                              <IconButton onClick={() => SetFavorite(!isFavorite)}>{isFavorite? <FavoriteIconActive fontSize="small"/> : <FavoriteIcon fontSize="small"/>}</IconButton>
                          </Tooltip>

                          <Tooltip title="Open tester settings" enterDelay={500}>
                              <IconButton onClick={() => SetDialogOpen(true)}><SettingsIcon fontSize="small"/></IconButton>
                          </Tooltip>

                          <Tooltip title="Eject tester" enterDelay={500}>
                              <IconButton onClick={() => SetTester(null)}><EjectIcon fontSize="small"/></IconButton>
                          </Tooltip>
  
                      </Stack>
                  </ListItem>
              </List>
          </Box>

          <Dialog
              open={dialogOpen}
              onClose={() => SetDialogOpen(false)}
          >
              <DialogTitle>Tester Settings</DialogTitle>

              <DialogContent sx={{minWidth:400}}>

                  <List
                      sx={{
                      width: '100%',
                      maxWidth: 360,
                      bgcolor: 'background.paper',
                    }}
                  >
                      { /* Product name */ }
                      <ListItem>
                          <ListItemAvatar>
                              <Avatar sx={{ bgcolor: blue[500] }}>
                                  <ProductNameIcon />
                              </Avatar>
                          </ListItemAvatar>
                          <ListItemText primary="Product name" secondary={activeTester?.productName} />
                      </ListItem>

                      <Divider variant="inset" component="li" />

                      { /* Serial Number */ }
                      <ListItem>
                          <ListItemAvatar>
                              <Avatar sx={{ bgcolor: blue[500] }}>
                                  <SerialNumberIcon />
                              </Avatar>
                          </ListItemAvatar>
                          <ListItemText primary="Serial number" secondary={activeTester?.serialNumber} sx={{ userSelect:"text" }}/>
                      </ListItem>

                      <Divider variant="inset" component="li" />

                      { /* Hardware Version */ }
                      <ListItem>
                          <ListItemAvatar>
                              <Avatar sx={{ bgcolor: blue[500] }}>
                                  <HardwareVersionIcon />
                              </Avatar>
                          </ListItemAvatar>
                          <ListItemText primary="Hardware version" secondary={activeTester? `${activeTester.hardwareVersion.major}.${activeTester.hardwareVersion.minor}` : '-.--'} sx={{ userSelect:"text" }}/>
                      </ListItem>

                      <Divider variant="inset" component="li" />

                      { /* Firmware Version */ }
                      <ListItem
                          secondaryAction={
                              <IconButton edge="end" aria-label="update" disabled>
                                  <ProgramIcon />
                              </IconButton>
                      }>
                          <ListItemAvatar>
                              <Avatar sx={{ bgcolor: blue[500] }}>
                                  <FirmwareVersionIcon />
                              </Avatar>
                          </ListItemAvatar>
                          <ListItemText primary="Firmware version" secondary={activeTester? `${activeTester.firmwareVersion.major}.${activeTester.firmwareVersion.minor}` : '-.--'} sx={{ userSelect:"text" }}/>
                      </ListItem>
                  </List>

              </DialogContent>

              <DialogActions>
                  <Button onClick={() => SetDialogOpen(false)} autoFocus>Cancel</Button>
              </DialogActions>

              
          </Dialog>

          
      </div>
  )
}

Blt24SelectComponent.propTypes = {
}

export default Blt24SelectComponent