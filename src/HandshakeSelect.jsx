/* eslint-disable simple-import-sort/imports */
import React from 'react'
import { Handshake } from '@arendi/testing'
import Selection from './Selection'


const items = Handshake.List().map(def => ({text:`${def[0]}`, value:def[1]}))

const HandshakeSelect = React.memo(({onChange=null, selected=Handshake.None, multiple=false, disabled=false, style={}}) => {
  return (
      <Selection 
          label="Handshake"
          items={items}
          selected={selected}
          onChange={onChange}
          multiple={multiple}
          disabled={disabled}
          style={style}
      />
  )
})

HandshakeSelect.propTypes = {
    onChange: null,
    selected: [],
    multiple: false,
    disabled: false,
    style: {}
}

export default HandshakeSelect