/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable react/no-unused-prop-types */
/* eslint-disable simple-import-sort/imports */
/* eslint-disable no-shadow */
import React from 'react'
import Grid from '@mui/material/Grid'
import CardHeader from '@mui/material/CardHeader'
import Card from '@mui/material/Card'
import CardMedia from '@mui/material/CardMedia'
import CardContent from '@mui/material/CardContent'
import List from '@mui/material/List'
import ListItem from '@mui/material/ListItem'
import ListItemText from '@mui/material/ListItemText'
import ListItemIcon from '@mui/material/ListItemIcon'
import Link  from '@mui/material/Link'
import Blt24Image from '../resources/BLT2450.png'
import CheckIcon from '@mui/icons-material/Check'
import Alert from '@mui/material/Alert'
import AlertTitle from '@mui/material/AlertTitle'

import { ArendiGreen } from './Colors'

function Blt24Missing() { 
    return (

        <Grid container spacing={2} sx={{ minWidth: 600, maxWidth: 1000, mt: 0 }}>
                
            {/* Arendi info page */ }
            <Grid item xs={12}>
                    
                <Card sx={{ minWidth: 200, mb: 0 }} spacing={0}>                   
                        
                    {/* Error alert */}
                    <Alert variant="filled" severity="warning" >
                        <AlertTitle>BLT2450 missing</AlertTitle>
                        No BLT2450 was found on this system
                    </Alert>
                        
                        
                    {/* BLT2450 debug info */}
                    <CardHeader
                        title="BLT2450 Bluetooth tester missing"
                        subheader={<>
                            Follow the instructions below or visit our product website to <Link href="https://www.arendi.ch/blt2450" target="Blt2450" sx={{color:ArendiGreen}}>download the manual and available tools</Link>
                        </>}
                        sx={{mt:2}}
                        />

                    <CardContent sx={{paddingTop:2}}>
                        <Grid container>
                            <Grid item sm={12} md={6}>
                                    
                                <List>
                                    <ListItem>
                                        <ListItemIcon>
                                            <CheckIcon  sx={{color:ArendiGreen}} />
                                        </ListItemIcon>
                                        <ListItemText primary="Connect tester" secondary="Use provided USB cable to connect a BLT2450 tester to this system"/>                                            
                                    </ListItem>

                                    <ListItem>
                                        <ListItemIcon>
                                            <CheckIcon  sx={{color:ArendiGreen}} />
                                        </ListItemIcon>
                                        <ListItemText primary="Check power" secondary="Make sure, the power LED of the tester is on"/>                                            
                                    </ListItem>

                                    <ListItem>
                                        <ListItemIcon>
                                            <CheckIcon  sx={{color:ArendiGreen}} />
                                        </ListItemIcon>
                                        <ListItemText primary="Check status" secondary="Make sure, the tester has no errors (status LED not blinking) "/>                                            
                                    </ListItem>
                                        

                                </List>
                            </Grid>

                            <Grid item sm={12} md={6}>
                                <CardMedia component="img" image={Blt24Image} alt="BLT2450" sx={{objectFit: "contain"}} />
                            </Grid>
                        </Grid>
                    </CardContent>
                        
                </Card>
            </Grid>                 
                
        </Grid>    
  )
}

Blt24Missing.propTypes = {

}

export default Blt24Missing