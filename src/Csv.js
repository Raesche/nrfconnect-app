const fs = require('fs')
const Csv = require('csv')

class CsvClass {

  // Convert provided measurement serie to csv string
  // Type of argument: [ {"name:SERIE_NAME_1", data:[ [powerDbm, per], [powerDbm, per]..]}, {"name:SERIE_NAME_2", data:[ [powerDbm, per], [powerDbm, per]..]}, ..]
  Create({series=[], directory='', name=''}) {

    // Create object containing all series names. This is important for constructing the header field in CSV
    const NAMES = Object.fromEntries(series.map(series => [series.name, null]))

    // Transform series data into new format
    const transformedMap = new Map()
    series.forEach(serie => {
      // Insert power and per values from every serie
      serie.data.forEach(([powerDbm, per]) => {
        const existing = transformedMap.get(powerDbm)?? NAMES                     // get existing entry with matching powerDbm or create an empty entry containing all series names
        transformedMap.set(powerDbm, {powerDbm, ...existing, [serie.name]:per})   // re-insert existing entry and overwrite per value of current serie
      })
    })

    // Convert from map to array, sort by decreasing power
    const sortedArray = [...transformedMap.values()].sort((a,b) => b.powerDbm - a.powerDbm)

    return new Promise((resolve, reject) => {
      const csvData = Csv.stringify(sortedArray, {
        header:true,
      }, (error, output) => {
        
        if (error)
          reject({error})

        // Write to disk if directory and fileName provided
        if (directory && name) {
          const fileName = `${directory}\\${name}.csv`
          fs.writeFile(fileName, output, error => error? reject({error}) : resolve({csv:output, fileName}))
        }
        else
          resolve({csv:output})
      })
    })    
  }

  Parse({data=null, csvString=null, directory=null, name=null}) {

    // Parses the csv data string
    const ParseData = data => {      
      return new Promise((resolve, reject) => {
        Csv.parse(data, {
          auto_parse:true,
          columns:true,
          relax_column_count:true
        }, (error, output) => {
  
          if (error)
            return reject({error})
  
          // Sort by decreasing power
          output.sort((a,b) => b.powerDbm - a.powerDbm)

          // Get header, do some error handling
          const HEADER = Object.keys(output[0]) 
          if (!HEADER.find(name => name === "powerDbm"))
            throw new Error("Error parsing CSV header: Invalid header format, 'powerDbm' expected")
           
          // Get series names, do some error handling
          const NAMES = HEADER.filter(value => value !== 'powerDbm')
          if (!NAMES.length)
            throw new Error("Error parsing CSV header: Missing series, expected at least one serie")

  
          // Transform data to series
          const series = []  
          NAMES.forEach(name => {
            const newSerie = { name, data:[] }
            output.forEach(line => {
              if (line[name]) {
                
                // Read values and check validity
                const powerDbm = parseFloat(line.powerDbm)
                const per = parseFloat(line[name])                
                if ((powerDbm < -140) || (powerDbm > 20) || (per < 0) || (per > 100))
                  throw new Error("Error parsing CSV data: Invalid values for powerDbm or PER")
                
                newSerie.data.push([powerDbm, per])
              }
            })
  
            series.push(newSerie)
          })

          resolve({series})
        })
      })
    }


    // Pass provided string to ParseData()
    if (typeof data === 'string') {
      return ParseData(data)
    }

    // Read data from file and pass it to ParseData()
    else if ((typeof name === 'string') && (typeof directory === 'string')) {
      return new Promise((resolve, reject) => {
        const fileName = `${directory}\\${name}.csv`
        fs.readFile(fileName, 'utf-8', (error, data) => {   
          if (error)
            return Promise.reject(error)
          resolve(ParseData(data))
        })
      })
    }

  }
}

// Test with sample data
/* const csvInst = new CsvClass()
const series = [{"data":[[-98,22.5],[-99,28.125],[-100,46.25],[-101,66.875],[-102,93.125],[-103,99.375],[-104,100],[-105,100],[-106,100],[-107,100],[-108,100],[-109,100],[-110,100],[-111,100],[-112,100],[-113,100]],"name":"CH0"},{"data":[[-10, 33.3],    [-98,30.3125],[-99,31.5625],[-100,29.6875],[-101,50],[-102,55.3125],[-103,85.625],[-104,97.8125],[-105,100],[-106,100],[-107,100],[-108,100],[-109,100],[-110,100],[-111,100],[-112,100],[-113,100]],"name":"CH19"},{"data":[[-98,19.375],[-99,25.625],[-100,25],[-101,19.0625],[-102,23.4375],[-103,45.3125],[-104,77.8125],[-105,94.6875],[-106,100],[-107,100],[-108,100],[-109,100],[-110,100],[-111,100],[-112,100],[-113,100]],"name":"CH39"}]
// Convert object to CSV
csvInst.Stringify({series})
  .then(({output}) => {
    console.log(output)
    // Convert CSV to object
    csvInst.Parse({ csvString:output })
      .then(({series}) => console.log(series))
  })*/

module.exports = CsvClass
