/* eslint-disable simple-import-sort/imports */
import React from 'react'
import { Baudrate } from '@arendi/testing'
import Selection from './Selection'


const items = Baudrate.List().map(def => ({text:`${def[1]}`, value:def[1]}))

const BaudrateSelect = React.memo(({onChange=null, selected=Baudrate.B19200, multiple=false, disabled=false, style={}}) => {
  return (
      <Selection 
          label="Baudrate"
          items={items}
          selected={selected}
          onChange={onChange}
          multiple={multiple}
          disabled={disabled}
          style={style}
      />
  )
})

BaudrateSelect.propTypes = {
    onChange: null,
    selected: [],
    multiple: false,
    disabled: false,
    style: {}
}

export default BaudrateSelect