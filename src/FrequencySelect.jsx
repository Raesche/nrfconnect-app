/* eslint-disable simple-import-sort/imports */
import React from 'react'
import { DtmChannel } from '@arendi/testing'
import Selection from './Selection'


const items = DtmChannel.List().map(def => ({text: `${2402 + 2*def[1]} MHz`, value: def[1]}))

const FrequencySelect = React.memo(({onChange=null, selected=DtmChannel.Ch0, multiple=false, disabled=false, style={}}) => {
  return (
      <Selection 
          label="Frequency"
          items={items}
          selected={selected}
          onChange={onChange}
          multiple={multiple}
          disabled={disabled}
          style={style}
          maxChars={30}
          sort
      />
  )
})

FrequencySelect.propTypes = {
    onChange: null,
    selected: [],
    multiple: false,
    disabled: false,
    style: {}
}

export default FrequencySelect