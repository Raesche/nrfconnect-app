/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable simple-import-sort/imports */
import React, { useEffect, useRef, useState, useCallback } from 'react'
import { spawn } from 'child_process'
import path from 'path'
import { getAppDir } from 'pc-nrfconnect-shared'
import { Enum, WsInterface } from '@arendi/testing'

import CircularProgress from '@mui/material/CircularProgress'
import { red, green } from '@mui/material/colors'
import List from '@mui/material/List'
import ListItem from '@mui/material/ListItem'
import ListItemText from '@mui/material/ListItemText'
import DialogTitle from '@mui/material/DialogTitle'
import Dialog from '@mui/material/Dialog'
import DialogContent from '@mui/material/DialogContent'
import Button from '@mui/material/Button'
import DialogActions from '@mui/material/DialogActions'
import TextField from '@mui/material/TextField'
import InputAdornment from '@mui/material/InputAdornment'

import SuccessIcon from '@mui/icons-material/Done'
import FailIcon from '@mui/icons-material/Clear'


class State extends Enum {
  static Connected = 1
  static Disconnected = 2
  static StartPolling = 3
  static Polling = 4
}

const DEBOUNCE_TIMEOUT_MS = 500     // debounce time in ms
const CONNECT_RETRY_MS = 1000       // should be smaller than CONNECT_TIMEOUT_MS
const CONNECT_TIMEOUT_MS = 3000
const POLLING_TIMEOUT_MS = 5000

const DEFAULT_SERVER_NAME = "127.0.0.1"
const DEFAULT_SERVER_PORT = 5000

const TestConnection = async (wsInterface, url) => {      
  // Close connection
  try {
    await wsInterface.Close()
  } catch(error) {console.error(error)}

  // Open connection with new settings
  try {
    await wsInterface.Open({url, protocols:undefined, retryMs:CONNECT_RETRY_MS}, CONNECT_TIMEOUT_MS)
  } catch(error) {console.error(error)}
}

// TODO: Suspend state machine (connecting, polling...) when open === false
const ServerTest = React.memo(({open=false, onClose=null, onChange=null, name=DEFAULT_SERVER_NAME, port=DEFAULT_SERVER_PORT}) => {
  const [ state, SetState ] = useState(State.StartPolling)
  const [ wsInterface ] = useState(new WsInterface())
  const [ changed, SetChanged ] = useState(false)
  const [ serverName, SetServerName ] = useState(DEFAULT_SERVER_NAME)
  const [ serverPort, SetServerPort ] = useState(DEFAULT_SERVER_PORT)
  const debounceTimer = useRef(null)
  const pollTimer = useRef(null)


  console.log("ServerTest rendering...")


  // Creates server connection status icon
  const ConnectionStatusIcon = () => {
    switch(state) {
      case State.Connected:
        return <SuccessIcon fontSize="medium" sx={{color: green[500]}}/>
  
      case State.Reset:
      case State.Disconnected:
        return <FailIcon fontSize="medium" sx={{color: red[500]}}/>
  
      default:
        return <CircularProgress size={25}/>
    }
  }

  // Initialize component, register event handler
  useEffect(() => {
    const OpenHandler = () => SetState(State.Connected)
    const CloseHandler = () => SetState(State.Disconnected)
    wsInterface.OnOpen(OpenHandler)
    wsInterface.OnClose(CloseHandler)

    // Cleanup on unmount
    return () => {
      wsInterface.OffOpen(OpenHandler)
      wsInterface.OffClose(CloseHandler)
      wsInterface.Close()
      wsInterface.Dispose()
    }
  }, [])

  // Handle state changes
  useEffect(() => {
    console.error("STATE: ", State.Name(state))

    switch(state) {

      // Server not reachable
      // Start polling after a few seconds or whenever url changed
      case State.Disconnected:
        clearTimeout(pollTimer.current)
        if (changed)
          SetState(State.StartPolling)
        else {
          pollTimer.current = setTimeout(() => {
            SetState(State.StartPolling)
          }, POLLING_TIMEOUT_MS)
        }
        break;
        
      // Initiate server connection
      case State.StartPolling:
        SetChanged(false)
        clearTimeout(pollTimer.current)
        TestConnection(wsInterface, url)
        SetState(State.Polling)
        break

      // Connection to server pending
      case State.Polling:
        break;      

      // Server reachable
      // Start polling whenever url changed
      case State.Connected:
        clearTimeout(pollTimer.current)
        if (changed)
          SetState(State.StartPolling)
        break;

      default:
        break;
    }

  }, [state, changed])

  // Update serverName if name attribute changed
  useEffect(() => {
    SetServerName(name)
  }, [name])

  // Update serverPort if port attribute changed
  useEffect(() => {
    SetServerPort(port)
  }, [port])

  const url = `ws://${serverName}:${serverPort}/blt24`

  // Test server connection after url has changed
  // Debounce test interval by 500 ms
  useEffect(() => {
    clearTimeout(pollTimer.current)
    clearTimeout(debounceTimer.current)
    debounceTimer.current = setTimeout(() => SetChanged(true), DEBOUNCE_TIMEOUT_MS)
  }, [url])// eslint-disable-line react-hooks/exhaustive-deps

  // Matches IP address such as 127.0.0.1 or 192.168.1.10
  // Matches host address such as localhost or server.com/test
  const urlMatch = /(^\d{1,3}.\d{1,3}.\d{1,3}.\d{1,3}$)|(^[A-Za-z]+[\w/.]*$)/g
  const ValidateServerName = () => (serverName?.match(urlMatch))
  const ValidateServerPort = () => (serverPort >= 1000 && serverPort <= 6000)

  
  // Handle user inputs
  const ServerNameHandler = useCallback(event => {
    SetServerName(event.target.value)
  }, [])

  // Handle user inputs
  const ServerPortHandler = useCallback(event => {
    SetServerPort(parseInt(event.target.value, 10))
  }, []) 
  
  const ServerStartHandler = () => {
    const serverPath = path.join(getAppDir(), "server", "WebSocketConsole.exe")
    console.log(`Starting server from ${serverPath} on Port ${serverPort}`)
    const server = spawn(serverPath, ['--port', serverPort ])
    server.on('close', () => {
        // process.exit() // Simply exit this process
        console.log("SERVER TERMINATED")
    })

    server?.stdout.on('data', data => console.log("SERVER:", `${data}`))
    server?.stdout.on('end', () => console.log("Server ended"))
    server?.on('close', () => console.log("Server closed"))    
    // await new Promise()
    return true
  }

  // Server settings changed by pressing "Ok" button
  // Copy settings and close dialog!
  const ServerSettingsHandler = () => { 
    onClose?.()
    onChange?.(serverName, serverPort)
  }

  // Server settings dialog is closed by "Cancel" button
  // Reset settings and close dialog!
  const ServerSettingsCancelHandler = () => {
    onClose?.()
    SetServerName(DEFAULT_SERVER_NAME)
    SetServerPort(DEFAULT_SERVER_PORT)
  }


  return <Dialog
      open={open}
      onClose={onClose}
>
      <DialogTitle>Server Settings</DialogTitle>

      <DialogContent sx={{minWidth:250}}>
          <List
              sx={{
            width: '100%',
            bgcolor: 'background.paper',
          }}
      >

              { /* Server address input field */ }
              <ListItem>
                  <TextField
                      disabledxxx
                      fullWidth
                      id="serverName"
                      label="Hostname / IP-address"
                      error={!ValidateServerName()}
                      defaultValue=""
                      value={serverName}
                      onChange={ServerNameHandler}
                      InputProps={{
                    startAdornment: <InputAdornment position="start">ws://</InputAdornment>,
                  }}
              />
              </ListItem>        

              { /* Server port input field */ }       
              <ListItem>
                  <TextField
                      disabledxxx
                      fullWidth
                      id="serverPort"
                      label="Port"
                      error={!ValidateServerPort()}
                      defaultValue="5000"
                      value={serverPort}
                      onChange={ServerPortHandler}
                  />
              </ListItem>

              { /* Show concatenated server url and connection status */ }
              <ListItem                          
                  secondaryAction={ConnectionStatusIcon()}
              >
                  <ListItemText
                      primary="WebSocket server"
                      secondary={url}
                      sx={{ userSelect:"text" }}
              />
              </ListItem>
          </List>

      </DialogContent>

      <DialogActions>
          <Button onClick={ServerSettingsHandler}>OK</Button>
          <Button onClick={ServerStartHandler}>Start</Button>
          <Button onClick={ServerSettingsCancelHandler} autoFocus>Cancel</Button>
          <Button onClick={ServerStartHandler}>Start</Button>
      </DialogActions>
  
  </Dialog>


 
})

ServerTest.propTypes = {
  open: false,
  onClose: null,
  onChange: null,
  name: '',
  port: ''
}

export default ServerTest
