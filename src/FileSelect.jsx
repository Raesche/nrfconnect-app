import React, {} from 'react'
import { useDropzone } from 'react-dropzone'

const FileSelect = ({onload, children, accept}) => {  

    const {getRootProps, getInputProps} = useDropzone({
        onDrop: files => {
            const reader = new FileReader()
            reader.onload = e => onload?.(e.currentTarget.result)
            reader.readAsText(files[0])
        },
        accept
    })
  
    return (
        <div {...getRootProps()} >
            <input {...getInputProps()} />
            {children}
        </div>
    )
}

FileSelect.propTypes = {
    children:null,
    onload:null,
    accept:null
}

export default FileSelect
