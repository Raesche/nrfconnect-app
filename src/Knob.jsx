/* eslint-disable no-shadow */
// inspired by https://codepen.io/janverstr/pen/PvPoaV

import React, { useEffect, useRef, useState } from 'react'

import {} from './Knob.scss'


// derived from https://www.codegrepper.com/code-examples/javascript/round+to+nearest+step
// Todo: Fix for negative numbers
const Round = (number=0, increment=0.1) => increment * (Math.ceil((number/increment - 2) ) + 2);

const Knob = React.memo(({value, min, max, start, stop, valueColor='rgba(33, 150, 243, 1)', labelColor='rgba(33, 150, 243, 1)', segmentColor='rgba(33, 150, 243, 1)', ringColor='rgba(200, 200, 200, 1)', label, width=300, height=300, onChanged=null, step=0.1, decimals=2, disabled=false, hideDecimals=false}) => {
  
  const [setpointValue, SetSetpointValue] = useState(value)   // Desired value as set by the user
  const [setpointActive, SetSetpointActive] = useState(false) // If true, the user setpoint value will be displayed, if false it is set to the value of the "value" attribute
  const range = Math.abs(max - min) // max value range
  const svg = useRef(null)
  const cursor = onChanged? "pointer" : "default"             // If no changed handler is defined, arrow cursor will be shown, drag icon otherwise
  
  const [integerPart, decimalPart] = (typeof value === "number")? value.toFixed(decimals).split(".") : ["-", "--"]


  const WIDTH = 20
  const HEIGHT = 20
  const RADIUS = 16
  const DEFAULT_STYLE = {stroke:"green", strokeWidth:4, fill:"transparent", opacity: 0.5}

  // Set angle of current user input to calculate and update value
  const SetAngle = deg => {
    let val = range * ((deg+start)/(360+start-stop)) + min
    if (val <= min) {
      val = min
    }
    else if (val >= max) {
      val = max
    }

    val = Round(val, step)
    SetSetpointValue(val)
    onChanged?.(val)
  }

  // Register mouse event handler when component is mounted
  // Cleanup mouse event handler when component is removed
  useEffect(() => {

    // Create an SVGPoin for future math
    const pt = svg.current.createSVGPoint()

    // Get point in global SVG space
    const cursorPoint = evt => {
        pt.x = evt.clientX;
        pt.y = evt.clientY;
        return pt.matrixTransform(svg.current.getScreenCTM().inverse())
    }

    const GetAngle = evt => {
      const loc = cursorPoint(evt)
      const dx = loc.x - WIDTH
      const dy = HEIGHT - loc.y    
      // calculate angle between midpoint and mouse
      return 180 * Math.atan2(dx, dy) / Math.PI  // Range: -180 .. 180°
    }

    // Handle mousemove events from inside the window
    // Calculate the angle between current mouse position and knob centre
    // Update the knob only when left mouse button is pressed
    const MouseMoveHandler = evt => {
      if (evt.buttons & 1) {      // eslint-disable-line no-bitwise
          const deg = GetAngle(evt)  // Range: -180 .. 180°
          SetAngle(deg + 180)        // Range:    0 .. 360°
      }
    }

    // Handle touchmove events from inside the svg
    // Calculate the angle between current touch position and knob centre
    const TouchStartMoveHandler = evt => {
      const deg = GetAngle(evt.touches[0])  // Range: -180 .. 180°
      SetAngle(deg + 180)                   // Range:    0 .. 360°
    }

    // Handle mousedown and mouseup events
    // Start listening for mousemove events when mouse button is pressed inside the knob
    // Stop listening for mousemove events when mouse button is released anywhere inside window
    const MouseButtonHandler = evt => {
      if (evt.buttons & 1) {  // eslint-disable-line no-bitwise
        SetSetpointActive(true)
        window.addEventListener('mousemove', MouseMoveHandler, false)
        const deg = GetAngle(evt) // Range: -180 .. 180°
        SetAngle(deg + 180)       // Range:    0 .. 360°
      }
      else {
        SetSetpointActive(false)
        window.removeEventListener('mousemove', MouseMoveHandler)
      }
    }

    // Listen for mousedown and touchmove events inside knob and mouseup events inside the entire window (if eventhandler is defined)
    if (onChanged && !disabled) {
      svg.current.addEventListener('mousedown', MouseButtonHandler, false)
      window.addEventListener('mouseup', MouseButtonHandler, false)
      svg.current.addEventListener('touchstart', TouchStartMoveHandler, false)
      svg.current.addEventListener('touchmove', TouchStartMoveHandler, false)
    } else {
      svg.current?.removeEventListener('mousedown', MouseButtonHandler)
      window.removeEventListener('mouseup', MouseButtonHandler)
      window.removeEventListener('mousemove', MouseMoveHandler)
      svg.current?.removeEventListener('touchstart', TouchStartMoveHandler, false)
      svg.current?.removeEventListener('touchmove', TouchStartMoveHandler, false)
    }

    // cleanup when unmounting
    return () => {
      svg.current?.removeEventListener('mousedown', MouseButtonHandler) // eslint-disable-line react-hooks/exhaustive-deps
      svg.current?.removeEventListener('touchstart', TouchStartMoveHandler, false) // eslint-disable-line react-hooks/exhaustive-deps
      svg.current?.removeEventListener('touchmove', TouchStartMoveHandler, false) // eslint-disable-line react-hooks/exhaustive-deps
      window.removeEventListener('mouseup', MouseButtonHandler)
      window.removeEventListener('mousemove', MouseMoveHandler)
    }
  }, [])   // eslint-disable-line react-hooks/exhaustive-deps


  // Returns an SVG path with a circle segment according to the provided arguments
  // cx, cy: X and y coordinates of the circle center
  // r: Radius of the circle
  // length: The length of the segment, range: [0..1]
  // rotation: The rotation of the circle [deg]
  // clearance: The angle that should be left out between min and max value [deg]
  // style: The style that should be applied to the path
  const CircleSegment = (cx=WIDTH, cy=HEIGHT, r=RADIUS, length=0.5, rotation=0, clearance=0, style=DEFAULT_STYLE) => {
       
    if (length < 0 || Number.isNaN(length))
      length = 0
    else if (length >= 1)
      length = 0.99999

    if (clearance > 360)
      clearance = 360
    
    const radClearance = Math.PI * clearance / 180
    const radLength = (2*Math.PI - radClearance) * length
    const radRotation = Math.PI * rotation/180
    const ax = cx + r * Math.sin(radRotation - radLength)
    const ay = cy + r * Math.cos(radRotation - radLength)
    const bx = cx + r * Math.sin(radRotation)
    const by = cy + r * Math.cos(radRotation)    
    const largeArc = (Math.abs(radLength) >= Math.PI)? 1 : 0
    const mirrorArc = 0
    return <path d={`M${ax}, ${ay} A${r} ${r} 0 ${largeArc} ${mirrorArc} ${bx} ${by}`} style={style}/>
  }


  return (
      <div className="knob-svg">
          <svg width={width} height={height} viewBox={`0 0 ${2*WIDTH} ${2*HEIGHT}`} className="donut" ref={svg}>
              {CircleSegment(HEIGHT, WIDTH, RADIUS, 1, start, stop-start, {stroke:ringColor, strokeWidth:4, fill:"transparent", opacity: 0.5, cursor})}
              {CircleSegment(HEIGHT, WIDTH, RADIUS, (value-min) / range, start, stop-start, {stroke:segmentColor, strokeWidth:4, fill:"transparent", opacity: (disabled? 0.1 : 0.5), cursor})}
              {CircleSegment(HEIGHT, WIDTH, RADIUS, (setpointActive? setpointValue-min : value-min) / range, start, stop-start, {stroke:segmentColor, strokeWidth:4, fill:"transparent", opacity: (disabled? 0.1 : 0.5), cursor})}              
              <g className={`knob-text  ${disabled? 'knob-disabled' : ''}`}>
                  <text y="50%" transform="translate(0, 2)" style={{fill: valueColor}}>
                      {!hideDecimals // center integerPart in middle of the knob if hideDecimals is set to true
                      ? <tspan x="60%" textAnchor="end" className={`knob-integervalue ${disabled? 'knob-disabled' : ''}`}>{integerPart}</tspan>
                      : <tspan x="50%" textAnchor="middle" className={`knob-integervalue ${disabled? 'knob-disabled' : ''}`}>{integerPart}</tspan>
                      }
                      
                      {!hideDecimals && // hide decimalPart if hideDecimals is set to true
                      <tspan x="60%" textAnchor="start" className={`knob-decimalvalue ${disabled? 'knob-disabled' : ''}`}>.{decimalPart}</tspan>
                      }
                  </text>
                  <text y="63%" transform="translate(0, 2)" style={{fill: labelColor}}>
                      <tspan x="50%" textAnchor="middle" className={`knob-label ${disabled? 'knob-disabled' : ''}`}>{label}</tspan>
                  </text>
              </g>
          </svg>
      </div>
  )
})

Knob.propTypes = {
  value: 0,
  min: 0,
  max: 100,
  start: 0,
  stop: 360,
  valueColor: "",
  labelColor: "",
  segmentColor: "",
  ringColor: "",
  label: "",
  width: 300,
  height: 300,
  onChanged: null,
  step: 0.1,
  decimals: 2,
  disabled: false,
  hideDecimals: false
}

export default Knob