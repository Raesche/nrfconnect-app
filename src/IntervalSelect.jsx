/* eslint-disable simple-import-sort/imports */
import React from 'react'
import Selection from './Selection'


const items = [{text:'100 ms', value:100}, {text:'200 ms', value:200}, {text:'250 ms', value:250}, {text:'300 ms', value:300}, {text:'500 ms', value:500}, {text:'1000 ms', value:1000}, {text:'5000 ms', value:5000}]

const IntervalSelect = React.memo(({onChange=null, selected=1000, multiple=false, disabled=false, style={}}) => {
  return (
      <Selection 
          label="Interval"
          items={items}
          selected={selected}
          onChange={onChange}
          multiple={multiple}
          disabled={disabled}
          style={style}
      />
  )
})

IntervalSelect.propTypes = {
    onChange: null,
    selected: [],
    multiple: false,
    disabled: false,
    style: {}
}

export default IntervalSelect