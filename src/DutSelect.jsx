/* eslint-disable no-nested-ternary */
/* eslint-disable no-shadow */
/* eslint-disable prefer-template */
/* eslint-disable simple-import-sort/imports */
import React, {useEffect, useState} from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { SetActiveDut, SetFavoriteDut } from './reducers/DutReducer'
import { ConnectionStatus, DutDefaultConfig } from '@arendi/testing'
import { api } from './Api'
import BaudrateSelect from './BaudrateSelect'
import HandshakeSelect from './HandshakeSelect'
import ParitySelect from './ParitySelect'
import SpecificationSelect from './SpecificationSelect'
import ProtocolSelect from './ProtocolSelect'

import { blue } from '@mui/material/colors'
import Box from '@mui/material/Box'
import List from '@mui/material/List'
import Divider from '@mui/material/Divider'
import Stack from '@mui/material/Stack'
import ListItem from '@mui/material/ListItem'
import IconButton from '@mui/material/IconButton'
import ListItemButton from '@mui/material/ListItemButton'
import ListItemIcon from '@mui/material/ListItemIcon'
import ListItemText from '@mui/material/ListItemText'
import ListItemAvatar from '@mui/material/ListItemAvatar'
import Avatar from '@mui/material/Avatar'
import ExpandLess from '@mui/icons-material/ExpandLess'
import ExpandMore from '@mui/icons-material/ExpandMore'
import Collapse from '@mui/material/Collapse'
import DialogTitle from '@mui/material/DialogTitle'
import Dialog from '@mui/material/Dialog'
import DialogContent from '@mui/material/DialogContent'
import Button from '@mui/material/Button'
import DialogActions from '@mui/material/DialogActions'
import ClickAwayListener from '@mui/base/ClickAwayListener'

import FavoriteIcon from '@mui/icons-material/StarBorderOutlined'
import FavoriteIconActive from '@mui/icons-material/Star'
import SettingsIcon from '@mui/icons-material/MiscellaneousServices'
import EjectIcon from '@mui/icons-material/Eject'
import ConnectedIcon from '@mui/icons-material/Link'
import DisconnectedIcon from '@mui/icons-material/LinkOff'
import ProductNameIcon from '@mui/icons-material/Sell'

// Truncate long strings to maxLength-1 characters and append …
const ShortString = (str, maxLength) => (str?.length <= maxLength)? str : `${str?.substring(0, maxLength-1)}\u2026`
const DEFAULT_TIMEOUT_MS = 2000

const WaitMs = timeMs => new Promise((resolve, reject) => setTimeout(resolve, timeMs))

function DutSelectComponent() {
  const dispatch = useDispatch()    
  const { measurementActive } = useSelector(state => state.app.app)
  const { activeDut, favoriteDut, baudrate, handshake, parity, specification, protocol } = useSelector(state => state.app.dut)
  const { DutList, dutList } = useSelector(state => state.app.api)
  const [selectOpen, SetSelectOpen] = useState(false)  
  const [dialogOpen, SetDialogOpen] = useState(false)
  const [userBaudrate, SetUserBaudrate] = useState(baudrate)
  const [userHandshake, SetUserHandshake] = useState(handshake)
  const [userParity, SetUserParity] = useState(parity)
  const [userSpecification, SetUserSpecification] = useState(specification)
  const [userProtocol, SetUserProtocol] = useState(protocol)
  const [configurationDut, SetConfigurationDut] = useState(null)
  const [openCloseBusy, SetOpenCloseBusy] = useState(false)
  const isFavorite = (activeDut?.identifier === favoriteDut)

  // Load proper UART settings when DUT for configuration has changed
  useEffect(() => {
    SetUserBaudrate(configurationDut?.baudrate?? DutDefaultConfig.baudrate)
    SetUserHandshake(configurationDut?.handshake?? DutDefaultConfig.handshake)
    SetUserParity(configurationDut?.parity?? DutDefaultConfig.parity)
    SetUserSpecification(configurationDut?.specification?? DutDefaultConfig.specification)
    SetUserProtocol(configurationDut?.protocol?? DutDefaultConfig.protocol) 
  }, [configurationDut]) // eslint-disable-line react-hooks/exhaustive-deps

  // Mark activeDut as favorite
  const SetFavorite = isFavorite => {
    dispatch(SetFavoriteDut(isFavorite? activeDut?.identifier : "", true))
  }  

  // Open DUT settings dialog
  const OpenSettings = () => {
    if (activeDut) {
      SetConfigurationDut(activeDut)
      SetDialogOpen(true)
    }
  }

  // Set active DUT 
  const SetActive = dut => {
    dispatch(SetActiveDut(dut))
  }

  // Handle DUT attach, remove, connect, disconnect events
  useEffect(() => {

    // Do nothing if a DUT is already selected and connected
    const active = DutList?.Get(activeDut?.identifier)
    if (active?.connectionStatus === ConnectionStatus.Connected) {
      return
    }

    // Automatically select favorite DUT if present and connected
    const favorite = DutList?.Get(favoriteDut)
    if (favorite?.connectionStatus === ConnectionStatus.Connected) {
      SetActive(favorite)
      return
    }

    SetActive(null)
  }, [dutList]) // eslint-disable-line react-hooks/exhaustive-deps


  // Called when a DUT is selected from the select menu
  const DutSelectHandler = dut => {
    
    // Open configuration dialog if DUT is not yet connected or if DUT is already set as activeDut
    if (dut?.connectionStatus !== ConnectionStatus.Connected || dut === activeDut) {
      SetConfigurationDut(dut)
      SetDialogOpen(true)
    }
    
    // Set as activeDut if dut is already connected, without showing configuration dialog
    else {
      SetActive(dut)
      SetDialogOpen(false)
      SetSelectOpen(false)
    }
  }

  // Called when pressing "Open" in DUT config dialog
  const DutOpenHandler = async dut => {
    try {
      SetOpenCloseBusy(true)
      await api.DutConnect(dut.identifier, {protocol:userProtocol, baudrate:userBaudrate, handshake:userHandshake, parity:userParity, specification:userSpecification}, DEFAULT_TIMEOUT_MS)
      await WaitMs(500) // Avoid race conditions with dutList change handler
      SetActive(dut)
      SetDialogOpen(false)    
      SetSelectOpen(false)
    }
    catch (error) {
      console.log("Error opening DUT", dut)
    }

    SetOpenCloseBusy(false)
  }

  // Called when pressing "Close" in DUT config dialog
  const DutCloseHandler = async dut => {
    try {
      SetOpenCloseBusy(true)
      await api.DutDisconnect(dut.identifier, DEFAULT_TIMEOUT_MS)
      SetDialogOpen(false)
      SetSelectOpen(false)
    }
    catch (error) {
      console.log("Error closing DUT", dut)
    } 

    SetOpenCloseBusy(false)
  }

  // Called when dialog is about to close
  const DialogCloseHandler = (event, reason) => {    
    if (reason === "backdropClick") {
      SetSelectOpen(false)
      return
    }
    SetDialogOpen(false)
  }


  // Creates a list containing all selectable DUTs
  // The list is numerically sorted by the DUT identifier and the favorite DUT always comes first
  const dutListComponent = dutList.sort((a,b) => {
    if (favoriteDut === a?.identifier)
      return -1
    if (favoriteDut === b?.identifier)
      return 1
    return a?.identifier.localeCompare(b?.identifier, undefined, { numeric:true })
  }).map(dut => {
    return  <ListItemButton
        dense
        selected={dut === activeDut}
        sx={{pl: 1, pr:1}}
        onClick={() => DutSelectHandler(dut)}
        >
        <ListItemIcon>{(favoriteDut === dut?.identifier)? <FavoriteIconActive /> : <FavoriteIcon color="disabled"/>}</ListItemIcon>
        <ListItemText primary={<><span>{`${dut?.identifier}`} </span>{(dut?.connectionStatus === ConnectionStatus.Connected)? <ConnectedIcon fontSize='small' sx={{ml:3}}/> : null}</>} secondary={dut?.description} />
    </ListItemButton>
  })

  return (
      <div>       
          <Box sx={{bgcolor: 'background.paper', mb:2}}>
              <List>

                  {dutList.length === 0 && <>
                      <ListItemButton disabled>
                          <ListItemText primary="No DUT found" secondary="DUT needs to be attached"/>                          
                      </ListItemButton>
                  </>}
                 
                  {dutList.length >= 1 && <>
                      <ListItemButton disabled={measurementActive} onClick={() => SetSelectOpen(open => !open)}>
                          <ListItemText primary={activeDut?.identifier?? "No DUT selected"} secondary={ShortString(activeDut?.description?? "", 20)} />
                          {selectOpen ? <ExpandLess /> : <ExpandMore />}
                      </ListItemButton>

                      {/* DUT selection list, with auto close when clicked outside */}
                      <Collapse in={selectOpen} timeout="auto" unmountOnExit>
                          <ClickAwayListener onClickAway={() => SetSelectOpen(false)}>
                              <div>
                                  {dutListComponent}
                              </div>
                          </ClickAwayListener>
                      </Collapse>                      
                  </>}

                  {activeDut && <>
                      <Divider/>
                      <ListItem disabled={(activeDut?.identifier === undefined) || measurementActive}>
                          <Stack direction="row" justifyContent="space-between" spacing={3}>
                              <IconButton onClick={() => SetFavorite(!isFavorite)}>{isFavorite? <FavoriteIconActive fontSize="small"/> : <FavoriteIcon fontSize="small"/>}</IconButton>
                              <IconButton onClick={() => OpenSettings()}><SettingsIcon fontSize="small"/></IconButton>
                              <IconButton onClick={() => DutCloseHandler(activeDut)}><EjectIcon fontSize="small"/></IconButton>
                          </Stack>
                      </ListItem>
                  </>}
              </List>
          </Box>

          {/* DUT configuration dialog */}
          <Dialog
              open={dialogOpen}
              onClose={DialogCloseHandler}
          >
              <DialogTitle>DUT Settings</DialogTitle>

              <DialogContent sx={{minWidth:400, maxWidth:600}}>

                  <List sx={{ width: '100%', maxWidth: 360, bgcolor: 'background.paper', mb: 3 }} >
                      { /* DUT identifier and connection info */ }
                      <ListItem
                          secondaryAction={
                            (configurationDut?.connectionStatus === ConnectionStatus.Connected)
                            ? <ConnectedIcon color='disabled'/>
                            : <DisconnectedIcon color='disabled' />
                          }
                      >
                          <ListItemAvatar>
                              <Avatar sx={{ bgcolor: blue[500] }}>
                                  <ProductNameIcon />
                              </Avatar>
                          </ListItemAvatar>
                          <ListItemText primary={ configurationDut?.identifier } secondary={ configurationDut?.description } sx={{ userSelect:"text" }} />
                      </ListItem>

                      <Divider variant="inset" component="li" />
                  </List>

                  <List sx={{ width: '100%', bgcolor: 'background.paper' }} >
                      { /* Select protocol */ }
                      <ListItem>
                          <Stack sx={{minWidth: 250, width:'100%'}}>
                              <ProtocolSelect
                                  selected={userProtocol}
                                  onChange={SetUserProtocol}
                                  style={{marginBottom: "10px"}}
                                  disabled={configurationDut?.connectionStatus === ConnectionStatus.Connected}
                              />
                          </Stack>
                      </ListItem>

                      { /* Select baudrate */ }
                      <ListItem>
                          <Stack sx={{minWidth: 250, width:'100%'}}>
                              <BaudrateSelect
                                  selected={userBaudrate}
                                  onChange={SetUserBaudrate}
                                  style={{marginBottom: "10px"}}
                                  disabled={configurationDut?.connectionStatus === ConnectionStatus.Connected}
                              />
                          </Stack>
                      </ListItem>

                      { /* Select handshake */ }
                      <ListItem>
                          <Stack sx={{minWidth: 250, width:'100%'}}>
                              <HandshakeSelect
                                  selected={userHandshake}
                                  onChange={SetUserHandshake}
                                  style={{marginBottom: "10px"}}
                                  disabled={configurationDut?.connectionStatus === ConnectionStatus.Connected}
                              />
                          </Stack>
                      </ListItem>

                      { /* Select parity */ }
                      <ListItem>
                          <Stack sx={{minWidth: 250, width:'100%'}}>
                              <ParitySelect
                                  selected={userParity}
                                  onChange={SetUserParity}
                                  style={{marginBottom: "10px"}}
                                  disabled={configurationDut?.connectionStatus === ConnectionStatus.Connected}  
                              />
                          </Stack>
                      </ListItem>

                      { /* Select specification */ }
                      <ListItem>
                          <Stack sx={{minWidth: 250, width:'100%'}}>
                              <SpecificationSelect
                                  selected={userSpecification}
                                  onChange={SetUserSpecification}
                                  style={{marginBottom: "10px"}}
                                  disabled={configurationDut?.connectionStatus === ConnectionStatus.Connected}
                              />
                          </Stack>
                      </ListItem>
                      
                  </List>

              </DialogContent>

              {/* Dut open / close and dialog cancel buttons */}
              <DialogActions>
                  {(configurationDut?.connectionStatus === ConnectionStatus.Connected)
                  ?<Button onClick={ () => DutCloseHandler(configurationDut) } disabled={openCloseBusy}>Disconnect</Button>
                  :<Button onClick={ () => DutOpenHandler(configurationDut) } disabled={openCloseBusy}>Connect</Button>                  
                  }
                  <Button onClick={() => SetDialogOpen(false)} autoFocus>Cancel</Button>
              </DialogActions>
              
          </Dialog>
      </div>
  )
}

DutSelectComponent.propTypes = {

}

export default DutSelectComponent