/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable react/no-unused-prop-types */
/* eslint-disable simple-import-sort/imports */
/* eslint-disable no-shadow */
import React, { useCallback, useEffect, useRef, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { SetActiveTesterAttenuation} from './reducers/TesterReducer';

import { blue, grey } from '@mui/material/colors';
import Card from '@mui/material/Card';
import CardHeader from '@mui/material/CardHeader';
import CardContent from '@mui/material/CardContent';
import CardActions from '@mui/material/CardActions'
import FormControl from '@mui/material/FormControl'
import Tooltip from '@mui/material/Tooltip'
import Stack from '@mui/material/Stack'
import Grid from '@mui/material/Grid'
import Button from '@mui/material/Button'

import ArrowBackIcon from '@mui/icons-material/KeyboardDoubleArrowLeft'
import ArrowForwardIcon from '@mui/icons-material/KeyboardDoubleArrowRight'
import ChevronLeftIcon from '@mui/icons-material/ChevronLeft'
import ChevronRightIcon from '@mui/icons-material/ChevronRight'
import IconButton from '@mui/material/IconButton'
import MoreVertIcon from '@mui/icons-material/MoreVert'
import SnapshotIcon from '@mui/icons-material/CameraAlt'

import Chart from './Chart'
import Knob from './Knob'


function Blt24Attenuator() {
  const dispatch = useDispatch()

  // get Tester data from store
  const { attenuationDb } = useSelector(state => state.app.tester)
  const chartRef = useRef(null)
  const [attenuationSerie, SetAttenuationSerie] = useState([])
  const [timer, SetTimer] = useState(0)


  // Initialize component
  useEffect(() => {    
    // Make sure chart is regularly updated, even if attenuation has not been changed
    const interval = setInterval(() => (SetTimer(value => value + 1)), 200)
    
    // Cleanup when dismounted
    return () => clearInterval(interval)
  }, [])  // eslint-disable-line react-hooks/exhaustive-deps

  // Update chart when attenuation has changed or on timer event
  useEffect(() => {
    SetAttenuationSerie(serie => [...serie, [new Date().toISOString(), attenuationDb]].splice(-300, 300))

  }, [timer]) // eslint-disable-line react-hooks/exhaustive-deps


  // Export SVG image
  const ExportImage = () => {
    chartRef.current?.ExportImage("Attenuator.svg")
  }
  
  const SetAttenuation = useCallback(value => {    
    value = Math.min(Math.max(value, 0), 120)
    dispatch(SetActiveTesterAttenuation(value))
  }, []) // eslint-disable-line react-hooks/exhaustive-deps

  const AttenuationButton = targetDb => {
    return <Button 
        dense
        variant={(attenuationDb === targetDb)? "contained" : "outlined"}
        color={((attenuationDb !== targetDb) && (Math.abs(attenuationDb-targetDb) <= 5))? "secondary" : "primary"}
        onClick={() => SetAttenuation(targetDb)}
        sx={{minWidth:50, width:"100%", textTransform:"none"}}
    >
        {`${targetDb} dB`}
    </Button>
  }



  return (
      <Grid container spacing={2} sx={{ minWidth: 400, maxWidth: 1000, mt: 0 }}>
          {/* BLT24 tester controls */}
          <Grid item xs="auto">

              {/* Attenuator knob and buttons */}
              <Card sx={{ minWidth: 300, maxWidth: 300, minHeight: 400 }}>
                  <CardHeader
                      action={
                          <IconButton aria-label="settings">
                              <MoreVertIcon />
                          </IconButton>
                        }
                      title="Attenuator"
                      subheader="Set BLT2450 attenuation"
                    />

                  <CardContent sx={{ minHeight: 300, display: "flex", flexDirection: "column" }}>
                      <Grid container spacing={2}>

                          {/* PER and attenuation knobs */}
                          <Grid item xs={12}>
                              <Knob
                                  value={attenuationDb}
                                  min={0}
                                  max={120}
                                  start={-45}
                                  stop={45}
                                  segmentColor={blue[500]}
                                  valueColor={blue[500]}
                                  labelColor={grey[500]}
                                  label="dB"
                                  step={0.25}
                                  onChanged={SetAttenuation}
                                  width="200"
                                  height="200"
                                  />
                          </Grid>
                            
                          <Grid item xs={12} sx={{ marginTop:"auto" }}>
                              <Stack direction="row" justifyContent="space-evenly" spacing={1}>
                                  <IconButton onClick={() => SetAttenuation(attenuationDb-1)}><ArrowBackIcon /></IconButton>
                                  <IconButton onClick={() => SetAttenuation(attenuationDb-0.25)}><ChevronLeftIcon /></IconButton>
                                  <IconButton onClick={() => SetAttenuation(attenuationDb+0.25)}><ChevronRightIcon /></IconButton>
                                  <IconButton onClick={() => SetAttenuation(attenuationDb+1)}><ArrowForwardIcon /></IconButton>
                              </Stack>
                          </Grid>
                      </Grid>

                  </CardContent>
              </Card>
          </Grid>

          {/* Attenuation settings */}
          <Grid item xs>
              {/* Attenuator controls */}
              <Card sx={{ minWidth: 300, minHeight: 400}}>
                  <CardHeader
                      action={
                          <IconButton aria-label="settings">
                              <MoreVertIcon />
                          </IconButton>
                        }
                      title="Attenuator"
                      subheader="Select discrete BLT2450 attenuation values"
                    />

                  <CardContent sx={{ minHeight: 300, display: "flex", flexDirection: "column" }}>
                      <Grid container spacing={2} columns={5}>
                            
                          <Grid item xs={1}>
                              {AttenuationButton(0)}
                          </Grid>
                          <Grid item xs={1}>
                              {AttenuationButton(10)}
                          </Grid>
                          <Grid item xs={1}>
                              {AttenuationButton(20)}
                          </Grid>
                          <Grid item xs={1}>
                              {AttenuationButton(30)}
                          </Grid>
                          <Grid item xs={1}>
                              {AttenuationButton(40)}
                          </Grid>

                          <Grid item xs={1}>
                              {AttenuationButton(50)}
                          </Grid>
                          <Grid item xs={1}>
                              {AttenuationButton(60)}
                          </Grid>
                          <Grid item xs={1}>
                              {AttenuationButton(70)}
                          </Grid>
                          <Grid item xs={1}>
                              {AttenuationButton(80)}
                          </Grid>
                          <Grid item xs={1}>
                              {AttenuationButton(90)}
                          </Grid>

                          <Grid item xs={1}>
                              {AttenuationButton(100)}
                          </Grid>
                          <Grid item xs={1}>
                              {AttenuationButton(110)}
                          </Grid>
                          <Grid item xs={1}>
                              {AttenuationButton(120)}
                          </Grid>
                      </Grid>

                  </CardContent>
              </Card>
          </Grid>


          {/* Attenuation chart */}
          <Grid item xs={12}>
              <Card sx={{ minWidth: 400, mb: 0 }}>
                  <CardContent>
                      <Grid container spacing={2}>
                          <Grid item xs={12}>
                              <Chart
                                  ref={chartRef}
                                  time
                                  yMin={0}
                                  yMax={120}
                                  height={400}
                                  yAxisUnit="dB"
                                  series={[{name:"Attenuation [dB]", data: attenuationSerie}]}
                                   />
                          </Grid>
                      </Grid>

                  </CardContent>



                  <CardActions>
                      <Stack spacing={2} sx={{width:"100%", maxwidth:800}}>

                          <Stack direction="row" spacing={2}>                         

                   
                              {/* Save SVG image */}
                              <FormControl sx={{ml:4}} size="small" >
                                  <Tooltip title="Save snapshot" enterDelay={500}>
                                      <IconButton onClick={ExportImage}><SnapshotIcon fontSize="medium"/></IconButton>
                                  </Tooltip>
                              </FormControl>

                          </Stack>
                      </Stack>
                  </CardActions>
              </Card>
          </Grid>
      </Grid>
  )
}

Blt24Attenuator.propTypes = {

}

export default Blt24Attenuator