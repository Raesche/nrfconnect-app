/* eslint-disable simple-import-sort/imports */
import React from 'react'
import { Protocol } from '@arendi/testing'
import Selection from './Selection'


const items = Protocol.List().map(def => ({text:`${def[0]}`, value:def[1]}))

const ProtocolSelect = React.memo(({onChange=null, selected=Protocol.TwoWire, multiple=false, disabled=false, style={}}) => {
  return (
      <Selection 
          label="Protocol"
          items={items}
          selected={selected}
          onChange={onChange}
          multiple={multiple}
          disabled={disabled}
          style={style}
      />
  )
})

ProtocolSelect.propTypes = {
    onChange: null,
    selected: [],
    multiple: false,
    disabled: false,
    style: {}
}

export default ProtocolSelect