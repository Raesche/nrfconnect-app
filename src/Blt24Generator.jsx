/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable react/no-unused-prop-types */
/* eslint-disable simple-import-sort/imports */
/* eslint-disable no-shadow */
import React, { useCallback, useState, useEffect, useRef } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Start, Stop, SetPower } from './reducers/GeneratorReducer'

import { keyframes } from '@mui/system';

import { blue, grey } from '@mui/material/colors';
import Stack from '@mui/material/Stack';
import Grid from '@mui/material/Grid'
import Button from '@mui/material/Button'
import Card from '@mui/material/Card';
import CardHeader from '@mui/material/CardHeader';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import FormControl from '@mui/material/FormControl'
import Tooltip from '@mui/material/Tooltip'

import IconButton from '@mui/material/IconButton';
import MoreVertIcon from '@mui/icons-material/MoreVert';
import ArrowBackIcon from '@mui/icons-material/KeyboardDoubleArrowLeft'
import ArrowForwardIcon from '@mui/icons-material/KeyboardDoubleArrowRight'
import ChevronLeftIcon from '@mui/icons-material/ChevronLeft'
import ChevronRightIcon from '@mui/icons-material/ChevronRight'
import StartTxIcon from '@mui/icons-material/PlayArrow'
import StopIcon from '@mui/icons-material/Stop'
import SnapshotIcon from '@mui/icons-material/CameraAlt'

import Chart from './Chart'
import Knob from './Knob'
import { DtmChannel } from '@arendi/testing'
import FrequencySelect from './FrequencySelect'


const pulse = keyframes`
  0% {
    transform: scale(1);
  }
  30% {
    transform: scale(1.2);
    color:${blue[500]};
  }
  60% {
    transform: scale(1);
  }
`;


function Blt24Generator() {
  const dispatch = useDispatch()

  // get tester data and user settings from store
  const { activeTester, generatorPowerDbm:powerDbm, generatorActive:active } = useSelector(state => state.app.tester)
  const { channel, powerDbm:initialPowerDbm } = useSelector(state => state.app.generator)
  const [_channel, _SetChannel] = useState(DtmChannel.Ch19)
  const chartRef = useRef(null)
  const [powerSerie, SetPowerSerie] = useState([])
  const [timer, SetTimer] = useState(0)

  // Initialize component
  useEffect(() => {    
    // Make sure chart is regularly updated, even if attenuation has not been changed
    const interval = setInterval(() => (SetTimer(value => value + 1)), 200)

    // Restore settings
    if (activeTester) {
        dispatch(SetPower(activeTester, initialPowerDbm))
    }
    
    // Cleanup when dismounted
    return () => clearInterval(interval)
  }, [])  // eslint-disable-line react-hooks/exhaustive-deps

  // Update chart when attenuation has changed or on timer event
  useEffect(() => {
    SetPowerSerie(serie => [...serie, [new Date().toISOString(), active? powerDbm : null]].splice(-300, 300))

  }, [timer]) // eslint-disable-line react-hooks/exhaustive-deps


  // Export SVG image
  const ExportImage = () => {
    chartRef.current?.ExportImage("Attenuator.svg")
  }

  // Changing output power has immediate effect on running generator
  const SetOutputPower = useCallback(value => {
    dispatch(SetPower(activeTester, value))
  }, []) // eslint-disable-line react-hooks/exhaustive-deps


  // Changing channel will be applied when starting the generator
  const ChannelHandler = channel => {
    _SetChannel(channel)    
  }

  const StartGenerator = () => {
    if (activeTester) {
        dispatch(Start(activeTester, _channel, powerDbm))
    }
  }

  const StopGenerator = () => {
    if (activeTester) {
        dispatch(Stop(activeTester))
    }
  }

  const PowerButton = targetDbm => {
    return <Button 
        dense
        variant={(powerDbm === targetDbm)? "contained" : "outlined"}
        color={((powerDbm !== targetDbm) && (Math.abs(powerDbm-targetDbm) <= 5))? "secondary" : "primary"}
        onClick={() => SetOutputPower(targetDbm)}
        sx={{minWidth:50, width:"100%", textTransform:"none"}}
    >
        {`${targetDbm} dBm`}
    </Button>
  }

  return (
      <Grid container spacing={2} sx={{ minWidth: 400, maxWidth: 1000, mt: 0 }}>
              
          {/* Output power knob and buttons */}
          <Grid item xs="auto">

              <Card sx={{ minWidth: 300, maxWidth: 300, minHeight: 400}}>
                  <CardHeader
                      action={
                          <IconButton aria-label="settings">
                              <MoreVertIcon />
                          </IconButton>
                        }
                      title="Signal Generator"
                      subheader="Set BLT2450 output power"
                    />

                  <CardContent sx={{ minHeight: 300, display: "flex", flexDirection: "column" }}>
                      <Grid container spacing={2}>

                          {/* Power level knob */}
                          <Grid item xs={12}>
                              <Knob
                                  value={powerDbm}
                                  min={-120}
                                  max={0}
                                  start={-45}
                                  stop={45}
                                  segmentColor={blue[500]}
                                  valueColor={blue[500]}
                                  labelColor={grey[500]}
                                  label="dBm"
                                  step={0.25}
                                  onChanged={SetOutputPower}
                                  width="200"
                                  height="200"
                                  />
                          </Grid>

                          <Grid item xs={12} sx={{ marginTop:"auto" }}>
                              <Stack direction="row" justifyContent="space-evenly" spacing={1}>
                                  <IconButton onClick={() => SetOutputPower(powerDbm-1)}><ArrowBackIcon /></IconButton>
                                  <IconButton onClick={() => SetOutputPower(powerDbm-0.25)}><ChevronLeftIcon /></IconButton>
                                  <IconButton onClick={() => SetOutputPower(powerDbm+0.25)}><ChevronRightIcon /></IconButton>
                                  <IconButton onClick={() => SetOutputPower(powerDbm+1)}><ArrowForwardIcon /></IconButton>
                              </Stack>

                          </Grid>
                      </Grid>

                  </CardContent>
              </Card>
          </Grid>

          {/* Output power buttons, channel select and start / stop button */}
          <Grid item xs>

              <Card sx={{ minWidth: 400, minHeight: 400}}>
                  <CardHeader
                      action={
                          <IconButton aria-label="settings">
                              <MoreVertIcon />
                          </IconButton>
                        }
                      title="Signal Generator"
                      subheader="Select frequency and output power, start / stop generator"
                      />

                  <CardContent sx={{ minHeight: 300, display: "flex", flexDirection: "column" }}>
                      <Grid container spacing={2} columns={5}>
                            
                          <Grid item xs={1}>
                              {PowerButton(0)}
                          </Grid>
                          <Grid item xs={1}>
                              {PowerButton(-10)}
                          </Grid>
                          <Grid item xs={1}>
                              {PowerButton(-20)}
                          </Grid>
                          <Grid item xs={1}>
                              {PowerButton(-30)}
                          </Grid>
                          <Grid item xs={1}>
                              {PowerButton(-40)}
                          </Grid>

                          <Grid item xs={1}>
                              {PowerButton(-50)}
                          </Grid>
                          <Grid item xs={1}>
                              {PowerButton(-60)}
                          </Grid>
                          <Grid item xs={1}>
                              {PowerButton(-70)}
                          </Grid>
                          <Grid item xs={1}>
                              {PowerButton(-80)}
                          </Grid>
                          <Grid item xs={1}>
                              {PowerButton(-90)}
                          </Grid>

                          <Grid item xs={1}>
                              {PowerButton(-100)}
                          </Grid>
                          <Grid item xs={1}>
                              {PowerButton(-110)}
                          </Grid>
                          <Grid item xs={1}>
                              {PowerButton(-120)}
                          </Grid>
                      </Grid>

                      <Grid container spacing={2} sx={{ marginTop:"auto" }}>
                          <Grid item xs={12}>
                              <Stack direction="row" justifyContent="flex-start" spacing={1}>
                                  {/* Select frequency */}
                                  <FrequencySelect
                                      selected={channel}
                                      onChange={ChannelHandler}
                                      disabled={active}
                                      style={{minWidth: 120}}
                                      />
                                  
                                  { /* Start / Stop generator */ }
                                  <div style={{width: 100}}>
                                      { active === true 
                                        ? <IconButton disabled={!active} onClick={StopGenerator}>
                                            <StopIcon fontSize="large" sx={{animation: `${pulse} 1s infinite ease-in-out`}}/></IconButton>
                                        : <IconButton disabled={active} onClick={StartGenerator}><StartTxIcon fontSize="large"/></IconButton>
                                      }
                                  </div>
                              </Stack>
                          </Grid>
                      </Grid>

                  </CardContent>
              </Card>
          </Grid>


          {/* Power chart */}
          <Grid item xs={12}>
              <Card sx={{ minWidth: 400, mb: 0 }}>
                  <CardContent>
                      <Grid container spacing={2}>
                          <Grid item xs={12}>
                              <Chart
                                  ref={chartRef}
                                  time
                                  yMin={-120}
                                  yMax={0}
                                  height={400}
                                  yAxisUnit="dBm"
                                  series={[{name:"Output power [dBm]", data: powerSerie}]}
                                  />
                          </Grid>
                      </Grid>
                  </CardContent>

                  <CardActions>
                      <Stack spacing={2} sx={{width:"100%", maxwidth:800}}>

                          {/* Save SVG image */}
                          <Stack direction="row" spacing={2}>          
                              <FormControl sx={{ml:4}} size="small" >
                                  <Tooltip title="Save snapshot" enterDelay={500}>
                                      <IconButton onClick={ExportImage}><SnapshotIcon fontSize="medium"/></IconButton>
                                  </Tooltip>
                              </FormControl>
                          </Stack>
                      </Stack>
                  </CardActions>
              </Card>
          </Grid>
      </Grid>
  )
}

Blt24Generator.propTypes = {
}

export default Blt24Generator