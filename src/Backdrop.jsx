import React from 'react'

function Backdrop({active=false, style={}, children}) {
    return (
        <>
            {active &&
            <div
                style={{
                    position:"absolute",
                    width: "100vw",
                    height: "100vh",
                    top: "0px",
                    left: "0px",
                    backgroundColor: "rgba(0,0,0,0.7)",
                    zIndex: 10,
                    ...style
                }}             
            >
                <div
                    style={{
                        width: "100vw",
                        height: "100vh",
                        display: "flex",
                        justifyContent: "center",
                        alignItems: "center"
                    }}
                >
                    {children}

                </div>
    
            </div>
            }
        </>
  )
}

Backdrop.propTypes = {
    active: false,
    style: {},
    children: null
}

export default Backdrop