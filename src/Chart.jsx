/* eslint-disable react/no-unused-prop-types */
/* eslint-disable simple-import-sort/imports */
/* eslint-disable no-shadow */
import React, { useEffect, useImperativeHandle, useRef, useState, forwardRef } from 'react'

// See
// https://vasconez.dev/posts/1
// https://vasconez.dev/posts/2
// https://dev.to/maneetgoyal/using-apache-echarts-with-react-and-typescript-353k
// https://echarts.apache.org/en/option.html#title
import { init, getInstanceByDom } from "echarts"

const defaultChartOptions = {
  width: 'auto',
  height: 'auto',
        
  xAxis: {
    type: "time",
    // min: 10,
    // max: 120,
    axisTick: {
      length: 6,
      lineStyle: {
        type: 'dashed'
      }
    },
    axisLabel: {
      formatter: '{value} dB',
      align: 'center',
      showMinLabel: true,
      showMaxLabel: true
    }
  },
  yAxis: {
    type: "value",
    min: 0,
    max: 100,
    splitNumber: 10,
    axisLabel: {
      formatter: '{value} dBm',
      align: 'right'
    },
    axisLine: {
      lineStyle: {
        type: 'dashed'
      }
    }
  },
  tooltip: {
    trigger: 'axis',
    axisPointer: {
      type: 'cross',
      label: {
        // https://echarts.apache.org/en/option.html#tooltip.axisPointer.label.formatter
        formatter: param => (param.axisDimension === 'x')? `${param.value?.toFixed(2) || '--'} dB` : `${param.value?.toFixed(1) || '--'} %`
      }
    },
    valueFormatter: value => `${value?.toFixed(1) || '--'} dBm`
    // formatter: 'name: {a}, name: {b}, value: {c}'
  },

  legend: {
    orient: 'horizontal',
    right: 30,
    top: 5
  },

  grid: {
    top: 50,
    left: 70,
    right: 30,
    bottom: 20
  },
  
  toolbox: {
    show: false,
    feature: {
      saveAsImage: {}
    }
  },

  animation: false
}

function Chart({xMin=null, xMax=null, width="100%", xAxisUnit='', xAxisReverse=false, yMin=0, yMax=100, height=400, yAxisUnit='', time=false, series=[]}, ref) {
  const chartRef = useRef(null)
  const chartInst = useRef(null)
  const [chartStyles, SetChartStyles] = useState({ width, height })

  // Update chart options
  const SetChartOptions = (options, replaceOptions=false) => {
    if (chartRef.current !== null) {
      const chart = getInstanceByDom(chartRef.current)
      chart?.setOption(options, replaceOptions)
    }
  }

  useImperativeHandle(ref, () => ({
    ExportString: () => ExportString(),
    ExportImage: fileName => ExportImage(fileName),
    Resize: (width='auto', height='auto') => chartInst.current?.resize({width, height})
  }))


  // Export SVG string
  const ExportString = () => {
    const str = chartInst.current?.renderToSVGString({
      useViewBox: true
    })
    return str
  }

  // Export SVG image
  const ExportImage = fileName => {
    const url = chartInst.current?.getDataURL({
      pixelRatio: 2,
      backgroundColor: '#fff',
      type: 'svg',
      excludeComponents: ['toolbox']
    })
    const tempA = document.createElement("a");
    tempA.download = fileName
    tempA.href = url
    document.body.appendChild(tempA)
    tempA.click()
    tempA.remove()
  }


  // Adjust chart on series change
  useEffect(() => {
    const chartSeries = series?.map(serie => (
      {
        ...serie,
        type: "line",
        smooth: false,
        showSymbol: false,
        symbolSize: 0
      }
    ))    

    SetChartOptions({series:chartSeries}, {replaceMerge: ['series']})
  }, [series])  // eslint-disable-line react-hooks/exhaustive-deps


  // Adjust chart on axis range change
  useEffect(() => {
    SetChartOptions({
      xAxis: {
        type: time? "time" : "value",
        min: xMin,
        max: xMax
      },
      yAxis: {
        min: yMin,
        max: yMax
      }
    })

  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [xMin, xMax, yMin, yMax, time])

  // Resize chart if height changes
  useEffect(() => {
    chartInst.current?.resize({width:'auto', height:'auto'})
    SetChartStyles(value => ({...value, height}))
  }, [height])


  // Initialize chart
  useEffect(() => {
    let chart
    function resizeChart() {
      chart?.resize({ width:'auto', height:'auto' })
    }
    
    if (chartRef.current !== null) {
      // Set initial chart options
      chart = init(chartRef.current, null, {renderer: 'svg'})
      chartInst.current = chart
      SetChartOptions({
        ...defaultChartOptions,
        xAxis: {
          min: xMin,
          max: xMax,
          type: time? "time" : "value",
          axisLabel: {
            formatter: time? '{HH}:{mm}:{ss}' : `{value} ${xAxisUnit}`
          },
          inverse: xAxisReverse
        },
        yAxis: {
          min: yMin,
          max: yMax,
          splitNumber: 10,
          axisLabel: {
            formatter: `{value} ${yAxisUnit}`,
            align: 'right'
          },
        },
  
        // Define formaters for displaying values in tooltips
        tooltip: {
          trigger: 'axis',
          axisPointer: {
            type: 'cross',
            label: {
              // https://echarts.apache.org/en/option.html#tooltip.axisPointer.label.formatter
              formatter: param => {
                if (param.axisDimension === 'x')
                  return time? `${new Date(param.value || Date.now()).toLocaleString()}` : `${param.value?.toFixed(1) || '--'} dB` // TODO
                return `${param.value?.toFixed(1) || '--'} ${yAxisUnit}`
              }
            }
          },
          valueFormatter: value => `${value?.toFixed(1) || '--'} ${yAxisUnit}`
        }
  
      }, true)
      
      // Adjust chart on window resize
      window.addEventListener("resize", resizeChart)      
    }

    // Cleanup on exit
    return () => {
      chart?.dispose()
      chart = null  // workaround to avoid memory leaks? https://medium.com/@kelvinau4413/memory-leak-from-echarts-occurs-if-not-properly-disposed-7050c5d93028
      window.removeEventListener("resize", resizeChart)
    }
  }, [])  // eslint-disable-line react-hooks/exhaustive-deps


  return (
      <div style={{ display: "flex", flexDirection: "row" }}>
          <div
              ref={chartRef}
              style={chartStyles}
          />
      </div>
  )
}


Chart.propTypes = {
  tester: null,
  xMin: 0,
  xMax: 120,
  width: "100%",
  xAxisUnit: '',
  xAxisReverse: false,
  yMin: -80, 
  yMax: 10,
  height: 400,
  yAxisUnit: '',
  time: false,
  series: []
}

export default forwardRef(Chart)