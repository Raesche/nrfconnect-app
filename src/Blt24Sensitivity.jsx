/* eslint-disable react/no-unused-prop-types */
/* eslint-disable simple-import-sort/imports */
/* eslint-disable no-shadow */
import React, { useCallback, useState, useEffect, useRef } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import { getUserDataDir } from 'pc-nrfconnect-shared'
import { Sensitivity, DtmDirection } from '@arendi/testing'
import { SetMeasurementActive } from './reducers/AppReducer'
import { SetDtmConfig, SetAttenuationRange, SetAttenuationStepsize, SetDirection, SetMeasurementData } from './reducers/SensitivityReducer'
import Report from './Report'
import Csv from './Csv'
import Chart from './Chart'
import DtmConfig from './DtmConfig'
import Selection from './Selection'
import Knob from './Knob'
import FileSelect from './FileSelect'

import { blue, grey } from '@mui/material/colors'
import Grid from '@mui/material/Grid'
import Stack from '@mui/material/Stack'
import FormControl from '@mui/material/FormControl'
import Card from '@mui/material/Card'
import CardHeader from '@mui/material/CardHeader'
import CardActions from '@mui/material/CardActions'
import CardContent from '@mui/material/CardContent'
import Slider from '@mui/material/Slider'
import Tooltip from '@mui/material/Tooltip'
import IconButton from '@mui/material/IconButton'
import Typography from '@mui/material/Typography'

import MoreVertIcon from '@mui/icons-material/MoreVert'
import StartIcon from '@mui/icons-material/PlayArrow'
import ScanIcon from '@mui/icons-material/FastForward'
import StopIcon from '@mui/icons-material/Stop'
import PrintIcon from '@mui/icons-material/LocalPrintshop'
import SnapshotIcon from '@mui/icons-material/CameraAlt'
import ArrowIcon from '@mui/icons-material/ArrowBack'

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faFloppyDisk as SaveIcon, faFolderOpen as OpenIcon } from '@fortawesome/free-solid-svg-icons'

import Backdrop from './Backdrop'


const REF_POWER_DBM = 0  // Chip outputpower
const SCAN_DEFAULT_START_DB = 0
const SCAN_DEFAULT_STOP_DB = 120

// State definitions
const IDLE = 0
const SCANNING = 1
const SWEEPING = 2


// Round up using provided step
const RoundUp = (value=0, step=0.1) => Math.ceil(value/step) * step

// ROund down using provided step
const RoundDown = (value=0, step=0.1) => Math.floor(value/step) * step

const serieLimit = {
  name: "Limit",
  data: [[-1000, 30.8 ], [1000, 30.8 ]],
  itemStyle: {color: '#ff0000'},
  lineStyle: {type: 'solid', width: 0.5}
}


function Blt24Sensitivity() {
  const dispatch = useDispatch()    
  const { dtmConfig, stepDb, rangeDb, direction, measurementData:series } = useSelector(state => state.app.sensitivity)
  const { activeTester:tester } = useSelector(state => state.app.tester)
  const { activeDut:dut } = useSelector(state => state.app.dut)
  const [ progress, SetProgress ] = useState(0)
  const [ state, SetState ] = useState(IDLE)
  const [ isPrinting, SetIsPrinting ] = useState(false)
  const [ date, SetDate ] = useState(new Date())
  const [ results, SetResults ] = useState({series:[], dtmConfig:null})
  const sensitivityRef = useRef(new Sensitivity())
  const dtmConfigRef = useRef(dtmConfig)
  const chartRef = useRef(null)
  const isBusy = state !== IDLE
  const isSeriesValid = series?.reduce((prev, current) => prev && (current.data?.length>0), true)

  dtmConfigRef.current = dtmConfig
  
  const AttenuationRangeHandler = useCallback(value => {
    dispatch(SetAttenuationRange(value.target.value))
  }, []) // eslint-disable-line react-hooks/exhaustive-deps

  const StepsizeHandler = useCallback(value => {
    dispatch(SetAttenuationStepsize(value))
  }, []) // eslint-disable-line react-hooks/exhaustive-deps

  const DirectionHandler = useCallback(value => {
    dispatch(SetDirection(value))
  }, []) // eslint-disable-line react-hooks/exhaustive-deps

  // TODO: Proper conversion from attenuation to outputpower (requries BLT2450 calibration data)
  const AttenuationToDbm = attenuationDb => (typeof attenuationDb === "number")? REF_POWER_DBM - attenuationDb : undefined
  const PowerToDb = powerDbm => (typeof powerDbm === "number")? REF_POWER_DBM - powerDbm : undefined
  
  
  // DTM settings changed handler
  const DtmSettingHandler = useCallback(settings => {
    dispatch(SetDtmConfig({
        channels: settings.channels,
        bytes: settings.bytes,
        pattern: settings.pattern,
        phy: settings.phy,
        interval: settings.interval
    }))
  }, []) // eslint-disable-line react-hooks/exhaustive-deps


  // Quick scan sensitivity limits
  const StartScan = () => {
    StopMeasurement()
    SetState(SCANNING)
    dispatch(SetMeasurementActive(true))
    SetDate(new Date())
    dispatch(SetAttenuationRange([SCAN_DEFAULT_START_DB, SCAN_DEFAULT_STOP_DB]))
    sensitivityRef.current.Scan(tester, dut, {...dtmConfig, startDb:SCAN_DEFAULT_START_DB, stopDb:SCAN_DEFAULT_STOP_DB, intervalMs:500, targetLimit:1, direction})
  }

    // Initiate new sensitivity measurement
  const StartSweep = () => {
    StopMeasurement()
    SetState(SWEEPING)
    dispatch(SetMeasurementActive(true))
    SetDate(new Date())
    sensitivityRef.current.Sweep(tester, dut, {...dtmConfig, startDb:rangeDb[0], stopDb:rangeDb[1], stepDb, intervalMs:dtmConfig.interval, direction})
  }

  // Stop running sensitivity measurement
  const StopMeasurement = () => {
    if (state === IDLE) return
    sensitivityRef.current.Stop()
  }


  const SetSeries = series => {
    dispatch(SetMeasurementData(series))
  }


  // Create CSV data
  const CreateCsv = () => {    
    try {
        const csv = new Csv()
        const name = `Sensitivity measurements ${date.toISOString().replaceAll("-","").replaceAll(":", "").replaceAll(".","")}`

        csv.Create({
            series,
            date: new Date(),
            directory: getUserDataDir(),
            name
        })
        .then(({csv, fileName}) => {
            console.log(csv)
            console.log(`CSV saved to ${fileName}`)
            open(fileName)  // eslint-disable-line no-restricted-globals
        })
    }

    catch (error) {
        console.log(`No measurement data available, CSV was not created`)
    }    
  }

  // Read CSV data
  const ReadCsv = ({data=null}) => {
    const csv = new Csv()
    csv.Parse({
        data
    })
    .then(({series}) => {

        // Show series data
        SetSeries(series)
        
        
        // Extract channel, per and sensitivity from every serie and save to results
        // This is required in order to show valid data when generating a report from imported measurement data
        const results = series.map(serie => {
            const limit = Sensitivity.CalculateSensitivity(serie.data.map(measurement => ({ attenuationDb:measurement[0], per:measurement[1] })))
            const regex = /(\d{1,2})/gm;
            const channel = parseFloat(serie.name.match(regex))
            return {
                channel,
                per: limit.per,
                sensitivityDbm: limit.attenuationDb
            }
        })        
        SetResults({series: results, dtmConfig:null})
        
        // Calculate min power from all series
        const minDbm = series.reduce((prevSerie, currSerie) => {
            const currMin = currSerie.data.reduce((prev, curr) => Math.min(prev, curr[0]), 1000)
            return Math.min(prevSerie, currMin)
        }, 1000)

        // Calculate max power from all series
        const maxDbm = series.reduce((prevSerie, currSerie) => {
            const currMax = currSerie.data.reduce((prev, curr) => Math.max(prev, curr[0]), -1000)
            return Math.max(prevSerie, currMax)
        }, -1000)

        // Convert min and max power to attenuation and adjust attenuation range to fit measurement data
        const [minDb, maxDb] = [PowerToDb(maxDbm), PowerToDb(minDbm)]
        dispatch(SetAttenuationRange([minDb, maxDb]))
    })
    
  }


  // Create report
  const CreateReport = () => {
    SetIsPrinting(true)
    const svg = ExportString({width:800, height:400})
    const name = `Sensitivity report ${date.toISOString().replaceAll("-","").replaceAll(":", "").replaceAll(".","")}`
    const report = new Report()
    report.Create({
      date: new Date(),
      directory: getUserDataDir(),
      name,
      svg,
      productName: tester.productName,
      serialNumber: tester.serialNumber,
      hardwareVersion: tester.hardwareVersion,
      firmwareVersion: tester.firmwareVersion,
      dtmConfig: results.dtmConfig,
      dut,
      measurements: results.series,
      onSuccess: fileName => {
        open(fileName)  // eslint-disable-line no-restricted-globals
      }
    }).then(fileName => {
      console.log(`Report saved to ${fileName}`)
      SetIsPrinting(false)
    })
  }

  // Connect sensitivity instance to chart
  useEffect(() => {    
    const sensitivity = sensitivityRef.current  // keep reference for cleaning up event handler    

    const SweepEndHandler = () => {
      SetState(IDLE)
      dispatch(SetMeasurementActive(false))
    }

    const SweepProgressHandler = dtm => {
      SetProgress(dtm.progress)
      const series = dtm.series.map(serie => ({ data: serie.data.map(data => [AttenuationToDbm(data.attenuationDb), data.per]), name:`CH${serie.channel}` }))
      const results = dtm.series.map(serie => ({ channel: serie.channel, per:serie.limit.per, sensitivityDbm: AttenuationToDbm(serie.limit.attenuationDb) }))
      SetSeries(series)
      SetResults({ series:results, dtmConfig:dtmConfigRef.current })
    }

    const ScanProgressHandler = scan => {
      SetProgress(scan.progress)
      const series = scan.series.map(serie => ({ data: serie.data.map(data => [AttenuationToDbm(data.attenuationDb), data.per]), name:`CH${serie.channel}` }))
      const results = scan.series.map(serie => ({ channel: serie.channel, per:serie.limit.per, sensitivityDbm: AttenuationToDbm(serie.limit.attenuationDb) }))
      SetSeries(series)
      SetResults({ series:results, dtmConfig:dtmConfigRef.current })
    }

    const ScanEndHandler = series => {
      const startDb = series.reduce((minDb, currentSerie) => Math.min(minDb, currentSerie.limit.attenuationDb?? 1000), 1000)
      const stopDb = series.reduce((maxDb, currentSerie) => Math.max(maxDb, currentSerie.limit.attenuationDb?? -1000), -1000)
      dispatch(SetAttenuationRange([Math.max(0, RoundDown(startDb, 5)-5), Math.min(120, RoundUp(stopDb, 5)+5)]))
      SetState(IDLE)
      dispatch(SetMeasurementActive(false))
    }


    // Register event handler
    sensitivity.OnScanProgress(ScanProgressHandler)
    sensitivity.OnScanEnd(ScanEndHandler)
    sensitivity.OnSweepProgress(SweepProgressHandler)
    sensitivity.OnSweepEnd(SweepEndHandler)

    // Cleanup event handler on exit
    return () => {
      sensitivity.OffScanProgress(ScanProgressHandler)
      sensitivity.OffScanEnd(ScanEndHandler)
      sensitivity.OffSweepProgress(SweepProgressHandler)
      sensitivity.OffSweepEnd(SweepEndHandler)
      sensitivity.Stop()
      dispatch(SetMeasurementActive(false))
    }
  }, []) // eslint-disable-line react-hooks/exhaustive-deps
  
  

  // Export SVG string
  // width and height can be set if the resulting svg should have a specific dimension
  const ExportString = ({width, height}) => {
    chartRef.current?.Resize(width, height)
    const svgString = chartRef.current?.ExportString()
    chartRef.current?.Resize()  // Reset width and height (set to 'auto')
    return svgString
  }

  // Export SVG image
  const ExportImage = () => {
    chartRef.current?.ExportImage("Sensitivity.svg")
  }


    return (
        <div>
            
            <Grid container spacing={2} sx={{ minWidth: 400, maxWidth: 1000, mt: 0 }}>
              
                {/* DTM settings, attenuation control */}
                <Grid item xs>

                    <Card sx={{ minWidth: 400, minHeight: 400}}>
                        <CardHeader
                            action={
                                <IconButton aria-label="settings">
                                    <MoreVertIcon />
                                </IconButton>
                            }
                            title="Measurement settings"
                            subheader="Adjust DTM settings and power range"
                        />

                        <CardContent sx={{ display: "flex", flexDirection: "column", marginTop:3}}>
                            <Grid container spacing={2}>

                                {/* DTM setting controls */}
                                <Grid item xs={12}>
                                    <DtmConfig 
                                        channels={dtmConfig.channels}
                                        bytes={dtmConfig.bytes}
                                        pattern={dtmConfig.pattern}
                                        phy={dtmConfig.phy}
                                        interval={dtmConfig.interval}
                                        multipleChannels
                                        onChange={DtmSettingHandler}
                                        disabled={state !== IDLE}
                                    >
                                        {/* Select direction */}
                                        <Selection 
                                            label="Direction"
                                            items={[{text:"Tester ⇒ DUT", value:DtmDirection.TesterToDut}, {text:"Tester ⇐ DUT", value:DtmDirection.DutToTester}]}
                                            selected={direction}
                                            onChange={DirectionHandler}
                                            disabled={state !== IDLE}
                                            style={{marginRight: "5px", marginBottom: "10px"}}
                                        />

                                    </DtmConfig>
                                </Grid>                              
                            </Grid>

                        </CardContent>


                        <CardContent sx={{ display: "flex", flexDirection: "column" }}>
                            <Grid container spacing={2}>

                                {/* Select attenuation range */}
                                <Grid item xs={12} sm={12} md={10}>
                                    <FormControl sx={{minWidth:{xs:500, md:450}, marginRight: "30px"}} size="small">
                                        <Slider
                                            aria-labelledby="attenuation-range-label"
                                            label="Attenuation range"
                                            value={rangeDb}
                                            valueLabelDisplay="auto"
                                            getAriaValueText={value => `${value} dB`}
                                            onChange={AttenuationRangeHandler}
                                            step={5}
                                            min={0}
                                            max={120}
                                            marks={[0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100, 110, 120].map(value => ({value, label:value}))}
                                            sx={{marginRight: "30px", marginBottom: "10px"}}
                                        />
                                    </FormControl>
                                </Grid>

                                {/* Select attenuation step size */}
                                <Grid item xs={2}>                                    
                                    <Selection 
                                        label="Stepsize"
                                        items={[{text:"0.25 dB", value:0.25}, {text:"0.5 dB", value:0.5}, {text:"1.0 dB", value:1}, {text:"5.0 dB", value:5}, {text:"10 dB", value:10}]}
                                        selected={stepDb}
                                        onChange={StepsizeHandler}
                                        disabled={state !== IDLE}
                                        style={{marginRight: "5px", marginBottom: "10px"}}
                                    />
                                </Grid>
                            </Grid>

                        </CardContent>

                    </Card>
                </Grid>
              
                {/* Measurement controls */}
                <Grid item xs="auto">

                    <Card sx={{ minWidth: 300, maxWidth: 300, minHeight: 400}}>
                        <CardHeader
                            action={
                                <IconButton aria-label="settings">
                                    <MoreVertIcon />
                                </IconButton>
                            }
                            title="Measurement control"
                            subheader="Start and stop measurements"
                        />

                        <CardContent sx={{ display: "flex", flexDirection: "column" }}>
                            <Grid container spacing={2}>

                                {/* Measurement progress knob */}
                                <Grid item xs={12}>
                                    <Knob
                                        value={parseFloat((100*progress).toFixed(2))}
                                        min={0}
                                        max={100}
                                        start={-45}
                                        stop={45}
                                        segmentColor={blue[500]}
                                        valueColor={blue[500]}
                                        labelColor={grey[500]}
                                        label="%"
                                        step={0.1}
                                        width="200"
                                        height="200"
                                        hideDecimals
                                    />
                                </Grid>

                                <Grid item xs={12}>
                                    <Stack direction="row" justifyContent="space-evenly" spacing={1}>
                                        {/* Start quick scan */}                          
                                        <FormControl sx={{ml:0}} size="small" >
                                            <Tooltip title={<h5>Start sensitivity scan</h5>} enterDelay={1000} placement="top" arrow>
                                                <div>
                                                    <IconButton onClick={StartScan} disabled={isBusy}><ScanIcon fontSize="medium" /></IconButton>
                                                </div>
                                            </Tooltip>
                                        </FormControl>
                          

                                        {/* Start measurement sweep */}                          
                                        <FormControl sx={{ml:4}} size="small" >
                                            <Tooltip title={<h5>Start sensitivity measurement</h5>} enterDelay={1000} placement="top" arrow>
                                                <div>
                                                    <IconButton onClick={StartSweep} disabled={isBusy}><StartIcon fontSize="medium" /></IconButton>
                                                </div>
                                            </Tooltip>
                                        </FormControl>
                          

                                        {/* Stop current measurement */}
                                        <FormControl sx={{ml:4}} size="small" >
                                            <Tooltip title={<h5>Terminate sensitivity measurement</h5>} enterDelay={1000} placement="top" arrow>
                                                <div>
                                                    <IconButton onClick={StopMeasurement} disabled={!isBusy}><StopIcon fontSize="medium"/></IconButton>
                                                </div>
                                            </Tooltip>
                                        </FormControl>
                                    </Stack>
                                </Grid>
                            </Grid>

                        </CardContent>
                    </Card>
                </Grid>


                {/* Sensitivity chart */}
                <Grid item xs={12}>
                    <Card sx={{ minWidth: 400, mb: 0 }}>
                        <CardContent>
                            <Grid container spacing={2}>
                                <Grid item xs={12}>
                                    <Chart
                                        ref={chartRef}
                                        xMin={AttenuationToDbm(rangeDb[0])}
                                        xMax={AttenuationToDbm(rangeDb[1])}
                                        xAxisUnit="dBm"
                                        xAxisReverse
                                        yMin={0}
                                        yMax={100}
                                        height={400}
                                        yAxisUnit="%"
                                        series={[serieLimit, ...series]}
                                    />
                                </Grid>
                            </Grid>
                        </CardContent>

                        <CardActions>
                            <Stack spacing={2} sx={{width:"100%", maxwidth:800}}>

                                <Stack direction="row" spacing={2}>

                                    {/* Open CSV file */}
                                    <FormControl sx={{ml:4}} size="small" >
                                        <FileSelect
                                            onload={data => ReadCsv({data})}
                                            accept={{'text/csv':['.csv']}}
                                        >
                                            <Tooltip title={<h5>Load CSV data</h5>} enterDelay={1000} placement="top" arrow>
                                                <div>
                                                    <IconButton disabled={isBusy}>
                                                        <FontAwesomeIcon icon={OpenIcon} size="sm" />
                                                    </IconButton>
                                                </div>
                                            </Tooltip>
                                        </FileSelect>
                                    </FormControl>

                                    {/* Save CSV file */}
                                    <FormControl sx={{ml:4}} size="small">
                                        <Tooltip title={<h5>Save CSV data</h5>} enterDelay={1000} placement="top" arrow>
                                            <div>
                                                <IconButton onClick={CreateCsv} disabled={isBusy || !isSeriesValid}>
                                                    <FontAwesomeIcon icon={SaveIcon} size="sm" />
                                                </IconButton>
                                            </div>
                                        </Tooltip>
                                    </FormControl>

                                    {/* Save SVG image */}
                                    <FormControl sx={{ml:4}} size="small" >
                                        <Tooltip title={<h5>Save snapshot</h5>} enterDelay={1000} placement="top" arrow>
                                            <div>
                                                <IconButton onClick={ExportImage} disabled={isBusy}><SnapshotIcon fontSize="medium"/></IconButton>
                                            </div>
                                        </Tooltip>
                                    </FormControl>

                                    {/* Create report */}
                                    <FormControl sx={{ml:4}} size="small" >
                                        <Tooltip title={<h5>Create and save report</h5>} enterDelay={1000} placement="top" arrow>
                                            <div>
                                                <IconButton onClick={CreateReport} disabled={isBusy || isPrinting}><PrintIcon fontSize="medium"/></IconButton>
                                            </div>
                                        </Tooltip>
                                    </FormControl>

                                </Stack>
                            </Stack>
                        </CardActions>
                    </Card>
                </Grid>
            </Grid>
            
            

              
            { /* Overlay to block UI elements when no DUT is selected */ }
            <Backdrop active={!(dut?.identifier)} style={{ left:-10, top:-10 }} >                
                <Stack spacing={4} direction="row" justifyContent="center" alignItems="center">
                    <ArrowIcon  sx={{color:"white", fontSize:"60pt"}}/>
                    <Stack direction="column">
                        <Typography variant="h2" sx={{color:"white"}}>No DUT selected.</Typography>
                        <Typography variant="h4" sx={{color:"white"}}>Select a DUT in the left panel!</Typography>                    
                    </Stack>                    
                </Stack>
            </Backdrop>
        </div>
  )
}

Blt24Sensitivity.propTypes = {

}

export default Blt24Sensitivity